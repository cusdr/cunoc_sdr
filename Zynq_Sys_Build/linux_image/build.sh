#!/bin/bash
# Set env. vars
export ARCH=arm
export CROSS_COMPILE=arm-xilinx-linux-gnueabi-
echo $CROSS_COMPILE
#
# Start Building
#
# Export Das U-Boot
cd ../uboot/u-boot-xlnx/tools
export PATH=`pwd`:$PATH
cd ../../../linux_image
#
# Make the Kernel
# 1. Set configuration
# 2. Setup for menuconfig, to select different things
# 3. Make the kernel with U-Boot packaging
#
cd linux-xlnx

cp ../cunoc_sdr_kernel_config .config
make UIMAGE_LOADADDR=0x8000 uImage 
cp arch/arm/boot/uImage ../uImage
cd ../
cp uImage ../boot_bin/uImage
