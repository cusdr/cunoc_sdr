#!/bin/bash
# 
export ARCH=arm
export CROSS_COMPILE=arm-xilinx-linux-gnueabi-
export CCOMPILE=arm-xilinx-linux-gnueabi-gcc

cd u-boot-xlnx
make zynq_zed_config
make 
cp u-boot ../u-boot
cp u-boot ../u-boot.elf
cp u-boot ../../boot_bin/u-boot.elf
cd ../
