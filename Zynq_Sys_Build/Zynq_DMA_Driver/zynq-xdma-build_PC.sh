#!/bin/bash

# This script is for building the cross compiling portion 
# of prepraing the zynq-xdma driver.
#
# Instructions:
#
# 0. Make sure a Xilinx Vivado SDK toolchain is installed, we support:
#      2014.4
#      2015.1	
# 1. First run the env_setup.sh script 
# 2. Run u_boot_build.sh to populate the u_boot tools
# 3. Run the kernel_build.sh to populate the kernel and headers 
# 4. To complete the process the library must be natively compiled.
#    This requires the Zedboard to be turned on, running the open-embedded OS
#    and visible on a local area network with:
#
#    hostname: zedboard-zynq7.local
#    username: root
#    pasword:
#
# 5. When the script finishes, you'll need to complete the process by 
#    logging into the zedboard and running the zynq-xdma-build_zedboard.sh 
#    script which will be copied over at the completion of this script. 
#
# TODO: Uncomment your version of Xilinx:
#export PATH=/opt/Xilinx/SDK/2014.4/gnu/arm/lin/bin:$PATH
#export PATH=/opt/Xilinx/SDK/2015.1/gnu/arm/lin/bin:$PATH
export CROSS_COMPILE=arm-xilinx-linux-gnueabi-
export ARCH=arm
cd zynq-xdma/dev/
export KDIR=../../linux-xlnx
make
cd ../..
cp zynq-xdma-build_zedboard.sh zynq-xdma/
scp -r zynq-xdma root@zedboard-zynq7.local:/home/root/

