#!/bin/bash

# This script is for finishing the preperation of the zynq-xdma driver for the Zedboard.
# The library must be natively compiled, copied and rule must be updated accordingly.
# You'll need root privelages to run this script

# Go to the library directory 
cd lib

# Compile the library natively 
make

# Copy the library into the /usr/lib directory 
cp libxdma.so /usr/lib/

cd ..

# Now prepare the driver for automatic bring up:
cp util/80-xdma.rules /etc/udev/rules.d/

# The following portion may or may not work depending on how the OS was setup
# If it gives you trouble go to:
# # cd /lib/modules
# # ls
# Look at the directories in modules
# Now run:
# # uname -r
# If the output of this command doesn't match the name of the directory in /lib/modules/
# that's the issue.
# To fix it in /lib/modules run:
# # cp -r <the directory in /lib/modules>/ <the output of uname -r>/
# This will copy the lib modules directory to the proper location so things match up
# Once this is done, just rerun this whole script
mkdir -p /lib/modules/$(uname -r)/extra/
cp xdma.ko /lib/modules/$(uname -r)/extra/
depmod -a
modprobe xdma
sh -c 'echo "xdma" >> /etc/modules'

