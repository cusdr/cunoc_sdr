#!/bin/bash
# Set env vars
export ARCH=arm
export CROSS_COMPILE=arm-xilinx-linux-gnueabi-
echo $CROSS_COMPILE
# Copy our device trees in
cp zynq-zed.dts ../linux_image/linux-xlnx/arch/arm/boot/dts/zynq-zed.dts
cp zynq-7000.dtsi ../linux_image/linux-xlnx/arch/arm/boot/dts/zynq-7000.dtsi

cd ../linux_image/linux-xlnx/
make zynq-zed.dtb
cp arch/arm/boot/dts/zynq-zed.dtb ../../device_tree/zynq-zed.dtb
cd ../../device_tree/
