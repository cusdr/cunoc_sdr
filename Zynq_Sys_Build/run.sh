#!/bin/bash

# This script is generating boot.bin, uImage, device tree to run on zync board

echo "input:"
echo -e "\tprep(prepare to pull source codes from github)"
echo -e "\tgen(generate all modules)"
read mode

if [ $mode ==  "prep" ];then
	echo "preparing"
	# Setup the Xilinx Linux Repo
	cd linux_image
	git clone -b master https://github.com/Xilinx/linux-xlnx
	cd linux-xlnx/
	git checkout 647ce3f4821fb298e7aaed34281fd315281929bf
	cd ../../

	# Setup the u-Boot Repo
	cd uboot/
	git clone https://github.com/Xilinx/u-boot-xlnx
	cd u-boot-xlnx/
	git checkout dda3f58af8eb86c649114fa87e35257947d22629
	cd ../../

	# Setup the devicetree Repo (optional)
	cd device_tree/
	git clone https://github.com/Xilinx/device-tree-xlnx
	cd device-tree-xlnx/
	git checkout 4d925747430d7eef7e17c49da90ecabb3d60fcd4
	cd ../../

	# Get the zynq-xdma driver:
	cd Zynq_DMA_Driver/
	git clone https://github.com/bmartini/zynq-xdma
	cd ../
elif [ $mode ==  "gen" ];then
	echo "generating"
	cd uboot/
	./build.sh
	cd ../linux_image/
	./build.sh
	cd ../device_tree/
	./build.sh
	cd ../boot_bin/
	./build.sh
else
	echo "wrong input"
fi
