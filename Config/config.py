#!/usr/bin/python
'''
	This script transforms json input to a protobuf class
'''
import json

# use absolute filepath
# return value
#	dict: component name-> list of instances
def JSON_to_Protobuf(filename):
	print "transforming json to protobuf object"
	with open(filename) as json_filedata:
		json_data = json.load(json_filedata)
	instances = {}
	assert "components" in json_data
	assert "version" in json_data
	# version data
	version = {}
	assert "major" in json_data["version"]
	assert "minor" in json_data["version"]
	version["major"] = json_data["version"]["major"]
	version["minor"] = json_data["version"]["minor"]
	# components
	for instance in json_data["components"]:
		assert "name" in instance
		component_name = instance["name"]
		if component_name not in instances:
			instances[component_name] = []
		instances[component_name].append(instance)
	return version, instances

def Protobuf_to_JSON(filename, components, version, switch_info, commitHash):
	print "transforming protobuf object to json"
	json_data = {}
	json_data["components"] = components
	json_data["version"] = {}
	json_data["version"] = version
	json_data["switch_info"] = {}
	json_data["switch_info"] = switch_info
	json_data["ip_commit_hash"] = commitHash
	with open(filename, 'w') as outfile:
		json.dump(json_data, outfile, indent=4)