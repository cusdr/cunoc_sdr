#!/usr/bin/python
'''
	This script is 
		1. automating the whole process of generating vhdl and tcl scripts
		2. run tcl script, generate system.bit
	arguments:
		vivado version, e.g. year.version 
		config file name	 

'''
from Config.config import JSON_to_Protobuf
from Config.config import Protobuf_to_JSON
from VHDL.vhdl_gen import GenerateVHDL
from TCL.tcl_gen import GenerateTCL
import os
import argparse
import subprocess
import re

def runBash(command):
	subprocess.Popen(command, shell=True).communicate()
def runBashOutput(command):
	return subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
gen_parser = subparsers.add_parser('gen', help='generate vhdl and tcl scripts')
gen_parser.add_argument("-v", "--vivado",dest="vivado", help="vivado version: supported are 2014.4, 2015.1", required = True)
gen_parser.add_argument("-c", "--config", dest="config", help="config file path(absolute or relative)", required = True)
gen_parser.add_argument("-i", "--iplib", dest="iplib", help="ip block library", required = False)
gen_parser.set_defaults(subcommand = 'gen')
run_parser = subparsers.add_parser('run', help='run tcl scripts')
run_parser.set_defaults(subcommand = 'run')
clean_parser = subparsers.add_parser('clean', help='clean what tcl scripts generated')
clean_parser.set_defaults(subcommand = 'clean')
args = parser.parse_args()

if args.subcommand == 'gen':
	print "pulling IP blocks"
	if args.iplib is not None:
		iplib = args.iplib
	else:
		iplib = "git@bitbucket.org:cusdr/cunoc_ip_lib.git"
	match = re.search('git@.*:.*/(.*).git', iplib)
	if match:
		repository_name = match.group(1)
		print repository_name
	else:
		print "iplib url not correct form"
		exit()
	PullCmd = "\
		git clone " + iplib + ";\
		rm " + repository_name + "/README.md;\
		cp -R " + repository_name + "/* IP_VHDL_Source/;\
		"
	runBash(PullCmd)
	GetCommitCmd = "\
		cd " + repository_name +";\
		git rev-parse HEAD;\
		cd ..;\
		rm -rf " + repository_name + ";\
		"
	out, error = runBashOutput(GetCommitCmd)
	commitHash = out.rstrip()
	commitHash = iplib+":"+commitHash
	print "generating vhdl and tcl files"
	version, components = JSON_to_Protobuf(os.path.realpath(args.config))
	switch_info, components = GenerateVHDL(version, components)
	Protobuf_to_JSON(os.path.realpath(args.config)+".out", components, version, switch_info, commitHash)
	GenerateTCL(components.keys(), args.vivado)
	print "Complete"
elif args.subcommand == 'run':
	TCLCmd = "\
		vivado -mode batch -source ./Build_Dir/AXIS_SWITCH_IP_gen.tcl;\
		vivado -mode batch -source ./Build_Dir/VER_CTRL_REG_IP_gen.tcl;\
		vivado -mode batch -source ./Build_Dir/Base_build.tcl;"
	runBash(TCLCmd)
	print "Complete"
elif args.subcommand == 'clean':
	CleanCmd = "\
		rm -rf vivado* Zed_Test system.bit hsi* fsbl.elf Build_Reports BOOT.BIN;\
		rm -rf Build_Dir/CUNoC_blob Build_Dir/VER_CTRL_REG;"
	runBash(CleanCmd)
	print "Complete"
