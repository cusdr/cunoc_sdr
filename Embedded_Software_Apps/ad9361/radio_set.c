/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "ad9361_api.h"
#include "parameters.h"
#include "platform.h"
#include "radio_set.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
	FILE *fp_config;
	fp_config = fopen("radio_conf.txt","r");
	char var_txt [255];
	char var_val [255];
	// Default values
	float sr_def = 24; //Default Sampling rate in MHz
	float rxlo_def = 617; //Default Sampling rate in MHz
	float txlo_def = 5500; //Default Sampling rate in MHz
	float rxbw_def = 8; //Default Sampling rate in MHz
	float txbw_def = 1; //Default Sampling rate in MHz
	uint8_t gcm_def = 0; //Defualt gain control mode is Manual
	float rxg_def = 40; //Default Rx gain is 30 dB
	float txa_def = 60; //Default Tx Attenuation is 50 dB
	// Parse Config File
	while(!feof(fp_config)){
		fscanf(fp_config,"%[^\t]\t%[^\n]\n",var_txt,var_val);
		if(strcmp("sr",var_txt)==0){
			sr_def = atof(var_val);
		}
		else if(strcmp("rxlo",var_txt)==0){
			rxlo_def = atof(var_val);
		}
		else if(strcmp("txlo",var_txt)==0){
			txlo_def = atof(var_val);
		}
		else if(strcmp("rxbw",var_txt)==0){
			rxbw_def = atof(var_val);
		}
		else if(strcmp("txbw",var_txt)==0){
			txbw_def = atof(var_val);
		}
		else if(strcmp("gcm",var_txt)==0){
			gcm_def = (uint8_t)(atoi(var_val));
		}
		else if(strcmp("rxg",var_txt)==0){
			rxg_def = atof(var_val);
		}		
		else if(strcmp("txa",var_txt)==0){
			txa_def = atof(var_val);
		}
		else {
		}		
	}
	fclose(fp_config);
	
	int error_val = 0; 
	int32_t atten_check = 0; 
	int32_t samp_check = 0; 
	int32_t rx_lo_check = 0; 
	int32_t tx_lo_check = 0; 
	int32_t rx_bw_check = 0; 
	int32_t tx_bw_check = 0; 
	int32_t gcm_check = 0; 
	int32_t rxg_check = 0; 
	/*// Debug Printouts
	printf("Sr is: %f\n",sr_def);
	printf("Rxlo is: %f\n",rxlo_def);
	printf("Txlo is: %f\n",txlo_def);
	printf("Rxbw is: %f\n",rxbw_def);
	printf("Txbw is: %f\n",txbw_def);
	printf("Gcm is: %d\n",(int)gcm_def);
	printf("Rxg is: %f\n",rxg_def);
	printf("Txa is: %f\n",txa_def);
	*/
	//Make while loop to decode input
	
	// Initialization Steps
	default_init_param.gpio_resetb = GPIO_RESET_PIN;
	default_init_param.gpio_sync = -1;
	default_init_param.gpio_cal_sw1 = -1;
	default_init_param.gpio_cal_sw2 = -1;
	gpio_init(default_init_param.gpio_resetb);
	gpio_direction(default_init_param.gpio_resetb, 1);
	spi_init(SPI_DEVICE_ID, 1, 0);	
	// Make Phy Structure
	struct ad9361_rf_phy *ad9361_phy;
	// Initialize Phy with default params in header
	ad9361_init(&ad9361_phy, &default_init_param);
	//  Set Radio
	atten_check = ad9361_set_tx_attenuation(ad9361_phy,0,(uint32_t)(1000*60));
	atten_check = atten_check + ad9361_set_tx_attenuation(ad9361_phy,1,(uint32_t)(1000*60));// little kludgy
	samp_check = ad9361_set_rx_sampling_freq (ad9361_phy,(uint32_t)(sr_def*1000000));
	samp_check = samp_check + ad9361_set_tx_sampling_freq (ad9361_phy,(uint32_t)(sr_def*1000000));//little kludgy
	rx_lo_check = ad9361_set_rx_lo_freq(ad9361_phy,(uint64_t)(1000000*rxlo_def));
	tx_lo_check = ad9361_set_tx_lo_freq(ad9361_phy,(uint64_t)(1000000*txlo_def));
	rx_bw_check = ad9361_set_rx_rf_bandwidth(ad9361_phy,(uint32_t)(1000000*rxbw_def));
	tx_bw_check = ad9361_set_tx_rf_bandwidth(ad9361_phy,(uint32_t)(1000000*txbw_def));
	gcm_check = ad9361_set_rx_gain_control_mode(ad9361_phy,0,(uint8_t)gcm_def);
	gcm_check = gcm_check + ad9361_set_rx_gain_control_mode(ad9361_phy,1,(uint8_t)gcm_def);
	rxg_check = ad9361_set_rx_rf_gain(ad9361_phy,0,(int32_t)(rxg_def));
	rxg_check = rxg_check + ad9361_set_rx_rf_gain(ad9361_phy,1,(int32_t)(rxg_def));
	atten_check = ad9361_set_tx_attenuation(ad9361_phy,0,(uint32_t)(1000*txa_def));
	atten_check = atten_check + ad9361_set_tx_attenuation(ad9361_phy,1,(uint32_t)(1000*txa_def));// little kludgy
	if(atten_check<0){
		printf("Tx Atten Failed!\n");
	}
	if(samp_check<0){
		printf("Sample Rate Failed!\n");
	}
	if(rx_lo_check<0){
		printf("Rx Lo Failed!\n");
	}
	if(tx_lo_check<0){
		printf("Tx Lo Failed!\n");
	}
	if(rx_bw_check<0){
		printf("Rx BW Failed!\n");
	}
	if(tx_bw_check<0){
		printf("Tx BW Failed!\n");
	}
	if(gcm_check<0){
		printf("GCM Failed!\n");
	}
	if(rxg_check<0){
		printf("Rx Gain Failed!\n");
	}
	if((atten_check+samp_check+rx_lo_check+tx_lo_check+rx_bw_check+tx_bw_check+gcm_check+rxg_check)==0){
		printf("Success!\n");
	}
	return error_val;
};
