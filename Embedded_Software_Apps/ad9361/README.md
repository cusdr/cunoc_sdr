This directory contains software for interfacing with an FMCOMMS3 radio module using the SPI interface. To build the configuation software use:

:# make radio_set

The software is a modified version of the AD9361 driver using the generic configuration and the linux SPI driver. Radio configurations are run by executing:

:#./radio_config

The text file: radio_config.txt contains the desired radio settings. The syntax is self explanatory. 
