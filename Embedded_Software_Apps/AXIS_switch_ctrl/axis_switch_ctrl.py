import sys
import mmap
import struct
import numpy as np 

# Calculate switch configuration 
#-----------START EGRIGOUS COPY AND PASTE--------------------
comp_dict_skel = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
                    
cpu_in = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
cpu_in['Comp Name'] = 'CPU'
cpu_in['Comp Inst Num'] = 0
cpu_in['Slave Ind'].append(0)
cpu_in['Master Switch Ind'].append(0)
cpu_in['Master Ind'].append(0)
cpu_in['Slave Switch Ind'].append(0)

radio_comp1 = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
radio_comp1['Comp Name'] = 'radio_module'
radio_comp1['Comp Inst Num'] = 0
radio_comp1['Slave Ind'].append(0)
radio_comp1['Master Switch Ind'].append(1)
radio_comp1['Master Ind'].append(0)
radio_comp1['Slave Switch Ind'].append(1)

radio_comp2 = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
radio_comp2['Comp Name'] = 'radio_module'
radio_comp2['Comp Inst Num'] = 1
radio_comp2['Slave Ind'].append(0)
radio_comp2['Master Switch Ind'].append(2)
radio_comp2['Master Ind'].append(0)
radio_comp2['Slave Switch Ind'].append(2)

NCI_comp = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
NCI_comp['Comp Name'] = 'NCI'
NCI_comp['Comp Inst Num'] = 0
NCI_comp['Slave Ind'].append(0)
NCI_comp['Master Switch Ind'].append(3)
NCI_comp['Master Ind'].append(0)
NCI_comp['Slave Switch Ind'].append(3)

adder_comp1 = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
adder_comp1['Comp Name'] = 'Adder'
adder_comp1['Comp Inst Num'] = 0
adder_comp1['Slave Ind'].append(0)
adder_comp1['Master Switch Ind'].append(4)
adder_comp1['Master Ind'].append(0)
adder_comp1['Slave Switch Ind'].append(4)

adder_comp2 = {'Comp Name':' ','Comp Inst Num':-1,
                    'Slave Ind':[],'Master Switch Ind':[],
                    'Master Ind':[],'Slave Switch Ind':[]}
adder_comp2['Comp Name'] = 'Adder'
adder_comp2['Comp Inst Num'] = 1
adder_comp2['Slave Ind'].append(0)
adder_comp2['Master Switch Ind'].append(5)
adder_comp2['Master Ind'].append(0)
adder_comp2['Slave Switch Ind'].append(5)

comp_map = []
comp_map.append(cpu_in)
comp_map.append(radio_comp1)
comp_map.append(radio_comp2)
comp_map.append(NCI_comp)
comp_map.append(adder_comp1)
comp_map.append(adder_comp2)

con_dict_skel = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
                 'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}

con1 = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
        'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}
con1['Master Comp Name'] = 'CPU'
con1['Master Comp Inst Num'] = 0
con1['Master Ind'] = 0
con1['Slave Comp Name'] = 'Adder'
con1['Slave Comp Inst Num'] = 0
con1['Slave Ind'] = 0

con2 = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
        'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}
con2['Master Comp Name'] = 'Adder'
con2['Master Comp Inst Num'] = 0
con2['Master Ind'] = 0
con2['Slave Comp Name'] = 'CPU'
con2['Slave Comp Inst Num'] = 0
con2['Slave Ind'] = 0

con3 = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
        'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}
con3['Master Comp Name'] = 'Adder'
con3['Master Comp Inst Num'] = 1
con3['Master Ind'] = 0
con3['Slave Comp Name'] = 'NCI'
con3['Slave Comp Inst Num'] = 0
con3['Slave Ind'] = 0

con4 = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
        'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}
con4['Master Comp Name'] = 'NCI'
con4['Master Comp Inst Num'] = 0
con4['Master Ind'] = 0
con4['Slave Comp Name'] = 'CPU'
con4['Slave Comp Inst Num'] = 0
con4['Slave Ind'] = 0

con5 = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
        'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}
con5['Master Comp Name'] = 'Adder'
con5['Master Comp Inst Num'] = 0
con5['Master Ind'] = 0
con5['Slave Comp Name'] = 'Adder'
con5['Slave Comp Inst Num'] = 1
con5['Slave Ind'] = 0

con6 = {'Master Comp Name':' ','Master Comp Inst Num':-1, 'Master Ind':-1,
        'Slave Comp Name':' ','Slave Comp Inst Num':-1, 'Slave Ind':-1}
con6['Master Comp Name'] = 'Adder'
con6['Master Comp Inst Num'] = 1
con6['Master Ind'] = 0
con6['Slave Comp Name'] = 'radio_module'
con6['Slave Comp Inst Num'] = 1
con6['Slave Ind'] = 0

con_dict_list = []
con_dict_list.append(con1)
con_dict_list.append(con2)
'''
con_dict_list.append(con3)
con_dict_list.append(con4)
con_dict_list.append(con5)
con_dict_list.append(con6)
'''

con_tuple_list = []
for kk in con_dict_list:
    master_name_tmp = kk['Master Comp Name']
    master_inst_tmp = kk['Master Comp Inst Num']
    master_ind_tmp = kk['Master Ind']
    for jj in comp_map:
        if((jj['Comp Name']==master_name_tmp) and (jj['Comp Inst Num']==master_inst_tmp)):
            ind_lu = jj['Master Ind'].index(con1['Master Ind']) 
            switch_slave_ind = jj['Slave Switch Ind'][ind_lu]
            
    slave_name_tmp = kk['Slave Comp Name']
    slave_inst_tmp = kk['Slave Comp Inst Num']
    slave_ind_tmp = kk['Slave Ind']
    for jj in comp_map:
        if((jj['Comp Name']==slave_name_tmp) and (jj['Comp Inst Num']==slave_inst_tmp)):
            ind_lu = jj['Slave Ind'].index(con1['Slave Ind']) 
            switch_master_ind = jj['Master Switch Ind'][ind_lu]
            
    switch_tuple = (switch_slave_ind,switch_master_ind)
    con_tuple_list.append(switch_tuple)
            
print "Switch Tuple Format: (slave,master)"
print con_tuple_list

num_master = 6
num_slave = 6
master_arr = np.zeros(num_master)
slave_arr = np.zeros(num_slave)
master_arr = master_arr.astype(dtype='int32')
slave_arr = slave_arr.astype(dtype='int32')

con_list = con_tuple_list
slave_en = 0
master_en = 0
for jj in con_list:
    slave_en = slave_en+pow(2,jj[0])
    master_en = master_en+pow(2,jj[1])
    slave_arr[jj[0]] = jj[1]
    master_arr[jj[1]] = jj[0]
    
print "Slave en: "+str(slave_en)
print "Master en: "+str(master_en)
print "Master Array:"
print master_arr
print "Slave Array:"
print slave_arr
#-----------STOP EGRIGOUS COPY AND PASTE---------------------

# Open and MMAP UIO Device
map_size = 16384
switch_uio_file = open('/dev/uio0','rb+')
mmapd_file = mmap.mmap(switch_uio_file.fileno(),map_size)
verreg_uio_file = open('/dev/uio1','rb+')
verreg_mmapd = mmap.mmap(verreg_uio_file.fileno(),map_size)
# Helper Function 
def write_val(file_in,offset,val):
    file_in.seek(offset*4)
    write_bytes = struct.pack('i',int(val))
    file_in.write(write_bytes)

# Helper Function 
def write_array(file_in,offset,arr):
    file_in.seek(offset*4)
    write_bytes = arr.tostring()
    file_in.write(write_bytes)


num_master = 6
num_slave = 6

upload_offset = 0
master_enable_offset = 1
slave_enable_offset = 2
mux_array_offset = 3

# Begin Upload
write_val(mmapd_file,upload_offset,1)

# Master Enable 
master_en_val = master_en
write_val(mmapd_file,master_enable_offset,master_en_val)

# Slave Enable 
slave_en_val = slave_en
write_val(mmapd_file,slave_enable_offset,slave_en_val)

# Write arrays
master_mux = np.array(master_arr,dtype='int32')
slave_mux = np.array(slave_arr,dtype='int32')
conc_arr = np.concatenate((master_mux,slave_mux))
write_array(mmapd_file,mux_array_offset,conc_arr)

# Finish Upload
write_val(mmapd_file,upload_offset,0)

# Read contents 
mmapd_file.seek(upload_offset*4)
read_val_byte_buff = mmapd_file.read(4*(3+num_master+num_slave))
#read_val_byte_buff = bytearray(read_val_byte_buff)
read_val = np.frombuffer(read_val_byte_buff,dtype=np.dtype('int32'))
print "AXIS Switch Contents"
print read_val

verreg_mmapd.seek(0*4)
read_val_byte_buff = verreg_mmapd.read(4*4)
read_val = np.frombuffer(read_val_byte_buff,dtype=np.dtype('int32'))
print "Version Control Reg Contents:"
print read_val

mmapd_file.close()
verreg_mmapd.close()
switch_uio_file.close()
verreg_uio_file.close()

