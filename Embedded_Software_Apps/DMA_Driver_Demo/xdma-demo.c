// the below define is a hack
#define u32 unsigned int
#define dma_cookie_t unsigned int
#include "xdma.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <string.h>
#include <math.h>

#define FILEPATH "/dev/xdma"
#define MAP_SIZE  4*1024*1024 // AA COMMENT: Unclear regarding the sizing of this number vs. the size of LENGTH. This will cause errors and weird behaviour if set wrong. 
#define FILESIZE (MAP_SIZE * sizeof(unsigned long long int)) // AA COMMENT: So scale map size by the number of bytes in our atomic division. This is used for mmaping

double time_diff(struct timeval x , struct timeval y)
{
    double x_us , y_us , diff;
     
    x_us = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_us = (double)y.tv_sec*1000000 + (double)y.tv_usec;
     
    diff = (double)y_us - (double)x_us;
     
    return diff;
}




int main(int argc, char *argv[])
{
	int LENGTH = 1*512*1024; 		 
	int i;
	int fd;
	unsigned long long int *map;
	double mb_scalar = ((double)(sizeof(unsigned long long int)))*1000000.0/(1024.0*1024.0); 
	// Timing Variables
	struct timeval timer_0, timer_1, timer_2, timer_3, timer_4;
	double seconds;
	
	gettimeofday(&timer_0,NULL);
		
	// Load Data
	srand(timer_0.tv_sec*1000+(timer_0.tv_usec/1000));
	FILE *fp_t;
	fp_t = fopen("test.bin","r");
	unsigned long long int *data_load = malloc(sizeof(unsigned long long int)*LENGTH); 
	unsigned long long int *data_out = malloc(sizeof(unsigned long long int)*LENGTH);
	fread(data_load,sizeof(unsigned long long int),LENGTH,fp_t);
	//fclose(fp_t);
	
	gettimeofday(&timer_1,NULL);
	seconds = time_diff(timer_0,timer_1);
	printf ("Time to Load: %f us, %f MB/s \n", seconds, mb_scalar*LENGTH/seconds);
		

	// Setup Save File
	FILE *fd_vals;
	fd_vals = fopen("data_demo.bin","w");
	//unsigned long long int *d_arr = malloc(sizeof(unsigned long long int)*LENGTH*2); 

	//unsigned long d_in = 0; 
	// End AA Additions
	/* Open a file for writing.
	 *  - Creating the file if it doesn't exist.
	 *  - Truncating it to 0 size if it already exists. (not really needed)
	 *
	 * Note: "O_WRONLY" mode is not sufficient when mmaping.
	 */
	 // AA COMMENT: Ok so this opens /dev/xdma and sets permissions and checks for an error

	fd = open(FILEPATH, O_RDWR | O_CREAT | O_TRUNC, (mode_t) 0600);
	if (fd == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}
	
	
	/* mmap the file to get access to the memory area.
	 */
	 // AA COMMENT: The meat of the thing. This actually runs the MMAP call.
	 // FILESIZE comes from that first map-size call. Not sure what MAP_SHARED does...
	 // 
	gettimeofday(&timer_1,NULL);
	map = mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (map == MAP_FAILED) {
		close(fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}
	//gettimeofday(&timer_1,NULL);
	// Load CPU -> FPGA Buffer (offset of Length)
	//fread(&map[LENGTH],sizeof(unsigned long long int),LENGTH,fp_t);
	memcpy(&map[LENGTH],&data_load[0],sizeof(unsigned long long int)*LENGTH);
	// Load FPGA -> CPU Buffer (offest of 0)
	//memset(&map[0],0,sizeof(unsigned long long int)*LENGTH);
	
	//map[LENGTH - 1] = '\n';
	//map[LENGTH + LENGTH - 1] = '\n';

	gettimeofday(&timer_2,NULL);
	
	//seconds = time_diff(timer_1,timer_2);
	//printf ("Time to Buf: %f us, %f MB/s \n", seconds, mb_scalar*LENGTH/seconds);
	/*
	printf("test: rx buffer before transmit:\n");
	for (i = 0; i < 20; i++) {
		printf("%d\t", (int)map[i+LENGTH]);
	}

	printf("\n");*/
	// AA COMMENT: Time stamp for performance benchmarking.
	//gettimeofday(&timer_1,NULL);  
	/* Query driver for number of devices.
	 */
	// AA COMMENT: Ok so this asks the driver how many DMAs we have behind it. 
	// Uses ioctl to check the /dev/xdma file for info and returns an error if there was one.
	int num_devices = 0;
	if (ioctl(fd, XDMA_GET_NUM_DEVICES, &num_devices) < 0) {
		perror("Error ioctl getting device num");
		exit(EXIT_FAILURE);
	}
	// AA COMMENT: Commented out for performance purposes, but would tell you how many DMAs there are.
	//printf("Number of devices: %d\n", num_devices);

	/* Query driver for number of devices.
	 */
	// AA COMMENT: ok so this one is looking at specific DMA channel info where the other one was how many
	// DMA engines exist in firmware that we can tell. This one is like whats going on in the one device. 
	struct xdma_dev dev;
	dev.tx_chan = (u32) NULL;
	dev.tx_cmp = (u32) NULL;
	dev.rx_chan = (u32) NULL;
	dev.rx_cmp = (u32) NULL;
	dev.device_id = num_devices - 1;
	if (ioctl(fd, XDMA_GET_DEV_INFO, &dev) < 0) {
		perror("Error ioctl getting device info");
		exit(EXIT_FAILURE);
	}

	// AA COMMENT: so this would report all that specific info on device channel and completion.
	// That device structure is used throughout the configuration and transfers.
	//printf("devices tx chan: %x, tx cmp:%x, rx chan: %x, rx cmp: %x\n",
	//     dev.tx_chan, dev.tx_cmp, dev.rx_chan, dev.rx_cmp);
	// AA COMMENT: so this section runs rx configuration 
	struct xdma_chan_cfg rx_config;
	rx_config.chan = dev.rx_chan;
	rx_config.dir = XDMA_DEV_TO_MEM; // Direction I think so go from FPGA Device to RAM memory
	rx_config.coalesc = 1; // Set it to coalesce
	rx_config.delay = 0;
	rx_config.reset = 0; // Wonder what these resets do? 
	if (ioctl(fd, XDMA_DEVICE_CONTROL, &rx_config) < 0) {
		perror("Error ioctl config rx chan");
		exit(EXIT_FAILURE);
	}
	//printf("config rx chans\n");
    // AA COMMENT: Same thing but for tx now. 
	struct xdma_chan_cfg tx_config;
	tx_config.chan = dev.tx_chan;
	tx_config.dir = XDMA_MEM_TO_DEV; // so direction is changed, makes sense we're transmitting (transferring?) from our buffer in RAM to the FPGA device.
	tx_config.coalesc = 1; // Same settings, still wonder about the tx resets...
	tx_config.delay = 0;
	tx_config.reset = 0;
	if (ioctl(fd, XDMA_DEVICE_CONTROL, &tx_config) < 0) {
		perror("Error ioctl config tx chan");
		exit(EXIT_FAILURE);
	}

	//printf("config tx chans\n");
	// AA COMMENT: So this configures receive buffers. Has the classic cookie structure thing. 
	struct xdma_buf_info rx_buf;
	rx_buf.chan = dev.rx_chan;
	rx_buf.completion = dev.rx_cmp;
	rx_buf.cookie = (u32) NULL;
	rx_buf.buf_offset = (u32) 0;
	rx_buf.buf_size = (u32) 8*LENGTH; // Ok so had to add these. Think the original assumed it was one byte wide. So as we're at 64 bits it's 1->8
	rx_buf.dir = XDMA_DEV_TO_MEM; // Same recieve direction 
	if (ioctl(fd, XDMA_PREP_BUF, &rx_buf) < 0) {
		perror("Error ioctl set rx buf");
		exit(EXIT_FAILURE);
	}
	//printf("config rx buffer\n");
    // AA COMMENT: Same thing now but for tx. This one actually has an offset
	struct xdma_buf_info tx_buf;
	tx_buf.chan = dev.tx_chan;
	tx_buf.completion = dev.tx_cmp;
	tx_buf.cookie = (u32) NULL;
	tx_buf.buf_offset = (u32) 8*LENGTH; // So this seems to be where we'd have to do adjustments on transmit size 
	tx_buf.buf_size = (u32) 8*LENGTH; // AA COMMENT: Not sure about the (u32) calls. Clearly that's what the offset/size configs are looking for is u32....just not sure about 
									  // what dictates that lengthx8
	tx_buf.dir = XDMA_MEM_TO_DEV;
	if (ioctl(fd, XDMA_PREP_BUF, &tx_buf) < 0) {
		perror("Error ioctl set tx buf");
		exit(EXIT_FAILURE);
	}

	//printf("config tx buffer\n");
	//AA COMMENT: ooooh the actually transfer call! Looks like the rx call. 
	// So its still using that dev call from above and now the cookie from the buffer
	// configurations. There's a wait call. Might be able to use that? 
	struct xdma_transfer rx_trans;
	rx_trans.chan = dev.rx_chan;
	rx_trans.completion = dev.rx_cmp;
	rx_trans.cookie = rx_buf.cookie;
	rx_trans.wait = 0;
	if (ioctl(fd, XDMA_START_TRANSFER, &rx_trans) < 0) {
		perror("Error ioctl start rx trans");
		exit(EXIT_FAILURE);
	}
	//printf("config rx trans\n");
	// AA COMMENT: same thing and now we're setting up the tx transfer. 
	struct xdma_transfer tx_trans;
	tx_trans.chan = dev.tx_chan;
	tx_trans.completion = dev.tx_cmp;
	tx_trans.cookie = tx_buf.cookie;
	tx_trans.wait = 0;
	if (ioctl(fd, XDMA_START_TRANSFER, &tx_trans) < 0) {
		perror("Error ioctl start tx trans");
		exit(EXIT_FAILURE);
	}

	gettimeofday(&timer_3,NULL);
	
	memcpy(&data_out[0],&map[0],sizeof(unsigned long long int)*LENGTH);

	gettimeofday(&timer_4,NULL);
	//seconds = time_diff(timer_0,timer_1);
	//printf ("Time to Transfer: %f us, %f MB/s \n", seconds, mb_scalar*LENGTH/seconds);

	printf("test: rx buffer after transmit:\n");
	for (i = 0; i < 128; i++) {
		printf("%d\t", (int)data_out[i]);
	}
	printf("\n");
	

	//memcpy(d_arr,map,sizeof(unsigned long long int)*2*LENGTH);
	fwrite(map, sizeof(unsigned long long int),2*LENGTH,fd_vals);
	
	fclose(fd_vals);

	gettimeofday(&timer_4,NULL);
	
	//seconds = time_diff(timer_0,timer_1);
	//printf ("Fread Time: %f us, %f MB/s \n", seconds, mb_scalar*LENGTH/seconds);
	
	seconds = time_diff(timer_1,timer_2);
	printf("Map and copy time: %f us, %f MB/s \n",seconds, mb_scalar*LENGTH/seconds);

	seconds = time_diff(timer_2,timer_3);
	printf("DMA Transfer time: %f us, %f MB/s \n", seconds, mb_scalar*LENGTH/seconds);

	seconds = time_diff(timer_0, timer_4);
	printf("Total time: %f us, %f MB/s \n", seconds, mb_scalar*LENGTH/seconds);

	/* Don't forget to free the mmapped memory
	 */
	if (munmap(map, FILESIZE) == -1) {
		perror("Error un-mmapping the file");
		/* Decide here whether to close(fd) and exit() or not. Depends... */
	}
	
	free(data_load);

	/* Un-mmaping doesn't close the file, so we still need to do that.
	 */
	fclose(fp_t);
	close(fd);
	return 0;
}
