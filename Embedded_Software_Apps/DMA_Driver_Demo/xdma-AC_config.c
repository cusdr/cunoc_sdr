// the below define is a hack
#define u32 unsigned int
#define dma_cookie_t unsigned int
#include "xdma.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <time.h>
#include <math.h>

#define FILEPATH "/dev/xdma"
#define MAP_SIZE  (2000) // AA COMMENT: Unclear regarding the sizing of this number vs. the size of LENGTH. This will cause errors and weird behaviour if set wrong. 
#define FILESIZE (MAP_SIZE * sizeof(unsigned long long int)) // AA COMMENT: So scale map size by the number of bytes in our atomic division. This is used for mmaping

int main(int argc, char *argv[])
{
	const int LENGTH = 512; // AA COMMENT: Again, unclear about how this and MAP_SIZE play with each other...
									 // AA COMMENT: This values seems to be restricted to powers of 2
	int i;
	int fd;
	unsigned long long int *map;		/* mmapped array of unsigned long long */ //AA COMMENT: Changed this to give us 64 bit control 
	// AA Additions
	// Load I,Q Data
	FILE *fp_I;
	FILE *fp_Q;
	fp_I = fopen("filter_I.bin","r");
	fp_Q = fopen("filter_Q.bin","r");
	int I_data[LENGTH];
	int Q_data[LENGTH];
	fread(I_data,sizeof(I_data),sizeof(I_data[0]),fp_I);
	fread(Q_data,sizeof(Q_data),sizeof(Q_data[0]),fp_Q);
	fclose(fp_I);
	fclose(fp_Q);
	// Timing Variables
	struct timeval timer_0;
	struct timeval timer_cur;
	struct timeval timer_cur_dev_querry;
	struct timeval timer_cur_chan_config;
	struct timeval timer_cur_buff_config;
	struct timeval timer_cur_txrx;
	
	struct tm y2k;
	double x_ms;
	double y_ms;
	double init_us;
	double quer_us;
	double chan_us;
	double buff_us;
	double tran_us;
	double diff_time_rx_load;
	double diff_time_quer;
	double diff_time_chan;
	double diff_time_buff;
	double diff_time_tran;
	double diff_time_full_txrx;

	// Setup Save Files
	FILE *fd_vals;
	fd_vals = fopen("data_demo.bin","w");
	long d_arr[2*LENGTH];
	// Setup for sending I,Q in
	unsigned long I_bitmask = 0xFFFF0000;
	int I_bit_shift = 16;
	unsigned long Q_bitmask = 0x0000FFFF;
	int Q_bit_shift = 0;
	unsigned long d_in = 0; 
	// End AA Additions
	/* Open a file for writing.
	 *  - Creating the file if it doesn't exist.
	 *  - Truncating it to 0 size if it already exists. (not really needed)
	 *
	 * Note: "O_WRONLY" mode is not sufficient when mmaping.
	 */
	 // AA COMMENT: Ok so this opens /dev/xdma and sets permissions and checks for an error
	fd = open(FILEPATH, O_RDWR | O_CREAT | O_TRUNC, (mode_t) 0600);
	if (fd == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	/* mmap the file to get access to the memory area.
	 */
	 // AA COMMENT: The meat of the thing. This actually runs the MMAP call.
	 // FILESIZE comes from that first map-size call. Not sure what MAP_SHARED does...
	 // 
	map = mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (map == MAP_FAILED) {
		close(fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	/* Now write int's to the file as if it were memory (an array of ints).
	 */
	gettimeofday(&timer_0,NULL);
	// AA Comments: So this step can be very time consuming and scales linearly with time. Need to figure out how to speed this section up!
	// fill tx with a value
	for (i = 0; i < LENGTH; i++) {
		// AA COMMENT: d_in call for putting in our IQ files
		d_in = ((I_data[i] << I_bit_shift) & I_bitmask) |
			   ((Q_data[i] << Q_bit_shift) & Q_bitmask);
		map[LENGTH + i] = d_in;
	}
	gettimeofday(&timer_cur,NULL);
	x_ms = (double)timer_0.tv_sec*1000000+(double)timer_0.tv_usec;
	y_ms = (double)timer_cur.tv_sec*1000000+(double)timer_cur.tv_usec;
	diff_time_rx_load = y_ms-x_ms;
	// AA COMMENT: Pulled this call. Don't think it was really doing anything  
	//map[LENGTH + LENGTH - 1] = '\n';
	// AA COMMENT: Not sure this step is stricly neccessary. Maybe just to clear the buffer.
	// AA COMMENT: Is there a better way in c to transfer large portions of memory rapidly instead of this serial call bullshit? 
	// fill rx with a value
	for (i = 0; i < LENGTH; i++) {
		map[i] = 0;
	}
	// AA COMMENT: Again pulled this, didn't seem like it was doing anything. Also pretty sure Xilnx did not support null or position bytes from the AXI standard
	// which I think this is related to. 
	//map[LENGTH - 1] = '\n';
	// AA COMMENT: Look at the receive buffer. LOOK AT IT!
	printf("test: rx buffer before transmit:\n");
	for (i = 0; i < 20; i++) {
		printf("%d\t", (int)map[i]);
	}
	printf("\n");
	// AA COMMENT: Time stamp for performance benchmarking.
	gettimeofday(&timer_0,NULL);  
	/* Query driver for number of devices.
	 */
	// AA COMMENT: Ok so this asks the driver how many DMAs we have behind it. 
	// Uses ioctl to check the /dev/xdma file for info and returns an error if there was one.
	int num_devices = 0;
	if (ioctl(fd, XDMA_GET_NUM_DEVICES, &num_devices) < 0) {
		perror("Error ioctl getting device num");
		exit(EXIT_FAILURE);
	}
	// AA COMMENT: Commented out for performance purposes, but would tell you how many DMAs there are.
	//printf("Number of devices: %d\n", num_devices);

	/* Query driver for number of devices.
	 */
	// AA COMMENT: ok so this one is looking at specific DMA channel info where the other one was how many
	// DMA engines exist in firmware that we can tell. This one is like whats going on in the one device. 
	struct xdma_dev dev;
	dev.tx_chan = (u32) NULL;
	dev.tx_cmp = (u32) NULL;
	dev.rx_chan = (u32) NULL;
	dev.rx_cmp = (u32) NULL;
	dev.device_id = num_devices - 1;
	if (ioctl(fd, XDMA_GET_DEV_INFO, &dev) < 0) {
		perror("Error ioctl getting device info");
		exit(EXIT_FAILURE);
	}
	gettimeofday(&timer_cur_dev_querry,NULL);
	// AA COMMENT: so this would report all that specific info on device channel and completion.
	// That device structure is used throughout the configuration and transfers.
	//printf("devices tx chan: %x, tx cmp:%x, rx chan: %x, rx cmp: %x\n",
	//     dev.tx_chan, dev.tx_cmp, dev.rx_chan, dev.rx_cmp);
	// AA COMMENT: so this section runs rx configuration 
	struct xdma_chan_cfg rx_config;
	rx_config.chan = dev.rx_chan;
	rx_config.dir = XDMA_DEV_TO_MEM; // Direction I think so go from FPGA Device to RAM memory
	rx_config.coalesc = 1; // Set it to coalesce
	rx_config.delay = 0;
	rx_config.reset = 0; // Wonder what these resets do? 
	if (ioctl(fd, XDMA_DEVICE_CONTROL, &rx_config) < 0) {
		perror("Error ioctl config rx chan");
		exit(EXIT_FAILURE);
	}
	//printf("config rx chans\n");
    // AA COMMENT: Same thing but for tx now. 
	struct xdma_chan_cfg tx_config;
	tx_config.chan = dev.tx_chan;
	tx_config.dir = XDMA_MEM_TO_DEV; // so direction is changed, makes sense we're transmitting (transferring?) from our buffer in RAM to the FPGA device.
	tx_config.coalesc = 1; // Same settings, still wonder about the tx resets...
	tx_config.delay = 0;
	tx_config.reset = 0;
	if (ioctl(fd, XDMA_DEVICE_CONTROL, &tx_config) < 0) {
		perror("Error ioctl config tx chan");
		exit(EXIT_FAILURE);
	}
	gettimeofday(&timer_cur_chan_config,NULL);
	//printf("config tx chans\n");
	// AA COMMENT: So this configures receive buffers. Has the classic cookie structure thing. 
	struct xdma_buf_info rx_buf;
	rx_buf.chan = dev.rx_chan;
	rx_buf.completion = dev.rx_cmp;
	rx_buf.cookie = (u32) NULL;
	rx_buf.buf_offset = (u32) 0;
	rx_buf.buf_size = (u32) 8*LENGTH; // Ok so had to add these. Think the original assumed it was one byte wide. So as we're at 64 bits it's 1->8
	rx_buf.dir = XDMA_DEV_TO_MEM; // Same recieve direction 
	if (ioctl(fd, XDMA_PREP_BUF, &rx_buf) < 0) {
		perror("Error ioctl set rx buf");
		exit(EXIT_FAILURE);
	}
	//printf("config rx buffer\n");
    // AA COMMENT: Same thing now but for tx. This one actually has an offset
	struct xdma_buf_info tx_buf;
	tx_buf.chan = dev.tx_chan;
	tx_buf.completion = dev.tx_cmp;
	tx_buf.cookie = (u32) NULL;
	tx_buf.buf_offset = (u32) 8*LENGTH; // So this seems to be where we'd have to do adjustments on transmit size 
	tx_buf.buf_size = (u32) 8*LENGTH; // AA COMMENT: Not sure about the (u32) calls. Clearly that's what the offset/size configs are looking for is u32....just not sure about 
									  // what dictates that lengthx8
	tx_buf.dir = XDMA_MEM_TO_DEV;
	if (ioctl(fd, XDMA_PREP_BUF, &tx_buf) < 0) {
		perror("Error ioctl set tx buf");
		exit(EXIT_FAILURE);
	}
	gettimeofday(&timer_cur_buff_config,NULL);
	//printf("config tx buffer\n");
	//AA COMMENT: ooooh the actually transfer call! Looks like the rx call. 
	// So its still using that dev call from above and now the cookie from the buffer
	// configurations. There's a wait call. Might be able to use that? 
	struct xdma_transfer rx_trans;
	rx_trans.chan = dev.rx_chan;
	rx_trans.completion = dev.rx_cmp;
	rx_trans.cookie = rx_buf.cookie;
	rx_trans.wait = 0;
	if (ioctl(fd, XDMA_START_TRANSFER, &rx_trans) < 0) {
		perror("Error ioctl start rx trans");
		exit(EXIT_FAILURE);
	}
	//printf("config rx trans\n");
	// AA COMMENT: same thing and now we're setting up the tx transfer. 
	struct xdma_transfer tx_trans;
	tx_trans.chan = dev.tx_chan;
	tx_trans.completion = dev.tx_cmp;
	tx_trans.cookie = tx_buf.cookie;
	tx_trans.wait = 0;
	if (ioctl(fd, XDMA_START_TRANSFER, &tx_trans) < 0) {
		perror("Error ioctl start tx trans");
		exit(EXIT_FAILURE);
	}
	gettimeofday(&timer_cur_txrx,NULL);	
	//  AA COMMENT: so at this stage the transfer has occured. 
	// AA COMMENT: So I think the way to do this would be to load the entire file of interest in RAM and chunk it out 1024 at a time with small wait periods 
	// in between. Easy to say, not sure how easy it'd be to do? 
	// The way to do it, I think, would be to run the full map to begin with, loading the full transmit into map to begin with. Then chunk through that 
	// by advancing the offsets in the buffer calls. So that could get us sliding through, I think. 
	// could try that?
	
	//printf("config tx trans\n");
	gettimeofday(&timer_cur,NULL);
	init_us = (double)timer_0.tv_sec*1000000+(double)timer_0.tv_usec;
	quer_us = (double)timer_cur_dev_querry.tv_sec*1000000+(double)timer_cur_dev_querry.tv_usec;
	chan_us = (double)timer_cur_chan_config.tv_sec*1000000+(double)timer_cur_chan_config.tv_usec;
	buff_us = (double)timer_cur_buff_config.tv_sec*1000000+(double)timer_cur_buff_config.tv_usec;
	tran_us = (double)timer_cur_txrx.tv_sec*1000000+(double)timer_cur_txrx.tv_usec;
    diff_time_quer = quer_us-init_us;
	diff_time_chan = chan_us-quer_us;
	diff_time_buff = buff_us-chan_us;
	diff_time_tran = tran_us-buff_us;
	diff_time_full_txrx = diff_time_quer+diff_time_chan+diff_time_buff+diff_time_tran;

	printf("Load time: %f us\n",diff_time_rx_load);
	printf("Querry time: %f us\n",diff_time_quer);
	printf("Chan config time: %f us\n",diff_time_chan);
	printf("Buff config time: %f us\n",diff_time_buff);
	printf("Trans config time: %f us\n",diff_time_tran);
	printf("Full transfer time: %f us\n",diff_time_full_txrx);
	printf("test: rx buffer after transmit:\n");
	for (i = 0; i < 20; i++) {
		printf("%d\t", (int)map[i]);
	}
	printf("\n");
	for (i = 0; i < 2*LENGTH; i++) {
		d_arr[i] = (long)map[i];
	}
	fwrite(d_arr, sizeof(d_arr[0]),2*LENGTH,fd_vals);
	fclose(fd_vals);
#if 0
	for (i = 0; i < MAP_SIZE; i++) {
		printf("%d\t", map[i]);
	}
	printf("\n");
#endif

	/* Don't forget to free the mmapped memory
	 */
	if (munmap(map, FILESIZE) == -1) {
		perror("Error un-mmapping the file");
		/* Decide here whether to close(fd) and exit() or not. Depends... */
	}

	/* Un-mmaping doesn't close the file, so we still need to do that.
	 */
	close(fd);
	return 0;
}
