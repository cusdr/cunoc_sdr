#include "libxdma.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	const int LENGTH = 2048;
	int i;
	unsigned long I_bitmask = 0xFFFF0000;
	int I_bit_shift = 16;
	unsigned long Q_bitmask = 0x0000FFFF;
	int Q_bit_shift = 0;
	//unsigned long Mag_bitmask = 0x0000FFFF;
	//int Mag_bit_shift = 0;
	uint32_t *src;
	uint32_t *dst;
	// File Saving
	FILE *fd_in;
	FILE *fd_out;
	int I_dum = 'A';
	int Q_dum = 'D';
	unsigned long d_in_arr[LENGTH];
	unsigned long d_out_arr[LENGTH];
	unsigned long d_in = ((I_dum << I_bit_shift) & I_bitmask) |
		   ((Q_dum << Q_bit_shift) & Q_bitmask);
	if (xdma_init() < 0) {
		exit(EXIT_FAILURE);
	}

	dst = (uint32_t *) xdma_alloc(LENGTH, sizeof(uint32_t));
	src = (uint32_t *) xdma_alloc(LENGTH, sizeof(uint32_t));
	printf("%X\n",I_dum);
	printf("%X\n",Q_dum);
	printf("%X\n",I_bitmask);
	printf("%X\n",Q_bitmask);
	printf("%X\n",d_in);
	printf("%X\n",(I_dum << I_bit_shift));
	
	
	int ii = 0;
	// fill src with a value
	for (i = 0; i < LENGTH; i++) {
		if((i%2)==0){
			/*if(ii<32){
				src[i] = 0;
			}
			else{
				src[i] = ii-32;
			}*/
			src[i] = ii;
			ii++;
		}
		else{
		    src[i] = 0;
		}
		//src[i] = i;
	}
	//src[LENGTH - 2] = '\n';//???
	src[LENGTH - 1] = '\n';//???
	// fill dst with a value
	for (i = 0; i < LENGTH; i++) {
		dst[i] = 'A';
	}
	//dst[LENGTH - 2] = '\n';//???
	dst[LENGTH - 1] = '\n';//???
	ii = 0;
	for (i = 0; i < LENGTH; i++) {
		d_in_arr[i] = (unsigned long)src[i];
	}
	fd_in = fopen("data_in.bin","w");
	fwrite(d_in_arr, sizeof(d_in_arr[0]),LENGTH,fd_in);
	fclose(fd_in);
	// Run the Transfer
	if (0 < xdma_num_of_devices()) {
		xdma_perform_transaction(0, XDMA_WAIT_NONE, src, LENGTH, dst,LENGTH);
	}
	ii = 0; 
	for (i = 0; i < LENGTH; i++) {
		d_out_arr[i] = (unsigned long)dst[i];
	}
	fd_out = fopen("data_out.bin","w");
	fwrite(d_out_arr, sizeof(d_out_arr[0]),LENGTH,fd_out);
	fclose(fd_out);
	printf("data in:\n");
	for (i = 0; i < 20; i++) {
		printf("%d\t", d_in_arr[i]);
	}
	printf("\n");
	printf("data out:\n");
	for (i = 0; i < 20; i++) {
		printf("%d\t", d_out_arr[i]);
	}
	printf("\n");
	xdma_exit();
	return 0;
}
