Myy_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
Myy_AXIS_TVALID : out std_logic;
Myy_AXIS_TREADY : in  std_logic;
Myy_AXIS_TLAST  : out std_logic;
Myy_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
