Sxx_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
Sxx_AXIS_TVALID : in  std_logic;
Sxx_AXIS_TREADY : out std_logic;
Sxx_AXIS_TLAST  : in  std_logic;
Sxx_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
