-- Open Source AXI4 Stream Switch
-- CU NoC Template for IP Generation 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.math_real.all;

entity CUNoC_IP is 
    generic (--CUNoC: Generics Start
             --AXIS Stream Params
             AXI_DATA_WIDTH     : integer := 64;
             --AXIS Switch Params
             NUM_SLAVE          : integer := 6; 
             NUM_MASTER         : integer := 6; 
             NUM_REG            : integer := 15; 
             --AXI Lite Params
             C_S_AXI_DATA_WIDTH : integer := 32; 
             C_S_AXI_ADDR_WIDTH : integer := 6;
             --Radio Params
             IQ_DATA_WIDTH      : integer := 12;
             RAD_FIFO_DEPTH     : integer := 12;
             TLAST_PER          : integer := 1048576);
             --CUNoC: Generics End
    port(
     -- AXIS Clocks/Resets
     AXIS_ACLK     : in  std_logic;
     AXIS_RESETN   : in  std_logic;
     DSP_CLK       : in  std_logic;
     -- AXIS Slave Interface0
     S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
     S0_AXIS_TVALID : in  std_logic;
     M0_AXIS_TREADY : in  std_logic;
     S0_AXIS_TLAST  : in  std_logic;
     S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
     -- AXIS Master Interface0 
     M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
     M0_AXIS_TVALID : out std_logic;
     S0_AXIS_TREADY : out std_logic;
     M0_AXIS_TLAST  : out std_logic;
     M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0); 
     -- AXI Lite Slave Interface For Setting Control
     S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
     S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
     S_AXI_AWVALID : in std_logic;
     S_AXI_AWREADY : out std_logic;
     S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  
     S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
     S_AXI_WVALID  : in std_logic;
     S_AXI_WREADY  : out std_logic;
     S_AXI_BRESP   : out std_logic_vector(1 downto 0);
     S_AXI_BVALID  : out std_logic;
     S_AXI_BREADY  : in std_logic;
     S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
     S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
     S_AXI_ARVALID : in std_logic;
     S_AXI_ARREADY : out std_logic;
     S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
     S_AXI_RRESP   : out std_logic_vector(1 downto 0);
     S_AXI_RVALID  : out std_logic;
     S_AXI_RREADY  : in std_logic);
end CUNoC_IP;

architecture behavior of CUNoC_IP is 
    --CUNoC: Default Constants, Components, Signals Begin
    
    --CUNoC: Default Component0: AXIS_SWITCH_TOP Begin
    component AXIS_SWITCH_TOP is 
        generic (AXI_DATA_WIDTH     : integer := AXI_DATA_WIDTH;
                 NUM_SLAVE          : integer := NUM_SLAVE; 
                 NUM_MASTER         : integer := NUM_MASTER; 
                 NUM_REG            : integer := NUM_REG; 
                 C_S_AXI_DATA_WIDTH : integer := C_S_AXI_DATA_WIDTH;
                 C_S_AXI_ADDR_WIDTH : integer := C_S_AXI_ADDR_WIDTH);
        port(AXIS_ACLK     : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             S_AXIS_TDATA  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
             S_AXIS_TVALID : in  std_logic_vector(NUM_SLAVE-1 downto 0);
             M_AXIS_TREADY : in  std_logic_vector(NUM_MASTER-1 downto 0);
             S_AXIS_TLAST  : in  std_logic_vector(NUM_SLAVE-1 downto 0);
             S_AXIS_TKEEP  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
             M_AXIS_TDATA  : out std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH-1 downto 0);
             M_AXIS_TVALID : out std_logic_vector(NUM_MASTER-1 downto 0);
             S_AXIS_TREADY : out std_logic_vector(NUM_SLAVE-1 downto 0);
             M_AXIS_TLAST  : out std_logic_vector(NUM_MASTER-1 downto 0);
             M_AXIS_TKEEP  : out std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH/8-1 downto 0);
             S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
             S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
             S_AXI_AWVALID : in std_logic;
             S_AXI_AWREADY : out std_logic;
             S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  
             S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
             S_AXI_WVALID  : in std_logic;
             S_AXI_WREADY  : out std_logic;
             S_AXI_BRESP   : out std_logic_vector(1 downto 0);
             S_AXI_BVALID  : out std_logic;
             S_AXI_BREADY  : in std_logic;
             S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
             S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
             S_AXI_ARVALID : in std_logic;
             S_AXI_ARREADY : out std_logic;
             S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
             S_AXI_RRESP   : out std_logic_vector(1 downto 0);
             S_AXI_RVALID  : out std_logic;
             S_AXI_RREADY  : in std_logic;
             SWITCH_RSTN   : out std_logic);
    end component AXIS_SWITCH_TOP;

    --CUNoC: Default Signals0: AXIS_SWITCH_TOP Begin
    signal S_AXIS_TDATA_int : std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
    signal S_AXIS_TVALID_int : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal M_AXIS_TREADY_int : std_logic_vector(NUM_MASTER-1 downto 0);
    signal S_AXIS_TLAST_int : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal S_AXIS_TKEEP_int : std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
    signal M_AXIS_TDATA_int : std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH-1 downto 0);
    signal M_AXIS_TVALID_int : std_logic_vector(NUM_MASTER-1 downto 0);
    signal S_AXIS_TREADY_int : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal M_AXIS_TLAST_int : std_logic_vector(NUM_MASTER-1 downto 0);
    signal M_AXIS_TKEEP_int : std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH/8-1 downto 0);
    signal SWITCH_RSTN : std_logic;
    --CUNoC: Default Signals0: AXIS_SWITCH_TOP End    
    
    --CUNoC: Default Constants, Components, Signals End
    
    --CUNoC: Constants Begin

    --CUNoC: Constants End
    
    --CUNoC: Components Begin
    
    --CUNoC: Components End
    
    --CUNoC: Signals Begin
    
    --CUNoC: Signals End
    
begin
    --CUNoC: Default Component Instantiation and Signal Assignments Begin
          
    -- AXIS Switch Instantiation
    AXIS_SWITCH_TOP_inst: AXIS_SWITCH_TOP
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             S_AXIS_TDATA  => S_AXIS_TDATA_int, 
             S_AXIS_TVALID => S_AXIS_TVALID_int,
             M_AXIS_TREADY => M_AXIS_TREADY_int,
             S_AXIS_TLAST  => S_AXIS_TLAST_int, 
             S_AXIS_TKEEP  => S_AXIS_TKEEP_int, 
             M_AXIS_TDATA  => M_AXIS_TDATA_int, 
             M_AXIS_TVALID => M_AXIS_TVALID_int,
             S_AXIS_TREADY => S_AXIS_TREADY_int,
             M_AXIS_TLAST  => M_AXIS_TLAST_int, 
             M_AXIS_TKEEP  => M_AXIS_TKEEP_int, 
             S_AXI_AWADDR  => S_AXI_AWADDR, 
             S_AXI_AWPROT  => S_AXI_AWPROT, 
             S_AXI_AWVALID => S_AXI_AWVALID,
             S_AXI_AWREADY => S_AXI_AWREADY,
             S_AXI_WDATA   => S_AXI_WDATA,  
             S_AXI_WSTRB   => S_AXI_WSTRB,  
             S_AXI_WVALID  => S_AXI_WVALID, 
             S_AXI_WREADY  => S_AXI_WREADY, 
             S_AXI_BRESP   => S_AXI_BRESP,  
             S_AXI_BVALID  => S_AXI_BVALID, 
             S_AXI_BREADY  => S_AXI_BREADY, 
             S_AXI_ARADDR  => S_AXI_ARADDR, 
             S_AXI_ARPROT  => S_AXI_ARPROT, 
             S_AXI_ARVALID => S_AXI_ARVALID,
             S_AXI_ARREADY => S_AXI_ARREADY,
             S_AXI_RDATA   => S_AXI_RDATA,  
             S_AXI_RRESP   => S_AXI_RRESP,  
             S_AXI_RVALID  => S_AXI_RVALID, 
             S_AXI_RREADY  => S_AXI_RREADY, 
             SWITCH_RSTN   => SWITCH_RSTN);
        
    -- AXIS S0 Assignments         
    S_AXIS_TDATA_int((0+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*0)     <= S0_AXIS_TDATA;  
    S_AXIS_TVALID_int(0)                                                 <= S0_AXIS_TVALID; 
    M_AXIS_TREADY_int(0)                                                 <= M0_AXIS_TREADY; 
    S_AXIS_TLAST_int(0)                                                  <= S0_AXIS_TLAST;  
    S_AXIS_TKEEP_int((0+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*0) <= S0_AXIS_TKEEP;    

    -- AXIS M0 Assignments         
    M0_AXIS_TDATA  <= M_AXIS_TDATA_int((0+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*0); 
    M0_AXIS_TVALID <= M_AXIS_TVALID_int(0);
    S0_AXIS_TREADY <= S_AXIS_TREADY_int(0);
    M0_AXIS_TLAST  <= M_AXIS_TLAST_int(0); 
    M0_AXIS_TKEEP  <= M_AXIS_TKEEP_int((0+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*0);  
         
    --CUNoC: Default Component Instantiation and Signal Assignments End
 
    --CUNoC: Component Instantiation Begin
    
    --CUNoC: Component Instantiation End
    
    --CUNoC: Signal Assignments Begin

    --CUNoC: Signal Assignments End
    
end behavior;
