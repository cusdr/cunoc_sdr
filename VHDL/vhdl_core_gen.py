import numpy as np 
from vhdl_utility import *
'''
    UpdateTopSet/UpdateVersionControl/UpdateDeclare/UpdateInst updates templates with user configured params
'''           
def UpdateTopSet(contents, num_master, num_slave, tlast_period):
    param_list = {}
    num_reg = num_slave+num_master+3
    axi_lite_addr_width = int(np.ceil(np.log2(num_reg)))+2   

    param_list["AXI_DATA_WIDTH"] = 64
    param_list["NUM_SLAVE"] = num_slave
    param_list["NUM_MASTER"] = num_master
    param_list["NUM_REG"] = num_reg
    param_list["C_S_AXI_DATA_WIDTH"] = 32
    param_list["C_S_AXI_ADDR_WIDTH"] = axi_lite_addr_width
    param_list["IQ_DATA_WIDTH"] = 12
    param_list["RAD_FIFO_DEPTH"] = 12
    param_list["TLAST_PER"] = tlast_period

    UpdateFields(contents, param_list)
    return contents

def UpdateVersionControl(maj_ver, min_ver, num_master, num_slave):
    ver_reg_template = "IP_VHDL_Source/VERSION_CTRL_REG/S_AXI_VER_REG.vhd"
    param_list = {}

    param_list["MAJOR_VERSION"] = maj_ver
    param_list["MINOR_VERSION"] = min_ver
    param_list["NUM_MASTER"] = num_master
    param_list["NUM_SLAVE"] = num_slave

    # Load Template
    ver_reg_contents = LoadTemplate("IP_VHDL_Source/VERSION_CTRL_REG/S_AXI_VER_REG.vhd")
    UpdateFields(ver_reg_contents, param_list)
    # Store Template
    StoreTemplate(ver_reg_template, ver_reg_contents)

def UpdateDeclare(contents, comp_name_str, param_list,
             num_slave, num_master, DSP_clk_en):
    tab_level = 1
    insert_string = "    --CUNoC: Components End\n"
    # Load Main, Slave, and Master Declaration Template Contents
    main_decl_contents = LoadTemplate(main_dec_template)

    # Replace Component Name
    UpdateString(main_decl_contents, "*comp_name*", comp_name_str)
    
    # Configure Generics
    pre_padding, start_index = FindPadding(main_decl_contents, "--CUNoC: Comp Generics", True)
    main_decl_contents = InsertFields(main_decl_contents, pre_padding, ";", ");", param_list, start_index, " : integer := ")  
    
    # Enable DSP CLk
    if(DSP_clk_en):
        UpdateString(main_decl_contents, "--DSP_CLK","  DSP_CLK")
    
    # Add Slave Interfaces
    pre_padding, start_index = FindPadding(main_decl_contents, "-- Slave", False) 
    for ii in range(num_slave):
        slave_decl_contents = LoadTemplate(slave_dec_template)
        if((num_master==0) and (ii==max(range(num_slave)))):
            slave_decl_contents = UpdateString(slave_decl_contents, 'xx', str(ii))
            main_decl_contents, start_index = InsertString(main_decl_contents, start_index, pre_padding, slave_decl_contents[:-1])                          
            kk = slave_decl_contents[-1]
            kk = kk.replace(';',');')
            main_decl_contents.insert(start_index,pre_padding+kk)      
        else:
            slave_decl_contents = UpdateString(slave_decl_contents, 'xx', str(ii))
                
            main_decl_contents, start_index = InsertString(main_decl_contents, start_index, pre_padding, slave_decl_contents)
        
    # Add Master Interfaces
    pre_padding, start_index = FindPadding(main_decl_contents, "-- Master", False) 
    for ii in range(num_master):
        master_decl_contents = LoadTemplate(master_dec_template)
        if(ii == max(range(num_master))):
            master_decl_contents = UpdateString(master_decl_contents, 'yy', str(ii))
            main_decl_contents, start_index = InsertString(main_decl_contents, start_index, pre_padding, master_decl_contents[:-1])            
            kk = master_decl_contents[-1]    
            kk = kk.replace(';',');')
            main_decl_contents.insert(start_index,pre_padding+kk)  
                 
        else:
            master_decl_contents = UpdateString(master_decl_contents, 'yy', str(ii))
            main_decl_contents, start_index = InsertString(main_decl_contents, start_index, pre_padding, master_decl_contents)
    # Find our insert string
    insert_ind = contents.index(insert_string)
    contents, insert_ind = InsertString(contents, insert_ind, tab_level*TAB_VAL, main_decl_contents)        
    return contents
        
def UpdateInst(contents, comp_name_str, comp_inst_num,
              param_list,
              switch_master_offset, switch_slave_offset, num_slave,
              num_master, DSP_clk_en):
              
    return_dict_skel = {'Comp Name':' ','Comp Inst Num':-1,
                        'Slave Ind':[],'Master Switch Ind':[],
                        'Master Ind':[],'Slave Switch Ind':[]}          
    comp_dict_entry = return_dict_skel
    comp_dict_entry['Comp Name'] = comp_name_str
    comp_dict_entry['Comp Inst Num'] = comp_inst_num
    for ii in range(num_slave):
        comp_dict_entry['Slave Ind'].append(ii)
        comp_dict_entry['Slave Switch Ind'].append(ii+switch_master_offset)
        
    for jj in range(num_master):
        comp_dict_entry['Master Ind'].append(jj)
        comp_dict_entry['Master Switch Ind'].append(jj+switch_slave_offset)
         
    # Add support for custom inst generic maps?
    tab_level = 1
    insert_string = "    --CUNoC: Component Instantiation End\n"
    # Load Main, Slave, and Master Declaration Template Contents
    main_inst_contents = LoadTemplate(main_inst_template)
    
    # Replace Component Name
    for jj in range(len(main_inst_contents)):
        main_inst_contents[jj] = main_inst_contents[jj].replace("nn",str(comp_inst_num))
        main_inst_contents[jj] = main_inst_contents[jj].replace("*comp_name*",comp_name_str)
    
    # Configure Generics
    pre_padding, start_index = FindPadding(main_inst_contents, "--CUNoC: Inst Generics", True)   
    main_inst_contents = InsertFields(main_inst_contents, pre_padding, ",", ")", param_list, start_index, " => ")   
    
    # Enable DSP CLk
    if(DSP_clk_en):
        for jj in range(len(main_inst_contents)):
            main_inst_contents[jj] = main_inst_contents[jj].replace("--DSP_CLK","  DSP_CLK") 

    # Add Slave Interfaces
    pre_padding, start_index = FindPadding(main_inst_contents, "-- SLAVE", False)   
    for ii in range(num_slave):
        slave_inst_contents = LoadTemplate(slave_inst_template)
        if((num_master==0) and (ii==max(range(num_slave)))):
            slave_inst_contents = UpdateString(slave_inst_contents, 'xx', str(ii))
            slave_inst_contents = UpdateString(slave_inst_contents, 'ww', str(ii + switch_master_offset))
            main_inst_contents, start_index = InsertString(main_inst_contents, start_index, pre_padding, slave_inst_contents[:-1])
            kk = slave_inst_contents[-1]
            kk = kk.replace(',',');')
            main_inst_contents.insert(start_index,pre_padding+kk)
                    
        else:
            slave_inst_contents = UpdateString(slave_inst_contents, 'xx', str(ii))
            slave_inst_contents = UpdateString(slave_inst_contents, 'ww', str(ii + switch_master_offset))
            main_inst_contents, start_index = InsertString(main_inst_contents, start_index, pre_padding, slave_inst_contents)

    # Add Master Interfaces
    pre_padding, start_index = FindPadding(main_inst_contents, "-- MASTER", False)      
    for ii in range(num_master):
        master_inst_contents = LoadTemplate(master_inst_template)
        if(ii == max(range(num_master))):
            master_inst_contents = UpdateString(master_inst_contents, 'yy', str(ii))
            master_inst_contents = UpdateString(master_inst_contents, 'zz', str(ii + switch_slave_offset))
            main_inst_contents, start_index = InsertString(main_inst_contents, start_index, pre_padding, master_inst_contents[:-1])
            kk = master_inst_contents[-1]          
            kk = kk.replace(',',');')
            main_inst_contents.insert(start_index,pre_padding+kk)  
        else:
            master_inst_contents = UpdateString(master_inst_contents, 'yy', str(ii))
            master_inst_contents = UpdateString(master_inst_contents, 'zz', str(ii + switch_slave_offset))
            main_inst_contents, start_index = InsertString(main_inst_contents, start_index, pre_padding, master_inst_contents)
    # Find our insert string
    insert_ind = contents.index(insert_string)
    contents, insert_ind = InsertString(contents, insert_ind, tab_level*TAB_VAL, main_inst_contents)  
    return comp_dict_entry, contents
