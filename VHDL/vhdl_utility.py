from vhdl_names import *
'''
    LoadTemplate/StoreTemplate reads/writes contents from/to files
'''
def LoadTemplate(filename):
    ftemp = open(filename,'r')
    contents = ftemp.readlines()
    ftemp.close()
    return contents

def StoreTemplate(filename, contents):
    # Write Top Template
    contents_out = "".join(contents)
    ftemp = open(filename,'w')
    ftemp.write(contents_out)
    ftemp.close()

'''
   UpdateFields update key value pairs to the contents list 
'''
def UpdateFields(contents, param_list):
    for key in param_list.keys():
        for line_num in range(len(contents)):
            check_val = contents[line_num].find(key)
            if check_val >= 0 :
                cur_val = filter(str.isdigit,contents[line_num])
                new_val = str(param_list[key])
                contents[line_num] = contents[line_num].replace(cur_val,new_val)
                break
    return contents
'''
    InsertFields insert key/values pairs to contents starting at start_index with various padding and connector
    format:pre_padding + key + connector + value + post_padding/post_padding_last + "\n"
'''
def InsertFields(contents, pre_padding, post_padding, post_padding_last, param_list, start_index, connector):
    totalKeys = len(param_list)
    curKeys = 1
    for key in param_list.keys():
        if curKeys == totalKeys:
            contents.insert(start_index,pre_padding+key+connector+str(param_list[key])+post_padding_last+"\n")
        else:
            contents.insert(start_index,pre_padding+key+connector+str(param_list[key])+post_padding+"\n")
            start_index = start_index+1
        curKeys += 1
    return contents

'''
    UpdateString match old string in the contents and replace it with new
'''
def UpdateString(contents, old, new):
    for line_num in range(len(contents)):
        contents[line_num] = contents[line_num].replace(old,new)
    return contents

'''
    InsertString inserts subcontents to contents starting at start_index
    and each line will be padded with padding in the front
'''
def InsertString(contents, start_index, padding, subcontents):
    for line in subcontents:
        contents.insert(start_index, padding+line)
        start_index = start_index+1
    return contents, start_index

'''
    FindPadding finds the padding for a specific string and the start index
'''
# spacePadding is a boolean that says whether to use space padding or copy the padding where obj_str resides
def FindPadding(contents, obj_str, spacePadding):
    line_num = 0
    for line in contents:
        val_check = line.find(obj_str)
        if(val_check >= 0):
            if spacePadding:
                padding = val_check*" "
            else:
                padding = line[0:val_check]
            start_index = line_num
            break
        line_num = line_num+1
    start_index = start_index+1 
    return padding, start_index

'''
    GetMasterSlaveNum get the num_master, num_slave information from the user provided component lists
'''
def GetMasterSlaveNum(components):
    num_master = 3
    num_slave = 3
    for component in components:
        for instance in components[component]:
            assert "num_master" in instance
            assert "num_slave" in instance
            num_master += instance["num_slave"]
            num_slave += instance["num_master"]
    return num_master, num_slave