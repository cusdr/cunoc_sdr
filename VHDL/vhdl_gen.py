from vhdl_core_gen import *
# config contains both components and versions
#	version: major and minor
# 	components: component name-> list of instances
def GenerateVHDL(version, components):
	print "generating vhdl files"
	template_contents = LoadTemplate(top_template)
	comp_dict_list = []

	# AA COMMENT: The 6's here can be automated
	num_mas, num_sla = GetMasterSlaveNum(components)
	template_contents = UpdateTopSet(template_contents, num_mas, num_sla, 1*1024*1024)
	UpdateVersionControl(version["major"], version["minor"], num_mas, num_sla)

	# Declare all unique components 
	# Instantiate all user components 
	for component in components:
		assert len(components[component]) > 0
		template_contents = UpdateDeclare(template_contents, component, components[component][0]["params"], components[component][0]["num_slave"], components[component][0]["num_master"], components[component][0]["dsp_enable"])

	# Instantiate all the components, record where they ended up on the network 
	master_offset = 3
	slave_offset = 3
	for component in components:
		instance_num = 0
		for instance in components[component]:
			comp_dict_entry, template_contents = UpdateInst(template_contents, component, instance_num, instance["params"], master_offset, slave_offset, instance["num_slave"], instance["num_master"], instance["dsp_enable"])
			comp_dict_list.append(comp_dict_entry)
			instance["switch_address"] = {}
			instance["switch_address"]["master"] = master_offset
			instance["switch_address"]["slave"] = slave_offset
			instance_num += 1
			master_offset += instance["num_slave"]
			slave_offset += instance["num_master"]
	# Write contents out
	StoreTemplate(final_output, template_contents)
	print "done"
	switch_info = {}
	switch_info["num_master"] = num_mas
	switch_info["num_slave"] = num_sla
	return switch_info, components