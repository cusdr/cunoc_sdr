dir_name = "VHDL/templates/"
top_template = dir_name + "top_template.vhd"
final_output = "IP_VHDL_Source/TOP_Dir/CUNoC_IP.vhd"

main_dec_template = dir_name + "comp_decl_template.vhd"
slave_dec_template = dir_name + "comp_decl_slave.vhd"
master_dec_template = dir_name + "comp_decl_master.vhd"

main_inst_template = dir_name + "comp_inst_template.vhd"
slave_inst_template = dir_name + "comp_inst_slave.vhd"
master_inst_template = dir_name + "comp_inst_master.vhd"

TAB_VAL = "    "