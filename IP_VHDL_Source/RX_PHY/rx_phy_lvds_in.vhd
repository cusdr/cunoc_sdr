library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity rx_phy_lvds_in is 
    port(rx_clk	     : in  std_logic;
		 --delay_clk   : in  std_logic;
         rx_in_p     : in  std_logic;
		 rx_in_n     : in  std_logic;
		 --delay_wdata : in  std_logic_vector(4 downto 0);
		 --delay_we    : in  std_logic;
		 rx_out_I    : out std_logic;
		 rx_out_Q    : out std_logic);
		 --delay_rdata : out std_logic_vector(4 downto 0));
end rx_phy_lvds_in;

architecture behavior of rx_phy_lvds_in is
	signal rx_ibufds : std_logic := '0';
	signal rx_delay : std_logic := '0';
begin 
	--IBUFDS
	IBUFDS_inst : IBUFDS
	port map (
	O => rx_ibufds, -- Buffer output
	I => rx_in_p, -- Diff_p buffer input (connect directly to top-level port)
	IB => rx_in_n -- Diff_n buffer input (connect directly to top-level port)
	);
	-- End of IBUFDS_inst instantiation
    
--	--IDELAY2 Inst
--	IDELAYE2_inst : IDELAYE2
	
--	generic map (
--	CINVCTRL_SEL => "FALSE", -- Enable dynamic clock inversion (FALSE, TRUE)
--	DELAY_SRC => "IDATAIN", -- Delay input (IDATAIN, DATAIN)
--	HIGH_PERFORMANCE_MODE => "FALSE", -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
--	IDELAY_TYPE => "VAR_LOAD", -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
--	IDELAY_VALUE => 0, -- Input delay tap setting (0-31)
--	PIPE_SEL => "FALSE", -- Select pipelined mode, FALSE, TRUE
--	REFCLK_FREQUENCY => 200.0, -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
--	SIGNAL_PATTERN => "DATA" -- DATA, CLOCK input signal
--	)
--	port map (
--	CNTVALUEOUT => delay_rdata, -- 5-bit output: Counter value output
--	DATAOUT => rx_delay, -- 1-bit output: Delayed data output
--	C => delay_clk, -- 1-bit input: Clock input
--	CE => '0', -- 1-bit input: Active high enable increment/decrement input
--	CINVCTRL => '0', -- 1-bit input: Dynamic clock inversion input
--	CNTVALUEIN => delay_wdata, -- 5-bit input: Counter value input
--	DATAIN => '0', -- 1-bit input: Internal delay data input
--	IDATAIN => rx_ibufds, -- 1-bit input: Data input from the I/O
--	INC => '0', -- 1-bit input: Increment / Decrement tap delay input
--	LD => delay_we, -- 1-bit input: Load IDELAY_VALUE input
--	LDPIPEEN => '0', -- 1-bit input: Enable PIPELINE register to load data input
--	REGRST => '0' -- 1-bit input: Active-high reset tap-delay input
--	);
--	-- End of IDELAYE2_inst instantiation

	--IDDR Inst
	IDDR_inst : IDDR
	generic map (
	DDR_CLK_EDGE => "SAME_EDGE_PIPELINED", -- "OPPOSITE_EDGE", "SAME_EDGE"
	-- or "SAME_EDGE_PIPELINED"
	INIT_Q1 => '0', -- Initial value of Q1: ’0’ or ’1’
	INIT_Q2 => '0', -- Initial value of Q2: ’0’ or ’1’
	SRTYPE => "ASYNC") -- Set/Reset type: "SYNC" or "ASYNC"
	port map (
	Q1 =>rx_out_I, -- 1-bit output for positive edge of clock
	Q2 =>rx_out_Q, -- 1-bit output for negative edge of clock
	C => rx_clk, -- 1-bit clock input
	CE => '1', -- 1-bit clock enable input
	--D => rx_ibufds, -- 1-bit DDR data input
	D => rx_ibufds, -- 1-bit DDR data input
	R => '0', -- 1-bit reset
	S => '0' -- 1-bit set
	);
	-- End of IDDR_inst instantiation
end behavior;