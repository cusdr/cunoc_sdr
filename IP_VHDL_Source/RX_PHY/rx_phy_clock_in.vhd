library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity rx_phy_clock_in is 
    port(clk_in_p : in  std_logic;
		 clk_in_n : in  std_logic;
		 clk_out  : out std_logic);
end rx_phy_clock_in;

architecture behavior of rx_phy_clock_in is
	signal clock_ibufgds : std_logic := '0';
begin 

	-- IBUFGDS: Differential Global Clock Input Buffer
	IBUFGDS_inst : IBUFGDS
	port map (
	O => clock_ibufgds, -- Clock buffer output
	I => clk_in_p, -- Diff_p clock buffer input (connect directly to top-level port)
	IB => clk_in_n -- Diff_n clock buffer input (connect directly to top-level port)
	);
	-- End of IBUFGDS_inst instantiation

	-- BUFG: Global Clock Simple Buffer
	BUFG_inst : BUFG
	port map (
	O => clk_out, -- 1-bit output: Clock output
	I => clock_ibufgds -- 1-bit input: Clock input
	);
	-- End of BUFG_inst instantiation
	
end behavior;
	-- End of IDDR_inst instantiation