library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity rx_phy_top is 
    port(rx_clk_in_p   : in  std_logic;
         rx_clk_in_n   : in  std_logic;
		 rx_frame_in_p : in  std_logic;
		 rx_frame_in_n : in  std_logic;
		 rx_data_in_p  : in  std_logic_vector(5 downto 0);
		 rx_data_in_n  : in  std_logic_vector(5 downto 0);
		 rx_clk_out    : out std_logic;
		 rx1_I_data    : out std_logic_vector(11 downto 0); 
		 rx1_Q_data    : out std_logic_vector(11 downto 0);
		 rx2_I_data    : out std_logic_vector(11 downto 0);
		 rx2_Q_data    : out std_logic_vector(11 downto 0); 
		 rx_val        : out std_logic);
end rx_phy_top;

architecture behavior of rx_phy_top is
	-- Constant, Signal, and Component Declarations
    -- Constants
	constant rx_d_width : integer := 6; -- width of differential data in 
    -- Signals
    signal rx_I   : std_logic_vector(5 downto 0) := (others => '0');
    signal rx_Q   : std_logic_vector(5 downto 0) := (others => '0');
    signal rx_I_d : std_logic_vector(5 downto 0) := (others => '0');
    signal rx_Q_d : std_logic_vector(5 downto 0) := (others => '0'); 
    signal rx_I_s : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_Q_s : std_logic_vector(11 downto 0) := (others => '0');   
    signal rx_I1_reg : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_Q1_reg : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_I2_reg : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_Q2_reg : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_I1_out : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_Q1_out : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_I2_out : std_logic_vector(11 downto 0) := (others => '0');
    signal rx_Q2_out : std_logic_vector(11 downto 0) := (others => '0');
	signal rx_frame : std_logic := '0';
	signal rx_frame_d : std_logic := '0';
    signal rx_clk : std_logic;
	--signal delay_we : std_logic_vector(6 downto 0) := (others => '0');
	signal frame_re : std_logic := '0';
	signal frame_fe : std_logic := '0';
	signal frame_re_d : std_logic := '0';
    signal frame_fe_d : std_logic := '0';
    --type   rd_arr is array(rx_d_width downto 0) of std_logic_vector(4 downto 0);
    --signal delay_rd_arr : rd_arr; 
    
	-- Component Declaration

	-- Rx Clock In 
	component rx_phy_clock_in is 
		port(clk_in_p : in  std_logic;
			 clk_in_n : in  std_logic;
			 clk_out  : out std_logic);
	end component rx_phy_clock_in;
	
	-- Rx Data In Path
	component rx_phy_lvds_in is 
		port(rx_clk	     : in  std_logic;
			 --delay_clk   : in  std_logic;
			 rx_in_p     : in  std_logic;
			 rx_in_n     : in  std_logic;
			 --delay_wdata : in  std_logic_vector(4 downto 0);
			 --delay_we    : in  std_logic;
			 rx_out_I    : out std_logic;
			 rx_out_Q    : out std_logic);
			 --delay_rdata : out std_logic_vector(4 downto 0));
	end component rx_phy_lvds_in;
	
    -- End Constants Signals and Components
begin
--	-- IDELAYCTRL: IDELAYE2/ODELAYE2 Tap Delay Value Control
--    IDELAYCTRL_inst : IDELAYCTRL
--    port map (
--    RDY => delay_rdy_out, -- 1-bit output: Ready output
--    REFCLK => delay_clk_in, -- 1-bit input: Reference clock input
--    RST => delay_rst_in -- 1-bit input: Active high reset input
--    );
--    -- End of IDELAYCTRL_inst instantiation

    -- Internal Assignments    
    rx_I_s <= rx_I_d & rx_I;
    rx_Q_s <= rx_Q_d & rx_Q;
    -- Output Assignments
    rx1_I_data <= rx_I1_out;
    rx1_Q_data <= rx_Q1_out;
    rx2_I_data <= rx_I2_out;
    rx2_Q_data <= rx_Q2_out;
    rx_val <= frame_re_d;
    rx_clk_out <= rx_clk;
    -- Component Instantiations            
	rx_phy_clock_inst : rx_phy_clock_in
		port map(clk_in_p => rx_clk_in_p,
				 clk_in_n => rx_clk_in_n,
				 clk_out  => rx_clk);
	
	rx_frame_in_inst : rx_phy_lvds_in
		port map(rx_clk => rx_clk,
				 --delay_clk => delay_clk_in,
				 rx_in_p => rx_frame_in_p,
				 rx_in_n => rx_frame_in_n,
				 --delay_wdata => delay_wdata,
				 --delay_we => delay_we(6),
				 rx_out_I => rx_frame,
				 rx_out_Q => open);
				 --delay_rdata => delay_rd_arr(6));
				 
	gen_rx_data_stages: for I in 0 to rx_d_width-1 generate
		rx_data_in_inst : rx_phy_lvds_in
			port map(rx_clk => rx_clk,
					 --delay_clk => delay_clk_in,
					 rx_in_p => rx_data_in_p(I),
					 rx_in_n => rx_data_in_n(I),
					 --delay_wdata => delay_wdata,
					 --delay_we => delay_we(I),
					 rx_out_I => rx_I(I),
					 rx_out_Q => rx_Q(I));
					 --delay_rdata => delay_rd_arr(I));
	end generate gen_rx_data_stages;

-- Processes
    data_frame_proc: process(rx_clk) begin
        if rx_clk'event and rx_clk = '1' then
            rx_I_d <= rx_I;
            rx_Q_d <= rx_Q;
            rx_frame_d <= rx_frame;
            frame_re <=(rx_frame and not(rx_frame_d));
            frame_fe <=(not(rx_frame) and rx_frame_d);
            frame_re_d <= frame_re;
            frame_fe_d <= frame_fe; 
            if(frame_re = '1') then 
                rx_I1_reg <= rx_I_s; 
                rx_Q1_reg <= rx_Q_s;
            end if;
            if(frame_fe = '1') then 
                rx_I2_reg <= rx_I_s; 
                rx_Q2_reg <= rx_Q_s;
            end if;
            if(frame_re_d <= '1') then 
                rx_I1_out <= rx_I1_reg;
                rx_Q1_out <= rx_Q1_reg;
                rx_I2_out <= rx_I2_reg;
                rx_Q2_out <= rx_Q2_reg;
            end if;       
        end if;                        
    end process data_frame_proc; 

--    delay_we_proc: process(delay_clk_in,delay_addr) begin
--        if delay_clk_in'event and delay_clk_in = '1' then
--			del_we: case(delay_addr) is
--				when "0000" => delay_we <="0000001";
--				               delay_rdata <= delay_rd_arr(0);
--				when "0001" => delay_we <="0000010";
--				               delay_rdata <= delay_rd_arr(1);
--				when "0010" => delay_we <="0000100";
--				               delay_rdata <= delay_rd_arr(2);
--				when "0011" => delay_we <="0001000";
--				               delay_rdata <= delay_rd_arr(3);
--				when "0100" => delay_we <="0010000";
--				               delay_rdata <= delay_rd_arr(4);
--				when "0101" => delay_we <="0100000";
--				               delay_rdata <= delay_rd_arr(5);
--				when "0110" => delay_we <="1000000";
--				               delay_rdata <= delay_rd_arr(6);
--				when others => delay_we <="0000000";
--				               delay_rdata <= delay_rd_arr(6);
--			end case del_we;
--        end if;
--    end process delay_we_proc;
end behavior; 