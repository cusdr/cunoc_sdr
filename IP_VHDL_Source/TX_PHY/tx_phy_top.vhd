library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
Library UNISIM;
use UNISIM.vcomponents.all;
entity tx_phy_top is 
    port(tx_clk_in      : in  std_logic;
		 tx_I1_in       : in  std_logic_vector(11 downto 0);
		 tx_Q1_in       : in  std_logic_vector(11 downto 0);
		 tx_I2_in       : in  std_logic_vector(11 downto 0);
		 tx_Q2_in       : in  std_logic_vector(11 downto 0);
		 tx_val_in      : in  std_logic;
		 tx_clk_out_p   : out std_logic;
		 tx_clk_out_n   : out std_logic;
		 tx_data_out_p  : out std_logic_vector(5 downto 0);
		 tx_data_out_n  : out std_logic_vector(5 downto 0);
		 tx_frame_out_p : out std_logic;
		 tx_frame_out_n : out std_logic);
end tx_phy_top;
architecture behavior of tx_phy_top is
	-- Constant, Signal, and Component Declarations
    -- Constants
	constant tx_d_width : integer := 6; -- width of differential data in 
    -- Signals
    signal I1M : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch1 I MSB
    signal I1L : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch1 I LSB
    signal I2M : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch2 I MSB
    signal I2L : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch2 I LSB
    signal Q1M : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch1 Q MSB
    signal Q1L : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch1 Q LSB
    signal Q2M : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch2 Q MSB
    signal Q2L : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- Tx Ch2 Q LSB
	signal tx_val_z1 : std_logic:= '0';
	signal tx_val_z2 : std_logic:= '0';
	signal tx_val_z3 : std_logic:= '0';
	signal tx_sel : std_logic_vector(3 downto 0) := (others => '0'); -- Mux select sIgnal for one hot encoding 
	signal oddr_d1_in : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- ODDR D1 Input
	signal oddr_d2_in : std_logic_vector(tx_d_width-1 downto 0) := (others => '0'); -- ODDR D2 Input
    signal tx_frame_int : std_logic := '0';         
	-- Component Declaration
	
	-- Tx Data Out Path
	component tx_phy_lvds_out is 
		port(tx_clk	     : in  std_logic;
			 tx_in_I     : in  std_logic;
			 tx_in_Q     : in  std_logic;
			 tx_out_p    : out  std_logic;
			 tx_out_n    : out  std_logic);
	end component tx_phy_lvds_out;
	
    -- End Constants Signals and Components
begin
	-- SIgnal Assignments
	tx_sel <= tx_val_z3 & tx_val_z2 & tx_val_z1 & tx_val_in;
	I1M <= tx_I1_in(11 downto 6);
	I1L <= tx_I1_in(5 downto 0);
	I2M <= tx_I2_in(11 downto 6);
	I2L <= tx_I2_in(5 downto 0);
	Q1M <= tx_Q1_in(11 downto 6);
	Q1L <= tx_Q1_in(5 downto 0);
	Q2M <= tx_Q2_in(11 downto 6);
	Q2L <= tx_Q2_in(5 downto 0);
	
	
	
	
    -- Processes
	val_del_proc: process(tx_clk_in) begin
		if tx_clk_in'event and tx_clk_in = '1' then 
			tx_val_z1 <= tx_val_in;
			tx_val_z2 <= tx_val_z1;
			tx_val_z3 <= tx_val_z2;
		end if;
	end process val_del_proc;
	
	-- Case Statement
	-- ODDR D1 Mux 
    mux_proc: process(tx_sel,tx_clk_in) begin
        if tx_clk_in'event and tx_clk_in = '1' then 
        
            tx_frame_int <= tx_val_z1 or tx_val_in; -- frame int 
        
            case tx_sel is
                when "0001" => oddr_d1_in <= I1M;  
                when "0010" => oddr_d1_in <= I1L;   
                when "0100" => oddr_d1_in <= I2M;  
                when "1000" => oddr_d1_in <= I2L;   
                when others =>  oddr_d1_in <= (others => '0');  
            end case; 
            
            -- ODDR D2 Mux 
            case tx_sel is
                when "0001" => oddr_d2_in <= Q1M;  
                when "0010" => oddr_d2_in <= Q1L;   
                when "0100" => oddr_d2_in <= Q2M;  
                when "1000" => oddr_d2_in <= Q2L;   
                when others =>  oddr_d2_in <= (others => '0');  
            end case; 
        
        end if;
    end process mux_proc;
	
	-- Component Instantiations            
	
	tx_clk_out_inst : tx_phy_lvds_out
		port map(tx_clk => tx_clk_in,
				 tx_in_I => '0',
				 tx_in_Q => '1',
				 tx_out_p => tx_clk_out_p,
				 tx_out_n => tx_clk_out_n);
				 
	tx_frame_out_inst : tx_phy_lvds_out
		port map(tx_clk => tx_clk_in,
				 tx_in_I => tx_frame_int,
				 tx_in_Q => tx_frame_int,
				 tx_out_p => tx_frame_out_p,
				 tx_out_n => tx_frame_out_n);
				 
	gen_tx_data_stages: for I in 0 to tx_d_width-1 generate
		tx_data_in_inst : tx_phy_lvds_out
			port map(tx_clk => tx_clk_in,
				 tx_in_I =>oddr_d1_in(I),
				 tx_in_Q =>oddr_d2_in(I),
				 tx_out_p => tx_data_out_p(I),
				 tx_out_n => tx_data_out_n(I));
	end generate gen_tx_data_stages;
	
end behavior; 