library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity tx_phy_lvds_out is 
    port(tx_clk	     : in  std_logic;
         tx_in_I     : in  std_logic;
		 tx_in_Q     : in  std_logic;
		 tx_out_p    : out  std_logic;
		 tx_out_n    : out  std_logic);
end tx_phy_lvds_out;

architecture behavior of tx_phy_lvds_out is
	signal tx_oddr : std_logic := '0';
begin 

	-- ODDR: Output Double Data Rate Output Register with Set, Reset
	ODDR_inst : ODDR
	generic map(
		DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
		INIT => '0', -- Initial value for Q port (’1’ or ’0’)
		SRTYPE => "ASYNC") -- Reset Type ("ASYNC" or "SYNC")
	port map (
		Q => tx_oddr, -- 1-bit DDR output
		C => tx_clk, -- 1-bit clock input
		CE => '1', -- 1-bit clock enable input
		D1 => tx_in_I, -- 1-bit data input (positive edge)
		D2 => tx_in_Q, -- 1-bit data input (negative edge)
		R => '0', -- 1-bit reset input
		S => '0' -- 1-bit set input
	);
	-- End of ODDR_inst instantiation

	-- OBUFDS: Differential Output Buffer
	OBUFDS_inst : OBUFDS
	port map (
		O => tx_out_p, -- Diff_p output (connect directly to top-level port)
		OB => tx_out_n, -- Diff_n output (connect directly to top-level port)
		I => tx_oddr -- Buffer input
	);
	-- End of OBUFDS_inst instantiation


end behavior;