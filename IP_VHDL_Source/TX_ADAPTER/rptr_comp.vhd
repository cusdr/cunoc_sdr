-- Read Pointer Comparing File 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
 use IEEE.STD_LOGIC_ARITH.ALL;
 
entity rptr_comp is 
    generic (ADDR : integer := 10);
    port(rclk	  : in   std_logic;
         rrst_n   : in   std_logic;
         ren	  : in   std_logic;
		 r_wptr   : in std_logic_vector(ADDR downto 0);
		 raddr    : out std_logic_vector(ADDR-1 downto 0);
		 rptr 	  : out std_logic_vector(ADDR downto 0);
		 rempty   : out	std_logic);
end rptr_comp;

architecture behavior of rptr_comp is
-- Constants 
-- Signals 
	-- Reg's 
	signal rbin : std_logic_vector(ADDR downto 0) := (others => '0');
	signal rptr_reg : std_logic_vector(ADDR downto 0) := (others => '0');
	signal rempty_reg : std_logic := '1'; 
	-- Wires 
	signal rempty_val : std_logic;
	signal rgraynext : std_logic_vector(ADDR downto 0);
	signal rbinnext : std_logic_vector(ADDR downto 0);
	signal add_sig : std_logic_vector(ADDR downto 0);
begin 
	-- Basic assignments
	add_sig(ADDR downto 1) <= (others => '0');
	add_sig(0) <= ren and not(rempty_reg);
	-- Reg assignments 
	rempty <= rempty_reg;
	rptr <= rptr_reg; 
	raddr <= rbin(ADDR-1 downto 0);
	rbinnext <= rbin + add_sig; 
	rgraynext <= ('0' & (rbinnext(ADDR downto 1)) xor rbinnext);
	-- Empty reg
	rempty_val <= '1' when (rgraynext=r_wptr) else '0';
	
	
	-- Main proc to reset and advance rbin,rptr_reg
	main_proc: process(rclk,rempty_val,rrst_n) begin
		if(rrst_n='0') then
			rbin <= (others => '0');
			rptr_reg <= (others => '0');
			rempty_reg <= '1';
		elsif rclk'event and rclk='1' then  
			rptr_reg <= rgraynext;
			rbin <= rbinnext;
			rempty_reg <= rempty_val;
		end if;
	end process main_proc;
end behavior;	