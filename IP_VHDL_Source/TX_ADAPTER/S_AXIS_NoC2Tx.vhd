library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity S_AXIS_NoC2Tx is 
    generic (AXI_DATA_WIDTH : integer := 64;
	     AXI_FIFO_DEPTH : integer := 12;
	     IQ_DATA_WIDTH  : integer := 12;
	     MEM_TYPE : string := "block");
    port(-- AXIS Clock Domain
		 AXIS_ACLK	   : in  std_logic;
         	 AXIS_RESETN   : in  std_logic;
		 -- Slave Interface
		 S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 S0_AXIS_TVALID : in  std_logic;
		 S0_AXIS_TREADY : out std_logic;
		 S0_AXIS_TLAST  : in  std_logic;
		 S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Tx Clock Domain
		 RX_CLK    	   : in  std_logic;
		 RX_VAL		   : in  std_logic;
		 TX_CLK    	   : out std_logic;
		 TX_VAL    	   : out std_logic;
		 I_DATA_OUT    : out std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
		 Q_DATA_OUT    : out std_logic_vector(IQ_DATA_WIDTH-1 downto 0));
end S_AXIS_NoC2Tx;

architecture behavior of S_AXIS_NoC2Tx is
	-- Constant, Signal, and Component Declarations
    -- Constants
	constant IQ_BUS_WIDTH : integer := 2*IQ_DATA_WIDTH;	
	-- 2 clk FIFO
	component FIFO_ASYNC_PHY is
	    generic (DATA_WIDTH :integer := IQ_BUS_WIDTH;
		     ADDR_WIDTH :integer := AXI_FIFO_DEPTH;
		     MEM_DoB : string := MEM_TYPE);
	    port (-- Reading port.
			  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
			  Empty_out   :out std_logic;
			  ReadEn_in   :in  std_logic;
			  RClk        :in  std_logic;
			  -- Writing port.
			  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
			  Full_out    :out std_logic;
			  WriteEn_in  :in  std_logic;
			  WClk        :in  std_logic;
			  -- rst
			  RRst_n	  :in  std_logic;
			  WRst_n	  :in  std_logic);
	end component FIFO_ASYNC_PHY;
	
	-- Synchronizer
	component clock_cross_bin is
		Port ( clk_in : in STD_LOGIC;
			   clk_out : in STD_LOGIC;
			   data_in : in STD_LOGIC;
			   data_out : out STD_LOGIC);
	end component clock_cross_bin;

	-- Shift Reg
	component shift_reg_en is 
		generic (data_width : integer := 32;
				 reg_stages : integer := 16);
		port(clk  : in   std_logic;
			 ce   : in   std_logic;
			 en   : in   std_logic;
			 rst  : in   std_logic;
			 din  : in	std_logic_vector(data_width-1 downto 0);
			 dout : out	std_logic_vector(data_width-1 downto 0));
	end component shift_reg_en;
	
	-- Shift Reg for bool
	component shift_reg_en_bool is 
		generic (reg_stages : integer := 16);
		port(clk  : in   std_logic;
			 ce   : in   std_logic;
			 en   : in   std_logic;
			 rst  : in   std_logic;
			 din  : in	std_logic;
			 dout : out	std_logic);
	end component shift_reg_en_bool;
	
	-- Signals
    -- Slave Signals
	signal S_WE : std_logic;
	signal S_FULL : std_logic;
	signal S_RDY_INT : std_logic; 
	signal RX_AXIS_RESETN : std_logic :='1';
	signal I_DATA_IN : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
	signal Q_DATA_IN : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
	
	-- TX Signals 
	signal TX_DATA_OUT : std_logic_vector(IQ_BUS_WIDTH-1 downto 0);
	signal TX_DATA_OUT_Z3 : std_logic_vector(IQ_BUS_WIDTH-1 downto 0);
	signal TX_RE : std_logic;
	signal EMPTY_INT : std_logic;
	signal RX_VAL_Z4 : std_logic;
	-- DSP Signals 
	signal D_RE_REG : std_logic := '0';
	signal D_EMPTY : std_logic := '0';
	signal FIFO_DOUT : std_logic_vector(AXI_DATA_WIDTH downto 0) := (others => '0');
	signal LAST_REG : std_logic := '0'; 
	signal PROC_EN_DSP : std_logic := '0';
	
	
begin
	-- Signal Assignments 
	-- Slave Signal Assignments
	S_RDY_INT <= not(S_FULL);
	S0_AXIS_TREADY <= S_RDY_INT;
	S_WE <= S0_AXIS_TVALID and S_RDY_INT;
	I_DATA_IN <= S0_AXIS_TDATA((IQ_DATA_WIDTH-1+16) downto 16);
	Q_DATA_IN <= S0_AXIS_TDATA(IQ_DATA_WIDTH-1 downto 0);
	-- TX Signal Assignments
	TX_RE <= not(EMPTY_INT) and RX_VAL; -- Now only read out when we're in the proc state
	I_DATA_OUT <= TX_DATA_OUT_Z3(IQ_BUS_WIDTH-1 downto IQ_DATA_WIDTH);
	Q_DATA_OUT <= TX_DATA_OUT_Z3(IQ_DATA_WIDTH-1 downto 0);
	TX_VAL <= RX_VAL_Z4; 
	TX_CLK <= RX_CLK;
	
	fifo_slave_2clk_inst: FIFO_ASYNC_PHY
		port map(Data_out => TX_DATA_OUT,
			 Empty_out => EMPTY_INT,
			 ReadEn_in => TX_RE,
			 RClk => RX_CLK,  
			 Data_in => (I_DATA_IN & Q_DATA_IN),
			 Full_out => S_FULL,
			 WriteEn_in => S_WE, 
			 WClk => AXIS_ACLK,
			 RRst_n => RX_AXIS_RESETN,
			 WRst_n => AXIS_RESETN);

	rst_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
			 clk_out => RX_CLK,
			 data_in => AXIS_RESETN,
			 data_out => RX_AXIS_RESETN);		 

	data_shift_reg : shift_reg_en
		generic map(data_width => IQ_BUS_WIDTH,
		            reg_stages => 3)
		port map(clk => RX_CLK,
		         ce  => '1',
		         en  => '1',
		         rst => not(RX_AXIS_RESETN),
		         din => TX_DATA_OUT,
		         dout => TX_DATA_OUT_Z3);

	val_shift_reg : shift_reg_en_bool
		generic map(reg_stages => 4)
		port map(clk => RX_CLK,
		         ce  => '1',
		         en  => '1',
		         rst => not(RX_AXIS_RESETN),
		         din => RX_VAL,
		         dout => RX_VAL_Z4);				 

end behavior;  
	
	
