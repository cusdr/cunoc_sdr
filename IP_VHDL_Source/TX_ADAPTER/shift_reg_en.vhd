library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity shift_reg_en is 
    generic (data_width : integer := 32;
             reg_stages : integer := 16);
    port(clk  : in   std_logic;
         ce   : in   std_logic;
		 en   : in   std_logic;
         rst  : in   std_logic;
		 din  : in	std_logic_vector(data_width-1 downto 0);
		 dout : out	std_logic_vector(data_width-1 downto 0));
end shift_reg_en;

architecture behavior of shift_reg_en is

    type pipe_arr is array (reg_stages-1 downto 0) of std_logic_vector(data_width-1 downto 0);
    signal reg_arr : pipe_arr  := (others => (others => '0'));

begin
    process (clk) begin
        if clk' event and clk = '1' then
            if rst = '1' then
                reg_arr <= (others => (others => '0'));
            elsif ((en = '1') and (rst ='0')) then
                for I in reg_stages-1 downto 0 loop
                    if I=0 then
                        reg_arr(I) <= din; 
                    else
                        reg_arr(I) <= reg_arr(I-1);
                    end if;
                end loop;
		    else 
				reg_arr <= reg_arr;
            end if;
        end if;
    end process; 
    dout <= reg_arr(reg_stages-1);
end behavior;