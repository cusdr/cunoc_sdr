-- Top 2-clk FIFO
-- Based on:
-- Simulation and Synthesis Techniques for Asynchronous FIFO Design
-- By:
-- Clifford E. Cummings, Sunburst Design, Inc. cliffc@sunburst-design.com

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity FIFO_ASYNC_PHY is
    generic (DATA_WIDTH :integer := 64;
	     ADDR_WIDTH :integer := 9;
    	     MEM_DoB : string := "distributed");
    port (-- Reading port.
		  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
		  Empty_out   :out std_logic;
		  ReadEn_in   :in  std_logic;
		  RClk        :in  std_logic;
		  -- Writing port.
		  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
		  Full_out    :out std_logic;
		  WriteEn_in  :in  std_logic;
		  WClk        :in  std_logic;
		  -- rst
		  RRst_n	  :in  std_logic;
		  WRst_n	  :in  std_logic);
end FIFO_ASYNC_PHY;

architecture behavior of FIFO_ASYNC_PHY is
	-- Component Declarations
	-- Dual Port RAM
	component DP_RAM_PHY is
		generic (DATA    : integer := DATA_WIDTH;
			 ADDR    : integer := ADDR_WIDTH;
			 MEM_DoB : string := MEM_DoB);
		port (wclk    : in  std_logic;
			  wen     : in  std_logic;
			  waddr   : in  std_logic_vector(ADDR-1 downto 0);
			  wdata   : in  std_logic_vector(DATA-1 downto 0);
			  rclk    : in  std_logic;
			  ren     : in  std_logic;
			  raddr   : in  std_logic_vector(ADDR-1 downto 0);
			  rdata   : out std_logic_vector(DATA-1 downto 0));
	end component DP_RAM_PHY;
	
	-- Clock Synchronizer for Gray counters
	component clk_sync is
		generic( data_width : integer := ADDR_WIDTH+1);
			Port ( clk : in STD_LOGIC;
			       rstn : in STD_LOGIC;
				   data_in : in STD_LOGIC_VECTOR(data_width-1 downto 0);
				   data_out : out STD_LOGIC_VECTOR(data_width-1 downto 0));
	end component clk_sync;
	
	-- Read Pointer Comparator
	component rptr_comp is 
		generic (ADDR : integer := ADDR_WIDTH);
		port(rclk	  : in   std_logic;
			 rrst_n   : in   std_logic;
			 ren	  : in   std_logic;
			 r_wptr   : in std_logic_vector(ADDR downto 0);
			 raddr    : out std_logic_vector(ADDR-1 downto 0);
			 rptr 	  : out std_logic_vector(ADDR downto 0);
			 rempty   : out	std_logic);
	end component rptr_comp;
	
	-- Write Pointer Comparator
	component wptr_comp is 
		generic (ADDR : integer := ADDR_WIDTH);
		port(wclk	  : in   std_logic;
			 wrst_n   : in   std_logic;
			 wen	  : in   std_logic;
			 w_rptr   : in std_logic_vector(ADDR downto 0);
			 waddr    : out std_logic_vector(ADDR-1 downto 0);
			 wptr 	  : out std_logic_vector(ADDR downto 0);
			 wfull    : out	std_logic);
	end component wptr_comp;
	
	-- Signal Declarations
	--signal SIGNAL1 : std_logic := '0';
	-- Addresses Internal Signals 
    signal waddr_int : std_logic_vector(ADDR_WIDTH-1 downto 0);-- := (others => '0');
	signal raddr_int : std_logic_vector(ADDR_WIDTH-1 downto 0);-- := (others => '0');
	-- Pointer Internal Signals
	signal wptr_w : std_logic_vector(ADDR_WIDTH downto 0);-- := (others => '0');
	signal rptr_r : std_logic_vector(ADDR_WIDTH downto 0);-- := (others => '0');
	signal wptr_r : std_logic_vector(ADDR_WIDTH downto 0);-- := (others => '0');
	signal rptr_w : std_logic_vector(ADDR_WIDTH downto 0);-- := (others => '0');
	-- Full/Empty Internal Signals
	signal full_int : std_logic;-- := '0';
	signal empty_int : std_logic;-- := '1';
	-- Write Enable Internal Signals 
	signal wen_int : std_logic;-- := '0';

begin 
	-- Signal Assignments
	wen_int <= WriteEn_in and not(full_int);
	Empty_out <= empty_int;
	Full_out <= full_int;
	
	-- Component Instantiation
	r2w_sync_inst: clk_sync
		port map(clk => WClk,
		         rstn => WRst_n,
				 data_in => rptr_r,
				 data_out => rptr_w);
				 
	w2r_sync_inst: clk_sync
		port map(clk => RClk,
		         rstn => RRst_n,
				 data_in => wptr_w,
				 data_out => wptr_r);	

	rptr_comp_inst: rptr_comp
		port map(rclk => RClk,
				 rrst_n => RRst_n,
				 ren => ReadEn_in,
				 r_wptr => wptr_r,
				 raddr => raddr_int,
				 rptr => rptr_r,
				 rempty => empty_int);
	
	wptr_comp_inst: wptr_comp
		port map(wclk => WClk,
				 wrst_n => WRst_n,
				 wen => WriteEn_in,
				 w_rptr => rptr_w,
				 waddr => waddr_int,
				 wptr => wptr_w,
				 wfull => full_int);
				 
	DP_RAM_inst: DP_RAM_PHY
		port map(wclk => WClk,
				 wen => wen_int,
				 waddr => waddr_int,
				 wdata => Data_in,
				 rclk => RClk,
				 ren => ReadEn_in,
				 raddr => raddr_int,
				 rdata => Data_out);

end behavior;
				
	
				 
	
	
