-- Open Source AXI4 Stream Switch
-- CU NoC Template for IP Generation 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.math_real.all;

entity CUNoC_IP is 
    generic (--CUNoC: Generics Start
             --AXIS Stream Params
             AXI_DATA_WIDTH     : integer := 64;
             --AXIS Switch Params
             NUM_SLAVE          : integer := 6; 
             NUM_MASTER         : integer := 6; 
             NUM_REG            : integer := 15; 
             --AXI Lite Params
             C_S_AXI_DATA_WIDTH : integer := 32; 
             C_S_AXI_ADDR_WIDTH : integer := 6;
             --Radio Params
             IQ_DATA_WIDTH      : integer := 12;
             RAD_FIFO_DEPTH     : integer := 12;
             TLAST_PER          : integer := 1048576);
             --CUNoC: Generics End
    port(
     -- AXIS Clocks/Resets
     AXIS_ACLK     : in  std_logic;
     AXIS_RESETN   : in  std_logic;
     DSP_CLK       : in  std_logic;
     -- AXIS Slave Interface0
     S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
     S0_AXIS_TVALID : in  std_logic;
     M0_AXIS_TREADY : in  std_logic;
     S0_AXIS_TLAST  : in  std_logic;
     S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
     -- AXIS Master Interface0 
     M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
     M0_AXIS_TVALID : out std_logic;
     S0_AXIS_TREADY : out std_logic;
     M0_AXIS_TLAST  : out std_logic;
     M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0); 
     -- AXI Lite Slave Interface For Setting Control
     S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
     S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
     S_AXI_AWVALID : in std_logic;
     S_AXI_AWREADY : out std_logic;
     S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  
     S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
     S_AXI_WVALID  : in std_logic;
     S_AXI_WREADY  : out std_logic;
     S_AXI_BRESP   : out std_logic_vector(1 downto 0);
     S_AXI_BVALID  : out std_logic;
     S_AXI_BREADY  : in std_logic;
     S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
     S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
     S_AXI_ARVALID : in std_logic;
     S_AXI_ARREADY : out std_logic;
     S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
     S_AXI_RRESP   : out std_logic_vector(1 downto 0);
     S_AXI_RVALID  : out std_logic;
     S_AXI_RREADY  : in std_logic;
     -- Radio Signals
     -- RX PHY 
     rx_clk_in_p   : in  std_logic;
     rx_clk_in_n   : in  std_logic;
     rx_frame_in_p : in  std_logic;
     rx_frame_in_n : in  std_logic;
     rx_data_in_p  : in  std_logic_vector(5 downto 0);
     rx_data_in_n  : in  std_logic_vector(5 downto 0);
     -- TX PHY
     tx_clk_out_p   : out std_logic;
     tx_clk_out_n   : out std_logic;
     tx_data_out_p  : out std_logic_vector(5 downto 0);
     tx_data_out_n  : out std_logic_vector(5 downto 0);
     tx_frame_out_p : out std_logic;
     tx_frame_out_n : out std_logic);
end CUNoC_IP;

architecture behavior of CUNoC_IP is 
    --CUNoC: Default Constants, Components, Signals Begin
    
    --CUNoC: Default Component0: AXIS_SWITCH_TOP Begin
    component AXIS_SWITCH_TOP is 
        generic (AXI_DATA_WIDTH     : integer := AXI_DATA_WIDTH;
                 NUM_SLAVE          : integer := NUM_SLAVE; 
                 NUM_MASTER         : integer := NUM_MASTER; 
                 NUM_REG            : integer := NUM_REG; 
                 C_S_AXI_DATA_WIDTH : integer := C_S_AXI_DATA_WIDTH;
                 C_S_AXI_ADDR_WIDTH : integer := C_S_AXI_ADDR_WIDTH);
        port(AXIS_ACLK     : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             S_AXIS_TDATA  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
             S_AXIS_TVALID : in  std_logic_vector(NUM_SLAVE-1 downto 0);
             M_AXIS_TREADY : in  std_logic_vector(NUM_MASTER-1 downto 0);
             S_AXIS_TLAST  : in  std_logic_vector(NUM_SLAVE-1 downto 0);
             S_AXIS_TKEEP  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
             M_AXIS_TDATA  : out std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH-1 downto 0);
             M_AXIS_TVALID : out std_logic_vector(NUM_MASTER-1 downto 0);
             S_AXIS_TREADY : out std_logic_vector(NUM_SLAVE-1 downto 0);
             M_AXIS_TLAST  : out std_logic_vector(NUM_MASTER-1 downto 0);
             M_AXIS_TKEEP  : out std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH/8-1 downto 0);
             S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
             S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
             S_AXI_AWVALID : in std_logic;
             S_AXI_AWREADY : out std_logic;
             S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  
             S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
             S_AXI_WVALID  : in std_logic;
             S_AXI_WREADY  : out std_logic;
             S_AXI_BRESP   : out std_logic_vector(1 downto 0);
             S_AXI_BVALID  : out std_logic;
             S_AXI_BREADY  : in std_logic;
             S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
             S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
             S_AXI_ARVALID : in std_logic;
             S_AXI_ARREADY : out std_logic;
             S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
             S_AXI_RRESP   : out std_logic_vector(1 downto 0);
             S_AXI_RVALID  : out std_logic;
             S_AXI_RREADY  : in std_logic;
             SWITCH_RSTN   : out std_logic);
    end component AXIS_SWITCH_TOP;
    --CUNoC: Default Component0: AXIS_SWITCH_TOP End

    --CUNoC: Default Component1: M_AXIS_Rx2NoC Begin
    component M_AXIS_Rx2NoC is 
        generic (AXI_DATA_WIDTH : integer := AXI_DATA_WIDTH;
                 AXI_FIFO_DEPTH : integer := RAD_FIFO_DEPTH;
                 IQ_DATA_WIDTH  : integer := IQ_DATA_WIDTH;
                 MEM_TYPE : string := "block");
        port(AXIS_ACLK     : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
             M0_AXIS_TVALID : out std_logic;
             M0_AXIS_TREADY : in  std_logic;
             M0_AXIS_TLAST  : out std_logic;
             M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
             tlast_period  : in std_logic_vector(31 downto 0); 
             over_flow     : out std_logic;
             RX_CLK        : in  std_logic;
             RX_VAL        : in  std_logic;
             I_DATA_IN     : in  std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
             Q_DATA_IN     : in  std_logic_vector(IQ_DATA_WIDTH-1 downto 0));
    end component M_AXIS_Rx2NoC;        
    --CUNoC: Default Component1: M_AXIS_Rx2NoC End
    
    --CUNoC: Default Component2: S_AXIS_NoC2Tx Begin
    component S_AXIS_NoC2Tx is 
        generic (AXI_DATA_WIDTH : integer := AXI_DATA_WIDTH;
                 AXI_FIFO_DEPTH : integer := RAD_FIFO_DEPTH;
                 IQ_DATA_WIDTH  : integer := IQ_DATA_WIDTH;
                 MEM_TYPE : string := "block");
        port(AXIS_ACLK     : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
             S0_AXIS_TVALID : in  std_logic;
             S0_AXIS_TREADY : out std_logic;
             S0_AXIS_TLAST  : in  std_logic;
             S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
             RX_CLK        : in  std_logic;
             RX_VAL        : in  std_logic;
             TX_CLK        : out std_logic;
             TX_VAL        : out std_logic;
             I_DATA_OUT    : out std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
             Q_DATA_OUT    : out std_logic_vector(IQ_DATA_WIDTH-1 downto 0));
    end component S_AXIS_NoC2Tx;
    --CUNoC: Default Component2: S_AXIS_NoC2Tx End
    
    --CUNoC: Default Component3: RX_PHY Begin
    component rx_phy_top is 
        port(rx_clk_in_p   : in  std_logic;
             rx_clk_in_n   : in  std_logic;
             rx_frame_in_p : in  std_logic;
             rx_frame_in_n : in  std_logic;
             rx_data_in_p  : in  std_logic_vector(5 downto 0);
             rx_data_in_n  : in  std_logic_vector(5 downto 0);
             rx_clk_out    : out std_logic;
             rx1_I_data    : out std_logic_vector(11 downto 0); 
             rx1_Q_data    : out std_logic_vector(11 downto 0);
             rx2_I_data    : out std_logic_vector(11 downto 0);
             rx2_Q_data    : out std_logic_vector(11 downto 0); 
             rx_val        : out std_logic);
    end component rx_phy_top;
    --CUNoC: Default Component3: RX_PHY End    

    --CUNoC: Default Component4: TX_PHY Begin
    component tx_phy_top is 
        port(tx_clk_in      : in  std_logic;
             tx_I1_in       : in  std_logic_vector(11 downto 0);
             tx_Q1_in       : in  std_logic_vector(11 downto 0);
             tx_I2_in       : in  std_logic_vector(11 downto 0);
             tx_Q2_in       : in  std_logic_vector(11 downto 0);
             tx_val_in      : in  std_logic;
             tx_clk_out_p   : out std_logic;
             tx_clk_out_n   : out std_logic;
             tx_data_out_p  : out std_logic_vector(5 downto 0);
             tx_data_out_n  : out std_logic_vector(5 downto 0);
             tx_frame_out_p : out std_logic;
             tx_frame_out_n : out std_logic);
    end component tx_phy_top;
    --CUNoC: Default Component4: TX_PHY End
    
    --CUNoC: Default Signals0: AXIS_SWITCH_TOP Begin
    signal S_AXIS_TDATA_int : std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
    signal S_AXIS_TVALID_int : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal M_AXIS_TREADY_int : std_logic_vector(NUM_MASTER-1 downto 0);
    signal S_AXIS_TLAST_int : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal S_AXIS_TKEEP_int : std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
    signal M_AXIS_TDATA_int : std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH-1 downto 0);
    signal M_AXIS_TVALID_int : std_logic_vector(NUM_MASTER-1 downto 0);
    signal S_AXIS_TREADY_int : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal M_AXIS_TLAST_int : std_logic_vector(NUM_MASTER-1 downto 0);
    signal M_AXIS_TKEEP_int : std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH/8-1 downto 0);
    signal SWITCH_RSTN : std_logic;
    --CUNoC: Default Signals0: AXIS_SWITCH_TOP End    

    --CUNoC: Default Signals1: M_AXIS_Rx2NoC Begin
    signal RX_CLK_int : std_logic;
    signal RX_VAL_int : std_logic;
    signal TX_CLK_int : std_logic;
    signal TX_VAL1_int : std_logic;
    signal TX_VAL2_int : std_logic;
    signal TX_VAL_int : std_logic;
    signal I_DATA_IN1_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
    signal Q_DATA_IN1_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
    signal I_DATA_IN2_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
    signal Q_DATA_IN2_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);    
    --CUNoC: Default Signals1: M_AXIS_Rx2NoC End

    --CUNoC: Default Signals2: S_AXIS_NoC2Tx Begin
    signal I_DATA_OUT1_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
    signal Q_DATA_OUT1_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
    signal I_DATA_OUT2_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
    signal Q_DATA_OUT2_int : std_logic_vector(IQ_DATA_WIDTH-1 downto 0);    
    --CUNoC: Default Signals2: S_AXIS_NoC2Tx End
    
    --CUNoC: Default Constants, Components, Signals End
    
    --CUNoC: Constants Begin

    --CUNoC: Constants End
    
    --CUNoC: Components Begin
    
    component Adder_AXIS_F2_2clk is 
        generic (--CUNoC: Comp Generics
                 AXI_DATA_WIDTH : integer := 64;
                 S_AXI_FIFO_DEPTH : integer := 6;
                 ADD_VAL : integer := 69;
                 M_AXI_FIFO_DEPTH : integer := 7);
        port(AXIS_ACLK	   : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             DSP_CLK       : in  std_logic; 
    		 -- Slave Interfaces
    		 S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
    		 S0_AXIS_TVALID : in  std_logic;
    		 S0_AXIS_TREADY : out std_logic;
    		 S0_AXIS_TLAST  : in  std_logic;
    		 S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
    		 -- Master Interfaces 
    		 M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
    		 M0_AXIS_TVALID : out std_logic;
    		 M0_AXIS_TREADY : in  std_logic;
    		 M0_AXIS_TLAST  : out std_logic;
    		 M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0));
    end component Adder_AXIS_F2_2clk;
    
    component NCI_AXIS_FIFO is 
        generic (--CUNoC: Comp Generics
                 AXI_DATA_WIDTH : integer := 64;
                 S_AXI_FIFO_DEPTH : integer := 6;
                 NCI_Stages : integer := 4;
                 M_AXI_FIFO_DEPTH : integer := 7);
        port(AXIS_ACLK	   : in  std_logic;
             AXIS_RESETN   : in  std_logic;
           --DSP_CLK       : in  std_logic; 
    		 -- Slave Interfaces
    		 S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
    		 S0_AXIS_TVALID : in  std_logic;
    		 S0_AXIS_TREADY : out std_logic;
    		 S0_AXIS_TLAST  : in  std_logic;
    		 S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
    		 -- Master Interfaces 
    		 M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
    		 M0_AXIS_TVALID : out std_logic;
    		 M0_AXIS_TREADY : in  std_logic;
    		 M0_AXIS_TLAST  : out std_logic;
    		 M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0));
    end component NCI_AXIS_FIFO;
    
    --CUNoC: Components End
    
    --CUNoC: Signals Begin
    
    --CUNoC: Signals End
    
begin
    --CUNoC: Default Component Instantiation and Signal Assignments Begin
    
    -- RX PHY Instantiation
    RX_PHY_inst: rx_phy_top
    port map(rx_clk_in_p   => rx_clk_in_p,
             rx_clk_in_n   => rx_clk_in_n,
             rx_frame_in_p => rx_frame_in_p,
             rx_frame_in_n => rx_frame_in_n,
             rx_data_in_p  => rx_data_in_p,
             rx_data_in_n  => rx_data_in_n,
             rx_clk_out    => RX_CLK_int,
             rx1_I_data    => I_DATA_IN1_int,
             rx1_Q_data    => Q_DATA_IN1_int,
             rx2_I_data    => I_DATA_IN2_int,
             rx2_Q_data    => Q_DATA_IN2_int,
             rx_val        => RX_VAL_int);
    
    -- TX PHY Instantiation         
    TX_PHY_inst: tx_phy_top
    port map(tx_clk_in      => TX_CLK_int,
             tx_I1_in       => I_DATA_OUT1_int,
             tx_Q1_in       => Q_DATA_OUT1_int,
             tx_I2_in       => I_DATA_OUT2_int,
             tx_Q2_in       => Q_DATA_OUT2_int,
             tx_val_in      => TX_VAL_int,
             tx_clk_out_p   => tx_clk_out_p,
             tx_clk_out_n   => tx_clk_out_n,
             tx_data_out_p  => tx_data_out_p,
             tx_data_out_n  => tx_data_out_n,
             tx_frame_out_p => tx_frame_out_p,
             tx_frame_out_n => tx_frame_out_n);
    
    -- RX PHY AXIS ADAPTER 1, Connected to M01
    RX_PHY_AXIS1_inst: M_AXIS_Rx2NoC
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             M0_AXIS_TDATA  => S_AXIS_TDATA_int((1+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*1),
             M0_AXIS_TVALID => S_AXIS_TVALID_int(1),
             M0_AXIS_TREADY => S_AXIS_TREADY_int(1),
             M0_AXIS_TLAST  => S_AXIS_TLAST_int(1),
             M0_AXIS_TKEEP  => S_AXIS_TKEEP_int((1+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*1),
             tlast_period  => std_logic_vector(to_unsigned(TLAST_PER,32)),
             over_flow     => open,
             RX_CLK        => RX_CLK_int,
             RX_VAL        => RX_VAL_int,
             I_DATA_IN     => I_DATA_IN1_int,
             Q_DATA_IN     => Q_DATA_IN1_int);

    -- RX PHY AXIS ADAPTER 2, Connected to M02
    RX_PHY_AXIS2_inst: M_AXIS_Rx2NoC
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             M0_AXIS_TDATA  => S_AXIS_TDATA_int((2+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*2),
             M0_AXIS_TVALID => S_AXIS_TVALID_int(2),
             M0_AXIS_TREADY => S_AXIS_TREADY_int(2),
             M0_AXIS_TLAST  => S_AXIS_TLAST_int(2),
             M0_AXIS_TKEEP  => S_AXIS_TKEEP_int((2+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*2),
             tlast_period  => std_logic_vector(to_unsigned(TLAST_PER,32)),
             over_flow     => open,
             RX_CLK        => RX_CLK_int,
             RX_VAL        => RX_VAL_int,
             I_DATA_IN     => I_DATA_IN2_int,
             Q_DATA_IN     => Q_DATA_IN2_int);

    -- TX PHY AXIS ADAPTER 1, Connected to S01
    TX_PHY_AXIS1_inst: S_AXIS_NoC2Tx
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             S0_AXIS_TDATA  => M_AXIS_TDATA_int((1+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*1),
             S0_AXIS_TVALID => M_AXIS_TVALID_int(1),
             S0_AXIS_TREADY => M_AXIS_TREADY_int(1),
             S0_AXIS_TLAST  => M_AXIS_TLAST_int(1),
             S0_AXIS_TKEEP  => M_AXIS_TKEEP_int((1+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*1),
             RX_CLK        => RX_CLK_int,
             RX_VAL        => RX_VAL_int,
             TX_CLK        => TX_CLK_int,
             TX_VAL        => TX_VAL1_int,
             I_DATA_OUT    => I_DATA_OUT1_int,
             Q_DATA_OUT    => Q_DATA_OUT1_int);
            
    -- TX PHY AXIS ADAPTER 2, Connected to S02
    TX_PHY_AXIS2_inst: S_AXIS_NoC2Tx
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             S0_AXIS_TDATA  => M_AXIS_TDATA_int((2+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*2),
             S0_AXIS_TVALID => M_AXIS_TVALID_int(2),
             S0_AXIS_TREADY => M_AXIS_TREADY_int(2),
             S0_AXIS_TLAST  => M_AXIS_TLAST_int(2),
             S0_AXIS_TKEEP  => M_AXIS_TKEEP_int((2+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*2),
             RX_CLK        => RX_CLK_int,
             RX_VAL        => RX_VAL_int,
             TX_CLK        => TX_CLK_int,
             TX_VAL        => TX_VAL2_int,
             I_DATA_OUT    => I_DATA_OUT2_int,
             Q_DATA_OUT    => Q_DATA_OUT2_int);            
             
    TX_VAL_int <= TX_VAL1_int or TX_VAL2_int;          
             
    -- AXIS Switch Instantiation
    AXIS_SWITCH_TOP_inst: AXIS_SWITCH_TOP
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             S_AXIS_TDATA  => S_AXIS_TDATA_int, 
             S_AXIS_TVALID => S_AXIS_TVALID_int,
             M_AXIS_TREADY => M_AXIS_TREADY_int,
             S_AXIS_TLAST  => S_AXIS_TLAST_int, 
             S_AXIS_TKEEP  => S_AXIS_TKEEP_int, 
             M_AXIS_TDATA  => M_AXIS_TDATA_int, 
             M_AXIS_TVALID => M_AXIS_TVALID_int,
             S_AXIS_TREADY => S_AXIS_TREADY_int,
             M_AXIS_TLAST  => M_AXIS_TLAST_int, 
             M_AXIS_TKEEP  => M_AXIS_TKEEP_int, 
             S_AXI_AWADDR  => S_AXI_AWADDR, 
             S_AXI_AWPROT  => S_AXI_AWPROT, 
             S_AXI_AWVALID => S_AXI_AWVALID,
             S_AXI_AWREADY => S_AXI_AWREADY,
             S_AXI_WDATA   => S_AXI_WDATA,  
             S_AXI_WSTRB   => S_AXI_WSTRB,  
             S_AXI_WVALID  => S_AXI_WVALID, 
             S_AXI_WREADY  => S_AXI_WREADY, 
             S_AXI_BRESP   => S_AXI_BRESP,  
             S_AXI_BVALID  => S_AXI_BVALID, 
             S_AXI_BREADY  => S_AXI_BREADY, 
             S_AXI_ARADDR  => S_AXI_ARADDR, 
             S_AXI_ARPROT  => S_AXI_ARPROT, 
             S_AXI_ARVALID => S_AXI_ARVALID,
             S_AXI_ARREADY => S_AXI_ARREADY,
             S_AXI_RDATA   => S_AXI_RDATA,  
             S_AXI_RRESP   => S_AXI_RRESP,  
             S_AXI_RVALID  => S_AXI_RVALID, 
             S_AXI_RREADY  => S_AXI_RREADY, 
             SWITCH_RSTN   => SWITCH_RSTN);
        
    -- AXIS S0 Assignments         
    S_AXIS_TDATA_int((0+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*0)     <= S0_AXIS_TDATA;  
    S_AXIS_TVALID_int(0)                                                 <= S0_AXIS_TVALID; 
    M_AXIS_TREADY_int(0)                                                 <= M0_AXIS_TREADY; 
    S_AXIS_TLAST_int(0)                                                  <= S0_AXIS_TLAST;  
    S_AXIS_TKEEP_int((0+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*0) <= S0_AXIS_TKEEP;    

    -- AXIS M0 Assignments         
    M0_AXIS_TDATA  <= M_AXIS_TDATA_int((0+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*0); 
    M0_AXIS_TVALID <= M_AXIS_TVALID_int(0);
    S0_AXIS_TREADY <= S_AXIS_TREADY_int(0);
    M0_AXIS_TLAST  <= M_AXIS_TLAST_int(0); 
    M0_AXIS_TKEEP  <= M_AXIS_TKEEP_int((0+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*0);  
         
    --CUNoC: Default Component Instantiation and Signal Assignments End
 
    --CUNoC: Component Instantiation Begin
    
    Adder_AXIS_F2_2clk_0_inst: Adder_AXIS_F2_2clk
    generic map(--CUNoC: Inst Generics
                AXI_DATA_WIDTH => 64,
                S_AXI_FIFO_DEPTH => 6,
                ADD_VAL => 69,
                M_AXI_FIFO_DEPTH => 7)
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             DSP_CLK       => DSP_CLK,
             -- SLAVE ASSIGNMENTS
             S0_AXIS_TDATA  => M_AXIS_TDATA_int((3+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*3),
             S0_AXIS_TVALID => M_AXIS_TVALID_int(3),
             S0_AXIS_TREADY => M_AXIS_TREADY_int(3),
             S0_AXIS_TLAST  => M_AXIS_TLAST_int(3),
             S0_AXIS_TKEEP  => M_AXIS_TKEEP_int((3+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*3),
             -- MASTER ASSIGNMENTS
             M0_AXIS_TDATA  => S_AXIS_TDATA_int((3+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*3),
             M0_AXIS_TVALID => S_AXIS_TVALID_int(3),
             M0_AXIS_TREADY => S_AXIS_TREADY_int(3),
             M0_AXIS_TLAST  => S_AXIS_TLAST_int(3),
             M0_AXIS_TKEEP  => S_AXIS_TKEEP_int((3+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*3));
    
    Adder_AXIS_F2_2clk_1_inst: Adder_AXIS_F2_2clk
    generic map(--CUNoC: Inst Generics
                AXI_DATA_WIDTH => 64,
                S_AXI_FIFO_DEPTH => 6,
                ADD_VAL => 39,
                M_AXI_FIFO_DEPTH => 7)
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
             DSP_CLK       => DSP_CLK,
             -- SLAVE ASSIGNMENTS
             S0_AXIS_TDATA  => M_AXIS_TDATA_int((4+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*4),
             S0_AXIS_TVALID => M_AXIS_TVALID_int(4),
             S0_AXIS_TREADY => M_AXIS_TREADY_int(4),
             S0_AXIS_TLAST  => M_AXIS_TLAST_int(4),
             S0_AXIS_TKEEP  => M_AXIS_TKEEP_int((4+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*4),
             -- MASTER ASSIGNMENTS
             M0_AXIS_TDATA  => S_AXIS_TDATA_int((4+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*4),
             M0_AXIS_TVALID => S_AXIS_TVALID_int(4),
             M0_AXIS_TREADY => S_AXIS_TREADY_int(4),
             M0_AXIS_TLAST  => S_AXIS_TLAST_int(4),
             M0_AXIS_TKEEP  => S_AXIS_TKEEP_int((4+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*4));
    
    NCI_AXIS_FIFO_0_inst: NCI_AXIS_FIFO
    generic map(--CUNoC: Inst Generics
                AXI_DATA_WIDTH => 64,
                S_AXI_FIFO_DEPTH => 6,
                NCI_Stages => 4,
                M_AXI_FIFO_DEPTH => 7)
    port map(AXIS_ACLK     => AXIS_ACLK,
             AXIS_RESETN   => AXIS_RESETN,
           --DSP_CLK       => DSP_CLK,
             -- SLAVE ASSIGNMENTS
             S0_AXIS_TDATA  => M_AXIS_TDATA_int((5+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*5),
             S0_AXIS_TVALID => M_AXIS_TVALID_int(5),
             S0_AXIS_TREADY => M_AXIS_TREADY_int(5),
             S0_AXIS_TLAST  => M_AXIS_TLAST_int(5),
             S0_AXIS_TKEEP  => M_AXIS_TKEEP_int((5+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*5),
             -- MASTER ASSIGNMENTS
             M0_AXIS_TDATA  => S_AXIS_TDATA_int((5+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*5),
             M0_AXIS_TVALID => S_AXIS_TVALID_int(5),
             M0_AXIS_TREADY => S_AXIS_TREADY_int(5),
             M0_AXIS_TLAST  => S_AXIS_TLAST_int(5),
             M0_AXIS_TKEEP  => S_AXIS_TKEEP_int((5+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*5));
    
    --CUNoC: Component Instantiation End
    
    --CUNoC: Signal Assignments Begin

    --CUNoC: Signal Assignments End
    
end behavior;
