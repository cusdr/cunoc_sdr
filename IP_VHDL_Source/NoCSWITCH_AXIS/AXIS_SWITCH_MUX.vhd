-- Open Source AXI4 Stream Switch Mux
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.math_real.all;

entity AXIS_SWITCH_MUX is 
    generic (AXI_DATA_WIDTH : integer := 8;
			 NUM_SLAVE : integer := 4);
    port(-- AXIS Clocks/Resets
		 AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- Slave Interface
		 S_AXIS_TDATA  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
		 S_AXIS_TVALID : in  std_logic_vector(NUM_SLAVE-1 downto 0);
		 S_AXIS_TLAST  : in  std_logic_vector(NUM_SLAVE-1 downto 0);
		 S_AXIS_TKEEP  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
		 -- Master Interface 
		 M_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 M_AXIS_TVALID : out std_logic;
		 M_AXIS_TLAST  : out std_logic;
		 M_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
         -- Control Interface
         SEL : in std_logic_vector(integer(ceil(log2(real(NUM_SLAVE))))-1 downto 0) 
       );
end AXIS_SWITCH_MUX;

architecture behavior of AXIS_SWITCH_MUX is
    type KEEP_ARR is array(NUM_SLAVE-1 downto 0) of std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
    type DAT_ARR is array(NUM_SLAVE-1 downto 0) of std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
    type BIN_ARR is array(NUM_SLAVE-1 downto 0) of std_logic_vector(0 downto 0);
    signal DAT_TMP : DAT_ARR;
    signal VAL_TMP : BIN_ARR;
    signal LAST_TMP : BIN_ARR;
    signal KEEP_TMP : KEEP_ARR;
    signal SEL_INT : integer := 0;
begin
    SEL_INT <= to_integer(unsigned(SEL));
    
    mux_loop: for I in 0 to NUM_SLAVE-1 generate
        DAT_TMP(I) <= S_AXIS_TDATA((I+1)*AXI_DATA_WIDTH-1 downto AXI_DATA_WIDTH*I); 
        VAL_TMP(I) <= S_AXIS_TVALID(I downto I);
        LAST_TMP(I) <= S_AXIS_TLAST(I downto I);
        KEEP_TMP(I) <= S_AXIS_TKEEP((I+1)*AXI_DATA_WIDTH/8-1 downto AXI_DATA_WIDTH/8*I);
    end generate mux_loop;
    
    M_AXIS_TDATA <= DAT_TMP(SEL_INT);
    M_AXIS_TVALID <= VAL_TMP(SEL_INT)(0);
    M_AXIS_TLAST <= LAST_TMP(SEL_INT)(0);
    M_AXIS_TKEEP <= KEEP_TMP(SEL_INT);
end behavior;  
