-- Open Source AXI4 Stream Switch
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.math_real.all;

entity AXIS_SWITCH_TOP is 
    generic (AXI_DATA_WIDTH     : integer := 64;-- AXI STREAM Data Width
             NUM_SLAVE          : integer := 2; -- Number of AXIS Slave interfaces
             NUM_MASTER         : integer := 4; -- Numer of AXIS Master interfaces
             NUM_REG            : integer := 2+4+3; -- = NUM_SLAVE+NUM_MASTER+3 (0 -> commit reg, 1 -> Masters Enable reg, 2 -> Slaves Enable reg) 
             C_S_AXI_DATA_WIDTH : integer := 32; -- AXI Lite Data Width
             C_S_AXI_ADDR_WIDTH : integer := 4+2); -- ceil(log2(NUM_REG))+2 
    port(-- AXIS Clocks/Resets
     AXIS_ACLK     : in  std_logic;
     AXIS_RESETN   : in  std_logic;
     -- AXI Stream Interfaces
     -- Slave Interface
     S_AXIS_TDATA  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
     S_AXIS_TVALID : in  std_logic_vector(NUM_SLAVE-1 downto 0);
     M_AXIS_TREADY : in  std_logic_vector(NUM_MASTER-1 downto 0);
     S_AXIS_TLAST  : in  std_logic_vector(NUM_SLAVE-1 downto 0);
     S_AXIS_TKEEP  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
     -- Master Interface 
     M_AXIS_TDATA  : out std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH-1 downto 0);
     M_AXIS_TVALID : out std_logic_vector(NUM_MASTER-1 downto 0);
     S_AXIS_TREADY : out std_logic_vector(NUM_SLAVE-1 downto 0);
     M_AXIS_TLAST  : out std_logic_vector(NUM_MASTER-1 downto 0);
     M_AXIS_TKEEP  : out std_logic_vector(NUM_MASTER*AXI_DATA_WIDTH/8-1 downto 0);
     -- AXI Lite Interface For Setting Control
     S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
     S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
     S_AXI_AWVALID : in std_logic;
     S_AXI_AWREADY : out std_logic;
     S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  
     S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
     S_AXI_WVALID  : in std_logic;
     S_AXI_WREADY  : out std_logic;
     S_AXI_BRESP   : out std_logic_vector(1 downto 0);
     S_AXI_BVALID  : out std_logic;
     S_AXI_BREADY  : in std_logic;
     S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
     S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
     S_AXI_ARVALID : in std_logic;
     S_AXI_ARREADY : out std_logic;
     S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
     S_AXI_RRESP   : out std_logic_vector(1 downto 0);
     S_AXI_RVALID  : out std_logic;
     S_AXI_RREADY  : in std_logic;
     -- Resetn Signal out
     SWITCH_RSTN   : out std_logic);
end AXIS_SWITCH_TOP;

architecture behavior of AXIS_SWITCH_TOP is 
    -- Constants
    constant sel_wid_m : integer := integer(ceil(log2(real(NUM_SLAVE))));
    constant sel_wid_s : integer := integer(ceil(log2(real(NUM_MASTER))));
    -- Signals 
    -- AXI Lite Regs
    signal AXI_LITE_REG : std_logic_vector(NUM_REG*C_S_AXI_DATA_WIDTH-1 downto 0);
    type REG_ARR is array(NUM_REG-1 downto 0) of std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal AXI_LITE_REG_ARR : REG_ARR;
    -- AXI Control signals 
    signal UPLOAD_FLAG : std_logic;
    signal MASTER_EN : std_logic_vector(NUM_MASTER-1 downto 0);
    signal SLAVE_EN : std_logic_vector(NUM_SLAVE-1 downto 0);
    -- Mux Select
    signal SEL_MASTER : std_logic_vector(NUM_MASTER*sel_wid_m-1 downto 0); 
    signal SEL_SLAVE : std_logic_vector(NUM_SLAVE*sel_wid_s-1 downto 0);
    signal RDY_OUT_INT : std_logic_vector(NUM_SLAVE-1 downto 0);
    signal VAL_OUT_INT : std_logic_vector(NUM_MASTER-1 downto 0);
    -- Components
    component AXIS_SWITCH_MUX is 
        generic (AXI_DATA_WIDTH : integer := AXI_DATA_WIDTH;
                 NUM_SLAVE : integer := NUM_SLAVE);
        port(AXIS_ACLK     : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             S_AXIS_TDATA  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH-1 downto 0);
             S_AXIS_TVALID : in  std_logic_vector(NUM_SLAVE-1 downto 0);
             S_AXIS_TLAST  : in  std_logic_vector(NUM_SLAVE-1 downto 0);
             S_AXIS_TKEEP  : in  std_logic_vector(NUM_SLAVE*AXI_DATA_WIDTH/8-1 downto 0);
             M_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
             M_AXIS_TVALID : out std_logic;
             M_AXIS_TLAST  : out std_logic;
             M_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
             SEL : in std_logic_vector(integer(ceil(log2(real(NUM_SLAVE))))-1 downto 0));
    end component AXIS_SWITCH_MUX;
    
    component AXIS_RDY_MUX is 
        generic (NUM_MASTER : integer := NUM_MASTER);
        port(AXIS_ACLK     : in  std_logic;
             AXIS_RESETN   : in  std_logic;
             S_AXIS_TREADY : out std_logic;
             M_AXIS_TREADY : in  std_logic_vector(NUM_MASTER-1 downto 0);
             SEL           : in std_logic_vector(integer(ceil(log2(real(NUM_MASTER))))-1 downto 0));
    end component AXIS_RDY_MUX;

    component S_AXI_REG is
        generic (NUM_REG            : integer := NUM_REG; 
                 C_S_AXI_DATA_WIDTH : integer := C_S_AXI_DATA_WIDTH;
                 C_S_AXI_ADDR_WIDTH : integer := C_S_AXI_ADDR_WIDTH);
        port (reg_out       : out std_logic_vector(NUM_REG*C_S_AXI_DATA_WIDTH-1 downto 0);    
              S_AXI_ACLK    : in std_logic;
              S_AXI_ARESETN : in std_logic;
              S_AXI_AWADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
              S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
              S_AXI_AWVALID : in std_logic;
              S_AXI_AWREADY : out std_logic;
              S_AXI_WDATA   : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);  
              S_AXI_WSTRB   : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
              S_AXI_WVALID  : in std_logic;
              S_AXI_WREADY  : out std_logic;
              S_AXI_BRESP   : out std_logic_vector(1 downto 0);
              S_AXI_BVALID  : out std_logic;
              S_AXI_BREADY  : in std_logic;
              S_AXI_ARADDR  : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
              S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
              S_AXI_ARVALID : in std_logic;
              S_AXI_ARREADY : out std_logic;
              S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
              S_AXI_RRESP   : out std_logic_vector(1 downto 0);
              S_AXI_RVALID  : out std_logic;
              S_AXI_RREADY  : in std_logic);
    end component S_AXI_REG;

begin
    -- End Debug Signals 
    AXI_LITE_INST: S_AXI_REG
    port map(reg_out => AXI_LITE_REG,
             S_AXI_ACLK => AXIS_ACLK,
             S_AXI_ARESETN => AXIS_RESETN,
             S_AXI_AWADDR => S_AXI_AWADDR,
             S_AXI_AWPROT => S_AXI_AWPROT,
             S_AXI_AWVALID => S_AXI_AWVALID,
             S_AXI_AWREADY => S_AXI_AWREADY,
             S_AXI_WDATA => S_AXI_WDATA,
             S_AXI_WSTRB => S_AXI_WSTRB,
             S_AXI_WVALID => S_AXI_WVALID,
             S_AXI_WREADY => S_AXI_WREADY,
             S_AXI_BRESP => S_AXI_BRESP,
             S_AXI_BVALID => S_AXI_BVALID,
             S_AXI_BREADY => S_AXI_BREADY,
             S_AXI_ARADDR => S_AXI_ARADDR,
             S_AXI_ARPROT => S_AXI_ARPROT,
             S_AXI_ARVALID => S_AXI_ARVALID,
             S_AXI_ARREADY => S_AXI_ARREADY,
             S_AXI_RDATA => S_AXI_RDATA,
             S_AXI_RRESP => S_AXI_RRESP,
             S_AXI_RVALID => S_AXI_RVALID,
             S_AXI_RREADY => S_AXI_RREADY);    

    -- Generate Array assignments 
    REG_ARR_GEN: for I in 0 to NUM_REG-1 generate
        AXI_LITE_REG_ARR(I) <= AXI_LITE_REG((I+1)*C_S_AXI_DATA_WIDTH-1 downto I*C_S_AXI_DATA_WIDTH);
    end generate REG_ARR_GEN;

    -- Assign Register Values
    UPLOAD_FLAG <= AXI_LITE_REG_ARR(0)(0); -- The Lowest Bit of Reg 0
    MASTER_EN <= AXI_LITE_REG_ARR(1)(NUM_MASTER-1 downto 0); -- Lowest bits of Reg 1
    SLAVE_EN <= AXI_LITE_REG_ARR(2)(NUM_SLAVE-1 downto 0); -- Lowest bits of Reg 2
    -- Master Mux Select Assignments 
    MASTER_SEL_GEN: for M in 0 to NUM_MASTER-1 generate
    -- The array position is 3+M as there are 3 registers in front of this
        SEL_MASTER(sel_wid_m*(M+1)-1 downto sel_wid_m*M) <= AXI_LITE_REG_ARR(3+M)(sel_wid_m-1 downto 0);
    end generate MASTER_SEL_GEN;

    -- Slave Mux Select Assignments 
    SLAVE_SEL_GEN: for N in 0 to NUM_SLAVE-1 generate
    -- The array position is 3+NUM_MASTER+N as there are 3 main and NUM_MASTER master select registers in front of this
        SEL_SLAVE(sel_wid_s*(N+1)-1 downto sel_wid_s*N) <= AXI_LITE_REG_ARR(3+NUM_MASTER+N)(sel_wid_s-1 downto 0);
    end generate SLAVE_SEL_GEN;

    -- Generate MASTER Side Muxes
    MASTER_MUX_GEN: for I in 0 to NUM_MASTER-1 generate
        AXIS_MUX_INST: AXIS_SWITCH_MUX
        port map(AXIS_ACLK => AXIS_ACLK,
                 AXIS_RESETN => AXIS_RESETN,
                 S_AXIS_TDATA => S_AXIS_TDATA,
                 S_AXIS_TVALID => S_AXIS_TVALID,
                 S_AXIS_TLAST => S_AXIS_TLAST,
                 S_AXIS_TKEEP => S_AXIS_TKEEP,
                 M_AXIS_TDATA => M_AXIS_TDATA(AXI_DATA_WIDTH*(I+1)-1 downto AXI_DATA_WIDTH*I),
                 M_AXIS_TVALID => VAL_OUT_INT(I),
                 M_AXIS_TLAST  => M_AXIS_TLAST(I),
                 M_AXIS_TKEEP => M_AXIS_TKEEP(AXI_DATA_WIDTH/8*(I+1)-1 downto AXI_DATA_WIDTH/8*I),
                 SEL => SEL_MASTER(sel_wid_m*(I+1)-1 downto sel_wid_m*I));
    end generate MASTER_MUX_GEN;
    
    -- Generate SLAVE side Muxes
    SLAVE_MUX_GEN: for J in 0 to NUM_SLAVE-1 generate
        RDY_MUX_INST: AXIS_RDY_MUX
        port map(AXIS_ACLK => AXIS_ACLK,
                 AXIS_RESETN => AXIS_RESETN,
                 M_AXIS_TREADY => M_AXIS_TREADY,
                 S_AXIS_TREADY => RDY_OUT_INT(J),
                 SEL => SEL_SLAVE(sel_wid_s*(J+1)-1 downto sel_wid_s*J));
    end generate SLAVE_MUX_GEN;

    -- Validate The S_RDY and M_VAL outputs
    S_AXIS_TREADY <= RDY_OUT_INT and SLAVE_EN when UPLOAD_FLAG='0' else (others => '0');
    M_AXIS_TVALID <= VAL_OUT_INT and MASTER_EN when UPLOAD_FLAG='0' else (others => '0');

    SWITCH_RSTN <= AXIS_RESETN or not(UPLOAD_FLAG);-- falling edge instead? 

end behavior;  
