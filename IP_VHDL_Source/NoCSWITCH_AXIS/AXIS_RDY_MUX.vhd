-- Open Source AXI4 Stream Switch Mux
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.math_real.all;

entity AXIS_RDY_MUX is 
    generic (NUM_MASTER : integer := 3);
    port(-- AXIS Clocks/Resets
		 AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- Slave Interface
		 S_AXIS_TREADY : out std_logic;
		 -- Master Interface 
		 M_AXIS_TREADY : in  std_logic_vector(NUM_MASTER-1 downto 0);
         -- Control Interface
         SEL : in std_logic_vector(integer(ceil(log2(real(NUM_MASTER))))-1 downto 0) 
       );
end AXIS_RDY_MUX;

architecture behavior of AXIS_RDY_MUX is
    type BIN_ARR is array(NUM_MASTER-1 downto 0) of std_logic_vector(0 downto 0);
    signal RDY_TMP : BIN_ARR;
    signal SEL_INT : integer := 0;
begin
    SEL_INT <= to_integer(unsigned(SEL));
    
    mux_loop: for I in 0 to NUM_MASTER-1 generate
        RDY_TMP(I) <= M_AXIS_TREADY(I downto I);
    end generate mux_loop;
    
    S_AXIS_TREADY <= RDY_TMP(SEL_INT)(0);
end behavior;  
