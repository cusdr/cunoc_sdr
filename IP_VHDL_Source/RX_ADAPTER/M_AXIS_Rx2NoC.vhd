library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity M_AXIS_Rx2NoC is 
    generic (AXI_DATA_WIDTH : integer := 64;
	     AXI_FIFO_DEPTH : integer := 12;
	     IQ_DATA_WIDTH  : integer := 12;
	     MEM_TYPE : string := "block");
    port( -- AXIS Clock Domain --
		 AXIS_ACLK	   : in  std_logic;
         AXIS_RESETN   : in  std_logic;
		 -- Master Interface 
		 M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 M0_AXIS_TVALID : out std_logic;
		 M0_AXIS_TREADY : in  std_logic;
		 M0_AXIS_TLAST  : out std_logic;
		 M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Config Signal
		 tlast_period  : in std_logic_vector(31 downto 0); 
		 -- Diagnostic Signal
		 over_flow	   : out std_logic;
		 -- Rx Clock Domain (4x sample clock) --
		 RX_CLK    	   : in  std_logic;
		 RX_VAL        : in  std_logic;
		 I_DATA_IN     : in  std_logic_vector(IQ_DATA_WIDTH-1 downto 0);
		 Q_DATA_IN     : in  std_logic_vector(IQ_DATA_WIDTH-1 downto 0));
end M_AXIS_Rx2NoC;

architecture behavior of M_AXIS_Rx2NoC is
	-- Constant, Component, and Signal  Declarations
    -- Constants
	constant IQ_BUS_WIDTH : integer := 2*IQ_DATA_WIDTH;
	-- Components
	-- 2 clk FIFO
	component FIFO_ASYNC_PHY is
	    generic (DATA_WIDTH :integer := IQ_BUS_WIDTH;
		     ADDR_WIDTH :integer := AXI_FIFO_DEPTH;
		     MEM_DoB : string := MEM_TYPE);
	    port (-- Reading port.
			  Data_out    :out std_logic_vector (DATA_WIDTH-1 downto 0);
			  Empty_out   :out std_logic;
			  ReadEn_in   :in  std_logic;
			  RClk        :in  std_logic;
			  -- Writing port.
			  Data_in     :in  std_logic_vector (DATA_WIDTH-1 downto 0);
			  Full_out    :out std_logic;
			  WriteEn_in  :in  std_logic;
			  WClk        :in  std_logic;
			  -- rst
			  RRst_n	  :in  std_logic;
			  WRst_n	  :in  std_logic);
	end component FIFO_ASYNC_PHY;
	
	-- Synchronizer
	component clock_cross_bin is
		Port ( clk_in : in STD_LOGIC;
			   clk_out : in STD_LOGIC;
			   data_in : in STD_LOGIC;
			   data_out : out STD_LOGIC);
	end component clock_cross_bin;
	
	-- Simple Counter
	component counter_simp_en is 
		generic (data_width : integer := 32;
				 step_size  : integer := 1);
		port(clk	 : in   std_logic;
			 ce   : in   std_logic;
			 rst  : in   std_logic;
			 en   : in   std_logic;
			 dout : out	std_logic_vector(data_width-1 downto 0));
	end component counter_simp_en;
	
	-- Signals
	-- AXIS Clock Domain 
    -- Master Signals
	signal M_RE : std_logic;
	signal M_EMPTY : std_logic;
	signal M_VAL_REG : std_logic := '0';	
	signal LAST_REG : std_logic := '0'; 
	signal TLST_COUNT : std_logic_vector(31 downto 0);
	signal TLST_VAL : std_logic;
	
	-- FIFO Data Out
	signal FIFO_DOUT : std_logic_vector(IQ_BUS_WIDTH-1 downto 0);
	-- RX FIFO Reset in AXIS Domain 
	signal RX_AXIS_RESETN : std_logic;

	-- RX Clock Domain 
	signal RX_WE : std_logic;
	signal FULL_INT : std_logic;


begin
	-- Signal Assignments 
	-- MASTER Signal Assignments
	M0_AXIS_TKEEP <= (others => '1'); 
	M_RE <= M0_AXIS_TREADY and not(M_EMPTY);
	M0_AXIS_TVALID <= M_VAL_REG;
	M0_AXIS_TLAST <= TLST_VAL and not(LAST_REG);
	-- TDATA Assignment, 64-bit wide, zero top 32, signe extend I/Q to 16 bit and assign to lower 32 with I occupying the MSB and Q the LSB
	M0_AXIS_TDATA(AXI_DATA_WIDTH-1 downto 32) <= (others => '0'); -- Zero Pad top 32
	M0_AXIS_TDATA(31 downto 28) <= (others => FIFO_DOUT(IQ_BUS_WIDTH-1)); -- I Sign extend for 16 bit
	M0_AXIS_TDATA(27 downto 16) <= FIFO_DOUT(IQ_BUS_WIDTH-1 downto IQ_DATA_WIDTH); -- I Data   
	M0_AXIS_TDATA(15 downto 12) <= (others => FIFO_DOUT(IQ_DATA_WIDTH-1)); -- Q Sign extend for 16 bit
	M0_AXIS_TDATA(11 downto 0) <= FIFO_DOUT(IQ_DATA_WIDTH-1 downto 0); -- Q Data 

	TLST_VAL <= '1' when (TLST_COUNT = tlast_period) else '0';
	
	-- RX Signal Assignments
	RX_WE <= not(FULL_INT) and RX_VAL;
	 	
	fifo_master_2clk_inst: FIFO_ASYNC_PHY
		port map(Data_out => FIFO_DOUT,
			 Empty_out => M_EMPTY,
			 ReadEn_in => M_RE,
			 RClk => AXIS_ACLK, 
			 Data_in => (I_DATA_IN & Q_DATA_IN),
			 Full_out => FULL_INT,
			 WriteEn_in => RX_WE, 
			 WClk => RX_CLK, 
			 RRst_n => AXIS_RESETN,
			 WRst_n => RX_AXIS_RESETN);

	rst_synch_inst: clock_cross_bin
		port map(clk_in => AXIS_ACLK,
			 clk_out => RX_CLK,
			 data_in => AXIS_RESETN,
			 data_out => RX_AXIS_RESETN);
			 
	ovf_synch_inst: clock_cross_bin
		port map(clk_in => RX_CLK,
			 clk_out => AXIS_ACLK,
			 data_in => FULL_INT,
			 data_out => over_flow);	

	tlast_counter_inst: counter_simp_en
		port map(clk => AXIS_ACLK,
				 ce => '1',
				 rst => (not(AXIS_RESETN) or LAST_REG),
				 en => M_RE,
				 dout => TLST_COUNT);
				 
	AXIS_Reg_Proc: process (AXIS_ACLK,M_EMPTY,AXIS_RESETN,TLST_VAL) begin
		if (AXIS_RESETN = '0') then
			M_VAL_REG <= '0';
			LAST_REG <= '0'; 
        elsif AXIS_ACLK'event and AXIS_ACLK = '1' then						  
		    LAST_REG <= TLST_VAL;
			M_VAL_REG <= not(M_EMPTY); 
		end if;				 
	end process AXIS_Reg_Proc;
	
end behavior;  
	
	
