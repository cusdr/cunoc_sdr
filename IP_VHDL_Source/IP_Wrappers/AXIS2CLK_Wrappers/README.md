This directory contains wrapper files for two types of asynchronous FIFOs. 

FIFO Type 1
-----------
   These are described in:

http://www.sunburst-design.com/papers/CummingsSNUG2002SJ_FIFO1.pdf

They use classical write and read pointer comparisons. 

FIFO Type 2
-----------
   These are described in:

http://www.sunburst-design.com/papers/CummingsSNUG2002SJ_FIFO2.pdf

They use a special comparison that reduces latency on falling edge of full and empty by 1-clock cycle.

Both FIFO types were included as they have slightly different resource draws and could have implications for timing closure. Both function as desired and are compatible with the NoC architecture. Users are left to choose one or the other for their design. 

If a user has no real preference, it is recommended they use Type 1. 

Directory Contents
------------------

These directories contain VHDL files for wrapping pipelined, streaming DSP and making it compatible with the AXI-4 stream protocol and our AXI-4 stream switch. The files for two clock domain wrappers are in these directories. For a more detailed description of the architecture see the Docs/ directory in the git repository. 

The VHDL files in this repository can be used to adapt your existing DSP cores to our Network on Chip (NoC) architecture. This system uses an AXI-4 stream cross-bar switch as the NoC. The base DSP must be pipelined and streaming, that is it produces as much data as is consumed and can produce data on each valid clock cycle. It is important for the user to specify the total number of valid clock cycles z it takes the core to produce a valid result. To that end, the base interface is:

Inputs
------
1. Data In -> n-bit vector where n <= 64
2. Enable -> 1-bit enable high 
3. Reset -> 1-bit reset high

Outputs
-------
1. Data Out -> m-bit vector where m <= 64

Existing cores that are compatible with these requirements can be integrated directly following the "TEMPLATE_TOP.vhd" example. This example implements a basic data register DSP core called "DSP_TEMPLATE.vhd". In this example a single register is generated. The register has a total latency of one clock cycle and this is reflected in the example design. The wrappers encapsulate all the required support files in the AXIS_WRAPPER_pkg.vhd so that users need only add that file to a project and declare their own DSP directly in the VHDL template. 

Two FIFOs are instantiated to handle data transactions between the network and the DSP: one for the slave-side and one for the master-side. The PIPE_DSP_FSM.vhd is a finite-state-machine that handles transitioning data to and from the FIFOs. A modularized counter, shift-register, and FIFO are included to support the FSM and transition FIFOs. 

In this implementation, the FIFOs are also used to cross clock domains. The two clock domains are the network clock domain and the DSP clock domain. For one-bit values the clock domains are crossed using three register latches to avoid meta-stability. The FSM still ping-pongs between loading/unloading data with the network and pushing data between the FIFOs through the DSP cores. In this implementation, however, guard states are added between the main state transitions to allow clock-crossing signals to settle out in time for use in the new state. 


