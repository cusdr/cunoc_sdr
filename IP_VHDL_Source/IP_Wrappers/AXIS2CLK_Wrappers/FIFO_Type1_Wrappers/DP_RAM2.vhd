-- A parametrized dual-port dual-clock block RAM in VHDL.
 
library ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
entity DP_RAM2 is
generic (
    DATA    : integer := 64;
    ADDR    : integer := 10
);
port (
    -- Write Port
    wclk    : in  std_logic;
    wen     : in  std_logic;
    waddr   : in  std_logic_vector(ADDR-1 downto 0);
    wdata   : in  std_logic_vector(DATA-1 downto 0);
     
    -- Read Port
	rclk    : in  std_logic;
	ren     : in  std_logic;
    raddr   : in  std_logic_vector(ADDR-1 downto 0);
    rdata   : out std_logic_vector(DATA-1 downto 0)
);
end DP_RAM2;
 
architecture rtl of DP_RAM2 is
    -- Shared memory
    constant RAM_DEPTH : integer := 2**ADDR;
    type mem_type is array ( 0 to  RAM_DEPTH-1) of std_logic_vector(DATA-1 downto 0);
    shared variable mem : mem_type := (others => (others => '0'));
    attribute ram_style: string;
    attribute ram_style of mem : variable is "distributed";--"distributed", --"block"
	signal rdata_reg : STD_LOGIC_VECTOR(DATA-1 downto 0) := (others => '0');
begin

rdata <= rdata_reg; 
 
-- Read port
process(rclk)
begin
	if(rclk'event and rclk='1') then
		if(ren='1') then
			 rdata_reg <= mem(to_integer(unsigned(raddr)));
		end if;
	end if;
end process;

 -- Write port 
process(wclk)
begin
    if(wclk'event and wclk='1') then
        if(wen='1') then
            mem(to_integer(unsigned(waddr))) := wdata;
        end if;
    end if;
end process;
 
end rtl;