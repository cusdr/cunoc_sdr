----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin M Anderson 
-- 
-- Create Date: 07/12/2016 2:24 PM MDT
-- Design Name: DSP_TEMPLATE.vhd
-- Module Name: DSP_TEMPLATE - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: This is an example pipelined, streaming DSP core. 
-- 
-- Dependencies: None
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DSP_TEMPLATE is
    generic(data_width : integer := 64);
    port(clk      : in STD_LOGIC;
         en       : in STD_LOGIC;
         rst      : in STD_LOGIC;
         data_in  : in STD_LOGIC_VECTOR (data_width-1 downto 0);
         data_out : out STD_LOGIC_VECTOR (data_width-1 downto 0));
end DSP_TEMPLATE;

architecture Behavioral of DSP_TEMPLATE is
begin   
    process(clk,en,rst) begin
        if rst = '1' then 
            data_out <= (others => '0');
        elsif clk'event and clk='1' then
            if (en = '1') then 
                data_out <=  data_in;
            end if; 
        end if;
    end process;
end Behavioral;
