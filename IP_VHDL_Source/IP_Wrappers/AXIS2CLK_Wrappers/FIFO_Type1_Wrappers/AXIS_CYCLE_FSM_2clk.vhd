----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin Anderson 
-- 
-- Create Date: 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: Zedboard
-- Tool Versions: 
-- Description: Finite state machine to control CFAR detection logic.
-- 
-- Dependencies: TBD
-- 
-- Revision: 1
-- Revision 0.01 - File Created
-- Additional Comments:
-- I'm a bit rusty so be gentle...
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_signed.all;
use IEEE.math_real.all;

-- Currently for one clock, will upgrade if it works for 2 clocks 
-- 2 CLOCK UPGRADES:
--	- Add synchronizers for DSP signals in and out  
--	- Run the whole thing on the AXIS clock 
entity AXIS_CYCLE_FSM_2clk is
    Port (-- Sysgen Simulation Signals 
		  --clk, rst, ce  : in  STD_LOGIC;
		  -- AXIS Clock Domain 
		  AXIS_ACLK	    : in  std_logic; -- AXIS Clk 
		  rst           : in  STD_LOGIC; -- rst signal 
          S_LOAD_DONE   : in  STD_LOGIC; -- Slave Buffer Full
          M_EMPT_DONE   : in  STD_LOGIC; -- Master Buffer Empty
          LOAD_EMPT_EN  : out STD_LOGIC; -- Enable Load/Empty Processes 
          PROC_EN       : out STD_LOGIC; -- Enable Processing Processes  
		  state_out     : out STD_LOGIC_VECTOR(1 downto 0); -- FSM State out
		  -- DSP Clock Domain 
		  DSP_CLK       : in  STD_LOGIC;
          S_PROC_DONE   : in  STD_LOGIC; -- Slave Buffer Loaded 
		  M_PROC_DONE   : in  STD_LOGIC); -- Master Buffer Loaded
end AXIS_CYCLE_FSM_2clk;

architecture Behavioral of AXIS_CYCLE_FSM_2clk is
	-- Constants
    -- Types
    type statetype is (S0_load_empt, S1_le_buf, S2_proc, S3_pr_buf);
    -- Signals
    signal state, nextstate: statetype;
	signal S_PD_Sync : std_logic := '0';
	signal M_PD_Sync : std_logic := '0';
	signal count_rst : std_logic := '0'; 
	signal count_en  : std_logic := '0'; 
	signal count_int : std_logic_vector(7 downto 0) := (others => '0');
	signal count_check : std_logic_vector(7 downto 0) := (others => '0');
	-- Components 
	-- Synchronizer
	component clock_cross_bin is
		Port ( clk_in : in STD_LOGIC;
			   clk_out : in STD_LOGIC;
			   data_in : in STD_LOGIC;
			   data_out : out STD_LOGIC);
	end component clock_cross_bin;
	
	-- Simple counter for addressing...and counting
    component counter_simp_en is 
		generic (data_width : integer := 8; -- just hard set the counter size to 8-bits
             step_size  : integer := 1);
        port(
            clk  : in   std_logic;
            ce   : in   std_logic;
            rst  : in   std_logic;
			en   : in   std_logic;
            dout : out  std_logic_vector(data_width-1 downto 0));
    end component counter_simp_en;
	
begin
	count_check <= std_logic_vector(to_unsigned(5,8)); -- just hard set it to 5 clocks for now
	-- Run FSM on AXIS clock and synchronize DSP signals 
	-- Component Instantiations 
	s_proc_synch_inst: clock_cross_bin
		port map(clk_in => DSP_CLK,
				 clk_out => AXIS_ACLK,
				 data_in => S_PROC_DONE,
				 data_out => S_PD_Sync);
	
	m_proc_synch_inst: clock_cross_bin
		port map(clk_in => DSP_CLK,
				 clk_out => AXIS_ACLK,
				 data_in => M_PROC_DONE,
				 data_out => M_PD_Sync);
				 
	buff_count_inst : counter_simp_en
		port map(clk => AXIS_ACLK,
				 ce  => '1',
				 rst => rst or count_rst,
				 en => count_en,
				 dout => count_int);
				 
    -- State Reset and Register Logic
    process(AXIS_ACLK,rst) begin
        if AXIS_ACLK'event and (AXIS_ACLK='1') then
            if (rst='1') then 
                state <= S0_load_empt;-- set this back to s0...
            else
                state <= nextstate;
            end if;
        end if;
    end process;
    
    -- Next State Logic
    process(state,S_LOAD_DONE,S_PD_Sync,M_PD_Sync,M_EMPT_DONE,count_int) begin
        case state is
            when S0_load_empt => if ((S_LOAD_DONE = '1') and (M_EMPT_DONE = '1')) then 
                                nextstate <= S1_le_buf;
                            else
                                nextstate <= S0_load_empt; 								
                            end if;
			when S1_le_buf => if (count_int>count_check) then 
                                nextstate <= S2_proc;
                            else
                                nextstate <= S1_le_buf; 								
                            end if;				
            when S2_proc => if ((S_PD_Sync = '1') and (M_PD_Sync = '1')) then
								nextstate <= S3_pr_buf;
							else
                                nextstate <= S2_proc; 								
                            end if;  
			when S3_pr_buf => if (count_int>count_check) then 
                                nextstate <= S0_load_empt;
                            else
                                nextstate <= S3_pr_buf; 								
                            end if;								
            when others => nextstate <= S0_load_empt;
        end case;
    end process;                                        
-- Output Logic and State Process
-- Need two Clock Procs for this, but they're well differentiated 
 process(state,AXIS_ACLK) begin
    if AXIS_ACLK'event and AXIS_ACLK='1' then
     if state = S0_load_empt then
		LOAD_EMPT_EN <= '1';
	    PROC_EN <= '0';  
		count_rst <= '1';
		count_en <= '0';
        state_out <= "00";
	elsif state = S1_le_buf then
		LOAD_EMPT_EN <= '0';
	    PROC_EN <= '0';  
		count_rst <= '0';
		count_en <= '1';
        state_out <= "01";	
	elsif state = S2_proc then
		LOAD_EMPT_EN <= '0';
	    PROC_EN <= '1';  
		count_rst <= '1';
		count_en <= '0';
        state_out <= "10";
	elsif state = S3_pr_buf then
		LOAD_EMPT_EN <= '0';
	    PROC_EN <= '0';  
		count_rst <= '0';
		count_en <= '1';
        state_out <= "11";			
     else
		LOAD_EMPT_EN <= '0';
	    PROC_EN <= '0';  
		count_rst <= '1';
		count_en <= '0';
        state_out <= "00";	
     end if;
    end if;
  end process;
 --
end Behavioral;