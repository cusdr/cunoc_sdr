-- Read Pointer Comparing File 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_ARITH.ALL;

entity RPTR_ACOMP is
	generic (ADDR : integer := 10);
	port(rclk     : in  std_logic;
		 rrst_n   : in  std_logic;
		 ren      : in  std_logic;
		 aempty_n : in  std_logic;
		 rempty   : out std_logic;
		 rptr     : out std_logic_vector(ADDR-1 downto 0));
end RPTR_ACOMP;

architecture behavior of RPTR_ACOMP is
-- Signal Instantiations
	-- Reg signals
	signal rbin : std_logic_vector(ADDR-1 downto 0) := (others => '0');
	signal rptr_reg : std_logic_vector(ADDR-1 downto 0) := (others => '0');
	signal rempty_reg : std_logic := '1';
	signal rempty2_reg : std_logic := '1';
	
	-- Wire signals 
	signal rgraynext : std_logic_vector(ADDR-1 downto 0);
	signal rbinnext : std_logic_vector(ADDR-1 downto 0);
	signal add_sig : std_logic_vector(ADDR-1 downto 0);
	
begin
	-- Basic assignments
	add_sig(ADDR-1 downto 1) <= (others => '0');
	add_sig(0) <= ren and not(rempty_reg);
	-- Reg assignments 
	rempty <= rempty_reg;
	rptr <= rptr_reg; 
	-- Combinational assignments 
	rbinnext <= rbin + add_sig; 
	rgraynext <= ('0' & (rbinnext(ADDR-1 downto 1)) xor rbinnext);	
	
	-- Main proc to reset and advance rbin,rptr_reg
	main_proc: process(rclk,rrst_n) begin
		if(rrst_n='0') then
			rbin <= (others => '0');
			rptr_reg <= (others => '0');
		elsif rclk'event and rclk='1' then  
			rptr_reg <= rgraynext;
			rbin <= rbinnext;
		end if;
	end process main_proc;
	
	-- Aempty Process
	aempty_proc: process(rclk,aempty_n) begin
		if(aempty_n = '0') then 
			rempty_reg <= '1';
			rempty2_reg <= '1';
		elsif rclk'event and rclk='1' then  
				rempty_reg <= rempty2_reg;
				rempty2_reg <= not(aempty_n);
		end if;
	end process aempty_proc;
	
end behavior;	