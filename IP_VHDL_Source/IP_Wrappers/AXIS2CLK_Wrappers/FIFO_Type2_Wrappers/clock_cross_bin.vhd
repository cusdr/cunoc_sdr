----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/04/2014 05:14:12 PM
-- Design Name: 
-- Module Name: clock_cross_bin - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_cross_bin is
    Port ( clk_in : in STD_LOGIC;
           clk_out : in STD_LOGIC;
           data_in : in STD_LOGIC;
           data_out : out STD_LOGIC);
end clock_cross_bin;

architecture Behavioral of clock_cross_bin is
--Reg Signals
 signal data_reg_clk_in : STD_LOGIC;
    signal data_reg_clk_out : STD_LOGIC;
-- End signals/components
begin
-- Data in 
    process_in: process(clk_in) begin
        if clk_in'event and clk_in = '1' then
            data_reg_clk_in <= data_in; 
        end if;
    end process;
-- Data Out    
    process_out: process(clk_out) begin
        if clk_out'event and clk_out = '1' then
            data_reg_clk_out <= data_reg_clk_in;
            data_out <= data_reg_clk_out;             
        end if;
    end process;    
--
end Behavioral;