library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;

entity M_AXIS_FIFO is 
    generic (AXI_DATA_WIDTH : integer := 64;
			 AXI_FIFO_DEPTH : integer := 6);-- in log2
    port(-- AXIS Clock Domain
		 -- AXIS_ACLK  : in  std_logic;
		 -- Sysgen Simulation Signals 
		 clk	       : in  std_logic;
		 ce        	   : in  std_logic; 
         AXIS_RESETN   : in  std_logic;
		 -- Master Interface 
		 M_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 M_AXIS_TVALID : out std_logic;
		 M_AXIS_TREADY : in  std_logic;
		 M_AXIS_TLAST  : out std_logic;
		 M_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
		 -- Buffer Signals
		 EMPTY_DONE	   : out std_logic;
		 EMPTY_EN       : in  std_logic;
		 -- DSP Clock Domain
		 -- DSP_clk    : in  std_logic;
		 -- Buffer Signals
		 PROC_DONE     : out std_logic;
		 PROC_EN       : in  std_logic;
		 -- DSP Signals In
		 DSP_DATA_IN   : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
		 DSP_VAL_IN    : in  std_logic;
		 LAST_IN       : in  std_logic);
end M_AXIS_FIFO;

architecture behavior of M_AXIS_FIFO is
	-- Constant, Signal, and Component Declarations
    -- Constants
	constant RAM_DEPTH : integer := 2**AXI_FIFO_DEPTH;
	-- Standard FIFO Component
	component STD_FIFO is
		Generic (constant DATA_WIDTH  : positive := AXI_DATA_WIDTH+1;
				 constant FIFO_DEPTH  : positive := RAM_DEPTH;
				 MEM_TYPE : string := "distributed");
		Port (CLK	  : in  STD_LOGIC;
			  CE      : in  STD_LOGIC;
			  RST	  : in  STD_LOGIC;
			  WriteEn : in  STD_LOGIC;
			  DataIn  : in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
			  ReadEn  : in  STD_LOGIC;
			  DataOut : out STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
			  Empty	  : out STD_LOGIC;
			  Full	  : out STD_LOGIC);
	end component STD_FIFO;
	
	-- Signals
    -- Master Signals
	signal M_RE : std_logic := '0';
	signal M_EMPTY : std_logic := '0';
	signal M_VAL_INT : std_logic := '0';
	signal M_VAL_REG : std_logic := '0';	
	signal LAST_REG : std_logic := '0'; 
	-- DSP Signals 
	signal D_WE : std_logic := '0';
	signal D_FULL : std_logic := '0';
	signal FIFO_DOUT : std_logic_vector(AXI_DATA_WIDTH downto 0) := (others => '0');
begin
	-- Signal Assignments 
	-- MASTER Signal Assignments
	M_AXIS_TKEEP <= (others => '1'); 
	M_VAL_INT <= not(M_EMPTY) and EMPTY_EN; 
	M_RE <= M_AXIS_TREADY and M_VAL_INT;
	M_AXIS_TVALID <= M_VAL_REG;
	M_AXIS_TLAST <= FIFO_DOUT(AXI_DATA_WIDTH) and not(LAST_REG); -- Again, TLAST gets the Rising edge of the fifo last signal
	M_AXIS_TDATA <= FIFO_DOUT(AXI_DATA_WIDTH-1 downto 0); 
	EMPTY_DONE <= M_EMPTY; 
	-- AA COMMENT: Good above here right now...
	
	-- DSP Signal Assignments
	D_WE <= not(D_FULL) and DSP_VAL_IN and PROC_EN;
	PROC_DONE <= not(DSP_VAL_IN) or D_FULL; -- the D_FULL should never get triggered  
	
	-- Component Instantiations 
	-- Slave FIFO
	fifo_master_inst: STD_FIFO
		port map(CLK => clk,
				 CE  => '1',
				 RST => not(AXIS_RESETN),
				 WriteEn => D_WE,
				 DataIn => (LAST_IN & DSP_DATA_IN),
				 ReadEn => M_RE,
				 DataOut => FIFO_DOUT,
				 Empty => M_EMPTY,
				 Full => D_FULL);
				 
	Reg_Proc: process (clk) begin
        if clk' event and clk = '1' then						  
		    LAST_REG <= FIFO_DOUT(AXI_DATA_WIDTH);
			M_VAL_REG <= M_VAL_INT; 
		end if;				 
	end process Reg_Proc;

end behavior;  
	
	