----------------------------------------------------------------------------------
-- Company: CU Boulder
-- Engineer: Austin M Anderson 
-- 
-- Create Date: 07/12/2016 2:38 PM MDT
-- Design Name: TEMPLATE_TOP.vhd
-- Module Name: TEMPLATE_TOP - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: This is an example pipelined, streaming DSP core. 
-- 
-- Dependencies: See README.md in git repo directory
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
use IEEE.std_logic_arith.all;
use IEEE.math_real.all;
use work.AXIS_WRAPPER_pkg.all;

-- Example of wrapping simple pipelined DSP adder in AXI Stream
entity TEMPLATE_TOP is 
    generic (AXI_DATA_WIDTH : integer := 64; -- Standard width is 64 for the Zedboard design
             S_AXI_FIFO_DEPTH : integer := 5; -- 2^5 = 32 FIFO depth on slave
             M_AXI_FIFO_DEPTH : integer := 6);-- 2^6 = 64 FIFO depth on Master 
                                              -- For flow control Master Depth = Slave Depth+1
    port(AXIS_ACLK       : in  std_logic; 
         AXIS_RESETN   : in  std_logic; -- Active Low reset for AXI4 standard in Xilinx
         -- Slave Interface
         S0_AXIS_TDATA  : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
         S0_AXIS_TVALID : in  std_logic;
         S0_AXIS_TREADY : out std_logic;
         S0_AXIS_TLAST  : in  std_logic;
         S0_AXIS_TKEEP  : in  std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0);
         -- Master Interface 
         M0_AXIS_TDATA  : out std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
         M0_AXIS_TVALID : out std_logic;
         M0_AXIS_TREADY : in  std_logic;
         M0_AXIS_TLAST  : out std_logic;
         M0_AXIS_TKEEP  : out std_logic_vector(AXI_DATA_WIDTH/8-1 downto 0));
end TEMPLATE_TOP;

architecture behavior of TEMPLATE_TOP is
    -- Constant, Signal, and Component Declarations
    -- Constants
    constant AXI_TKEEP_WIDTH : integer := AXI_DATA_WIDTH/8;
    constant MODULE_DELAY : integer := 1; -- Delay of the pipelined module.
    constant FSM_COUNT_WIDTH : integer := 8; -- Counter width for Pipe'd DSP control. 
                                             -- This needs to handle the DSP delay, 8-bits is 256 
                                             -- So more than sufficient 
    -- Signals
    -- Slave Signals
    signal S_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal S_VAL  : std_logic := '0';
    signal S_RDY  : std_logic := '0';
    signal S_LAST : std_logic := '0';
    signal S_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
    -- Master signals 
    signal M_DATA : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal M_VAL  : std_logic := '0';
    signal M_RDY  : std_logic := '0';
    signal M_LAST : std_logic := '0';
    signal M_KEEP : std_logic_vector(AXI_TKEEP_WIDTH-1 downto 0) := (others => '0');
    -- DSP Signals 
    signal DSP_DIN : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal DSP_VAL_IN : std_logic := '0';
    signal DSP_LAST_IN : std_logic := '0';
    --
    signal DSP_DOUT : std_logic_vector(AXI_DATA_WIDTH-1 downto 0) := (others => '0');
    signal DSP_EN   : std_logic := '0';
    signal DSP_VAL_OUT : std_logic := '0';
    signal DSP_LAST_OUT : std_logic := '0';   
    -- Control Signals 
    signal LOAD_EMPT_EN : std_logic := '0';
    signal PROC_EN : std_logic := '0'; 
    signal S_LOAD_DONE : std_logic := '0';
    signal S_PROC_DONE : std_logic := '0';
    signal M_EMPT_DONE : std_logic := '0';
    signal M_PROC_DONE : std_logic := '0';
    -- USER DSP SIGNALS
     
    -- DSP Component
    --=============INTEGRATE DSP MODULE HERE========================
    -- Basic Register Template as an Example
    component DSP_TEMPLATE is
        generic(data_width : integer := 64);
        port(clk      : in STD_LOGIC;
             en       : in STD_LOGIC;
             rst      : in STD_LOGIC;
             data_in  : in STD_LOGIC_VECTOR (data_width-1 downto 0);
             data_out : out STD_LOGIC_VECTOR (data_width-1 downto 0));
    end component DSP_TEMPLATE;    
    --===================END DSP MODULE HERE========================    
    
    -- End Signals and Components
begin
-- Signal Assignments
    -- Slave Signal Assignments
    S_DATA <= S0_AXIS_TDATA; 
    S_VAL  <= S0_AXIS_TVALID;
    S0_AXIS_TREADY <= S_RDY;
    S_LAST <= S0_AXIS_TLAST; 
    S_KEEP <= S0_AXIS_TKEEP; 
    -- Master Slave Signal Assignments
    M0_AXIS_TDATA  <= M_DATA;
    M0_AXIS_TVALID <= M_VAL;
    M_RDY <= M0_AXIS_TREADY;
    M0_AXIS_TLAST  <= M_LAST;
    M0_AXIS_TKEEP  <= M_KEEP;
    -- DSP Signal Assignments 
    
    -- Component Instantiations
    -- Slave AXI Stream Buffer
    S_AXIS_FIFO_inst: S_AXIS_FIFO
        generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
                    AXI_FIFO_DEPTH => S_AXI_FIFO_DEPTH)
        port map(clk => AXIS_ACLK,
                 ce => '1',
                 AXIS_RESETN => AXIS_RESETN,
                 S_AXIS_TDATA => S_DATA,
                 S_AXIS_TVALID => S_VAL,
                 S_AXIS_TREADY => S_RDY,
                 S_AXIS_TLAST => S_LAST,
                 S_AXIS_TKEEP => S_KEEP,
                 LOAD_DONE => S_LOAD_DONE,
                 LOAD_EN => LOAD_EMPT_EN,
                 PROC_DONE => S_PROC_DONE,
                 PROC_EN => PROC_EN,
                 DSP_DATA_OUT => DSP_DIN,
                 DSP_VAL_OUT => DSP_VAL_IN,
                 LAST_OUT => DSP_LAST_IN);
                 
    -- Pipelined DSP FSM Instantiation 
    PIPE_DSP_FSM_inst: PIPE_DSP_FSM
        generic map(module_delay => MODULE_DELAY,
                    counter_width => FSM_COUNT_WIDTH)
        port map(clk => AXIS_ACLK,
                 rst => not(AXIS_RESETN),
                 ce => '1',
                 en => '1',
                 val_in => DSP_VAL_IN,
                 last_in => DSP_LAST_IN,
                 DSP_en => DSP_EN,
                 val_out => DSP_VAL_OUT,
                 last_out => DSP_LAST_OUT,
                 state_out => open);
                 
    --==============INSTANTIATE DSP MODULE HERE=========================
    -- Template Instantiation
    DSP_TEMPLATE_inst : DSP_TEMPLATE
        port map(clk => AXIS_ACLK,
                 en  => DSP_EN,
                 rst => not(AXIS_RESETN),
                 data_in => DSP_DIN,
                 data_out => DSP_DOUT);
    
    -- Master AXI Stream Buffer    
    M_AXIS_FIFO_inst: M_AXIS_FIFO
        generic map(AXI_DATA_WIDTH => AXI_DATA_WIDTH,
                    AXI_FIFO_DEPTH => M_AXI_FIFO_DEPTH)
        port map(clk => AXIS_ACLK,
                 ce => '1',
                 AXIS_RESETN => AXIS_RESETN,
                 M_AXIS_TDATA => M_DATA,
                 M_AXIS_TVALID => M_VAL,
                 M_AXIS_TREADY => M_RDY,
                 M_AXIS_TLAST => M_LAST,
                 M_AXIS_TKEEP => M_KEEP,
                 EMPTY_DONE => M_EMPT_DONE,
                 EMPTY_EN => LOAD_EMPT_EN,
                 PROC_DONE => M_PROC_DONE,
                 PROC_EN => PROC_EN,
                 DSP_DATA_IN => DSP_DOUT,
                 DSP_VAL_IN => DSP_VAL_OUT,
                 LAST_IN => DSP_LAST_OUT);
    
    -- Load/Empty and Process control FSM
    AXIS_CYCLE_FSM_inst: AXIS_CYCLE_FSM
        port map(clk => AXIS_ACLK,
                 rst => not(AXIS_RESETN),
                 ce => '1',
                 S_LOAD_DONE => S_LOAD_DONE,
                 S_PROC_DONE => S_PROC_DONE,
                 M_PROC_DONE => M_PROC_DONE,
                 M_EMPT_DONE => M_EMPT_DONE,
                 LOAD_EMPT_EN => LOAD_EMPT_EN,
                 PROC_EN => PROC_EN,
                 state_out => open);
    
end behavior;  
