library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use IEEE.std_logic_arith.all;

entity counter_simp_en is 

    generic (data_width : integer := 16;
             step_size  : integer := 1
    );

    port(
		clk	 : in   std_logic;
        ce   : in   std_logic;
        rst  : in   std_logic;
        en   : in   std_logic;
		dout : out	std_logic_vector(data_width-1 downto 0)
    );

end counter_simp_en;

architecture behavior of counter_simp_en is

  	signal COUNT : std_logic_vector(data_width-1 downto 0) := (others => '0');
  	
begin
  	-- notice the process statement and the variable COUNT
  	clk_proc:process(CLK,rst)
  	begin 
  		if (CLK'EVENT AND CLK = '1') then
  			if rst = '1' then
  			   COUNT <= (others => '0');
  			elsif((rst = '0') and (en = '1')) then 
  			   COUNT <= COUNT + conv_std_logic_vector(step_size,data_width) ;
  			else
               COUNT <= COUNT;
            end if;
  		end if;
  		DOUT <= COUNT;
  	end process clk_proc;

end behavior;