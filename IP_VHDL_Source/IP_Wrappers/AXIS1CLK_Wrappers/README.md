This directory contains VHDL files for wrapping pipelined, streaming DSP and making it compatible with the AXI-4 stream protocol and our AXI-4 stream switch. The files for single clock domain wrappers are in this directory. For a more detailed description of the architecture see the Docs/ directory in the git repository. 

The VHDL files in this repository can be used to adapt your existing DSP cores to our Network on Chip (NoC) architecture. This system uses an AXI-4 stream cross-bar switch as the NoC. The base DSP must be pipelined and streaming, that is it produces as much data as is consumed and can produce data on each valid clock cycle. It is important for the user to specify the total number of valid clock cycles z it takes the core to produce a valid result. To that end, the base interface is:

Inputs
------
1. Data In -> n-bit vector where n <= 64
2. Enable -> 1-bit enable high 
3. Reset -> 1-bit reset high

Outputs
-------
1. Data Out -> m-bit vector where m <= 64

Existing cores that are compatible with these requirements can be integrated directly following the "TEMPLATE_TOP.vhd" example. This example implements a basic data register DSP core called "DSP_TEMPLATE.vhd". In this example a single register is generated. The register has a total latency of one clock cycle and this is reflected in the example design. The wrappers encapsulate all the required support files in the AXIS_WRAPPER_pkg.vhd so that users need only add that file to a project and declare their own DSP directly in the VHDL template. 

Two FIFOs are instantiated to handle data transactions between the network and the DSP: one for the slave-side and one for the master-side. The PIPE_DSP_FSM.vhd is a finite-state-machine that handles transitioning data to and from the FIFOs. A modularized counter, shift-register, and FIFO are included to support the FSM and transition FIFOs. 

For flow control, the FSM allows the slave-side FIFO to fill while the master-side FIFO empties in the first state. When the master is empty, the FSM transitions to transferring data internally between the slave FIFO and master FIFO through the DSP logic. This mechanism for flow control is completely standalone and requires no module-module synchroniztion, however it does result in suboptimal performance as the processing duty-factor is <=50%. However, as the network clock tends to be >> the radio clock, the lowered duty factor is not of significant concern. 


