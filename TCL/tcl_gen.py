import glob
import numpy as np
from tcl_utility import *

def GenerateTCL(components, vivado_version):
	print "generating tcl scripts"
	# List of components for main Switch IP core
	comp_list = ["CUNoC_IP", "AXIS_SWITCH_TOP", "M_AXIS_Rx2NoC", "S_AXIS_NoC2Tx", "rx_phy_top", "tx_phy_top"]
	comp_list.extend(components)
	print comp_list


	# Find required VHDL for Top Blob
	comp_vhd_list = []
	for component in comp_list:             
		comp_vhd_list.extend(FindIPSrc(component))

	# Make tcl script for generating main IP core
	proj_name = "CUNoC_blob"
	proj_dir = build_dir+proj_name
	IP_lib_dir = proj_dir+"/"+proj_name+"_bd_IP/"
	UpdateTCL(vivado_version, proj_name, proj_dir, comp_vhd_list, IP_lib_dir, 1, build_dir + "AXIS_SWITCH_IP_gen.tcl")
    
	'''
	# List of components for Version Control Register
	ver_reg_comp_list = ["S_AXI_VER_REG"]

	# Find required VHDL for Top Blob
	ver_reg_vhd_list = []
	for component in ver_reg_comp_list:             
    	ver_reg_vhd_list.extend(FindIPSrc(component))

	# Make tcl scirpt for generating main IP core
	proj_name = "VER_CTRL_REG"
	proj_dir = build_dir+proj_name
	IP_lib_dir = proj_dir+"/"+proj_name+"_bd_IP/"
	UpdateTCL((2014,4), proj_name, proj_dir, ver_reg_vhd_list, IP_lib_dir, 1, build_dir + "VER_CTRL_REG_IP_gen.tcl")
	'''
	print "done"
