import glob
from tcl_names import *

#comp_name = "NCI_AXIS_FIFO"
# Find Component Name 
def FindIPSrc(comp_name):
    comp_file = comp_name + ".vhd"
    vhd_list = []
    ip_dir_list = []
    ip_dir_found = []
    for file in glob.glob(ip_dir_name+'*/'):
        ip_dir_list.append(file)

    for ip_dir in ip_dir_list:
        files_found = glob.glob(ip_dir+comp_file)
        if(len(files_found)>0):
            ip_dir_found.append(ip_dir)
    
    # Think Dir_List should always just be 1 long    
    assert len(ip_dir_found) == 1
    for ip_dir in ip_dir_found:
        vhd_list.extend(glob.glob(ip_dir+"*.vhd"))
        
    return vhd_list

'''
    Inputs to tcl gen:
    Proj Name
    Proj Directory 
    Fileset
    Need All the IP Core names
    IP Library Directory
    Core Version #
'''
def UpdateTCL(viv_vers, proj_name, proj_dir, VHD_List, IP_lib_dir, core_version, tcl_out): 
    ftemp = open(tcl_template,'r')
    tcl_contents = ftemp.readlines()
    ftemp.close()      
    VHD_add = " ".join(VHD_List)
    if(viv_vers=="2015.1"):
        axis_clock_ass = "ipx::associate_bus_interfaces -busif S_AXI -clock AXIS_ACLK [ipx::current_core]"
    else:
        axis_clock_ass = "set_property value M0_AXIS:S0_AXIS:S_AXI [ipx::get_bus_parameters ASSOCIATED_BUSIF -of_objects [ipx::get_bus_interfaces AXIS_signal_clock -of_objects [ipx::current_core]]]"
    for jj in range(len(tcl_contents)):
        tcl_contents[jj] = tcl_contents[jj].replace("*proj_name*",proj_name)
        tcl_contents[jj] = tcl_contents[jj].replace("*proj_dir*",proj_dir)
        tcl_contents[jj] = tcl_contents[jj].replace("*fileset*",VHD_add)
        tcl_contents[jj] = tcl_contents[jj].replace("*ip_lib_dir*",IP_lib_dir)
        tcl_contents[jj] = tcl_contents[jj].replace("*core_version*",str(core_version))
        tcl_contents[jj] = tcl_contents[jj].replace("*AXIS_CLOCK_ASSOCIATE*",axis_clock_ass)
        
    ftemp = open(tcl_out,'w')
    contents_out = "".join(tcl_contents)
    ftemp.write(contents_out)
    ftemp.close()
    
