# BD IP Generation Template TCL Script

# Create the Project
create_project *proj_name* *proj_dir* -part xc7z020clg484-1

# Set to Zedboard
set_property board_part em.avnet.com:zed:part0:1.2 [current_project]

# Set VHDL
set_property target_language VHDL [current_project]

# Add Files
add_files -norecurse *fileset*
# Import Files
import_files -force -norecurse
# Update Hierarchy
update_compile_order -fileset sources_1
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

# Package Current Project
ipx::package_project -root_dir *ip_lib_dir* -import_files 
*AXIS_CLOCK_ASSOCIATE*
set_property vendor CU_Boulder [ipx::current_core]
set_property library CUNoC [ipx::current_core]
set_property display_name *proj_name* [ipx::current_core]
set_property description *proj_name* [ipx::current_core]
set_property taxonomy /CU_NoC_IP [ipx::current_core]
set_property core_revision *core_version* [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property ip_repo_paths  *ip_lib_dir* [current_project]
update_ip_catalog
