# BD IP Generation Template TCL Script

# Create the Project
create_project VER_CTRL_REG Build_Dir/VER_CTRL_REG -part xc7z020clg484-1

# Set to Zedboard
set_property board_part em.avnet.com:zed:part0:1.2 [current_project]

# Set VHDL
set_property target_language VHDL [current_project]

# Add Files
add_files -norecurse IP_VHDL_Source/VERSION_CTRL_REG/S_AXI_VER_REG.vhd
# Import Files
import_files -force -norecurse
# Update Hierarchy
update_compile_order -fileset sources_1
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

# Package Current Project
ipx::package_project -root_dir Build_Dir/VER_CTRL_REG/VER_CTRL_REG_bd_IP/ -import_files 
set_property vendor CU_Boulder [ipx::current_core]
set_property library CUNoC [ipx::current_core]
set_property display_name VER_CTRL_REG [ipx::current_core]
set_property description VER_CTRL_REG [ipx::current_core]
set_property taxonomy /CU_NoC_IP [ipx::current_core]
set_property core_revision 1 [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property ip_repo_paths  Build_Dir/VER_CTRL_REG/VER_CTRL_REG_bd_IP/ [current_project]
update_ip_catalog
