# BD IP Generation Template TCL Script

# Create the Project
create_project CUNoC_blob Build_Dir/CUNoC_blob -part xc7z020clg484-1

# Set to Zedboard
set_property board_part em.avnet.com:zed:part0:1.2 [current_project]

# Set VHDL
set_property target_language VHDL [current_project]

# Add Files
add_files -norecurse IP_VHDL_Source/TOP_Dir/CUNoC_IP.vhd IP_VHDL_Source/NoCSWITCH_AXIS/AXIS_RDY_MUX.vhd IP_VHDL_Source/NoCSWITCH_AXIS/AXIS_SWITCH_MUX.vhd IP_VHDL_Source/NoCSWITCH_AXIS/AXIS_SWITCH_TOP.vhd IP_VHDL_Source/NoCSWITCH_AXIS/S_AXI_REG.vhd IP_VHDL_Source/RX_ADAPTER/clk_sync.vhd IP_VHDL_Source/RX_ADAPTER/clock_cross_bin.vhd IP_VHDL_Source/RX_ADAPTER/counter_simp_en.vhd IP_VHDL_Source/RX_ADAPTER/DP_RAM_PHY.vhd IP_VHDL_Source/RX_ADAPTER/FIFO_ASYNC_PHY.vhd IP_VHDL_Source/RX_ADAPTER/M_AXIS_Rx2NoC.vhd IP_VHDL_Source/RX_ADAPTER/rptr_comp.vhd IP_VHDL_Source/RX_ADAPTER/wptr_comp.vhd IP_VHDL_Source/TX_ADAPTER/clk_sync.vhd IP_VHDL_Source/TX_ADAPTER/clock_cross_bin.vhd IP_VHDL_Source/TX_ADAPTER/DP_RAM_PHY.vhd IP_VHDL_Source/TX_ADAPTER/FIFO_ASYNC_PHY.vhd IP_VHDL_Source/TX_ADAPTER/rptr_comp.vhd IP_VHDL_Source/TX_ADAPTER/S_AXIS_NoC2Tx.vhd IP_VHDL_Source/TX_ADAPTER/shift_reg_en.vhd IP_VHDL_Source/TX_ADAPTER/shift_reg_en_bool.vhd IP_VHDL_Source/TX_ADAPTER/wptr_comp.vhd IP_VHDL_Source/RX_PHY/rx_phy_clock_in.vhd IP_VHDL_Source/RX_PHY/rx_phy_lvds_in.vhd IP_VHDL_Source/RX_PHY/rx_phy_top.vhd IP_VHDL_Source/TX_PHY/tx_phy_lvds_out.vhd IP_VHDL_Source/TX_PHY/tx_phy_top.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/Adder_AXIS_F2_2clk.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/adder_module_en.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/AXIS_CYCLE_FSM_2clk.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/AXIS_F2_2clk_pkg.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/clk_sync.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/clock_cross_bin.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/counter_simp_en.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/DP_RAM.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/FIFO_ASYNC_TOP.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/M_AXIS_F2_2clk.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/PIPE_DSP_FSM.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/rptr_comp.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/S_AXIS_F2_2clk.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/shift_reg_en_bool.vhd IP_VHDL_Source/ADDER2CLKT2_AXIS/wptr_comp.vhd IP_VHDL_Source/NCI_AXIS/adder_module_en.vhd IP_VHDL_Source/NCI_AXIS/adder_tree_en.vhd IP_VHDL_Source/NCI_AXIS/AXIS_CYCLE_FSM.vhd IP_VHDL_Source/NCI_AXIS/counter_simp_en.vhd IP_VHDL_Source/NCI_AXIS/M_AXIS_FIFO.vhd IP_VHDL_Source/NCI_AXIS/NCI_AXIS_FIFO.vhd IP_VHDL_Source/NCI_AXIS/NCI_AXIS_WRAPPER_pkg.vhd IP_VHDL_Source/NCI_AXIS/non_coh_int_en.vhd IP_VHDL_Source/NCI_AXIS/PIPE_DSP_FSM.vhd IP_VHDL_Source/NCI_AXIS/S_AXIS_FIFO.vhd IP_VHDL_Source/NCI_AXIS/shift_in_gen.vhd IP_VHDL_Source/NCI_AXIS/shift_reg_en_bool.vhd IP_VHDL_Source/NCI_AXIS/STD_FIFO.vhd
# Import Files
import_files -force -norecurse
# Update Hierarchy
update_compile_order -fileset sources_1
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

# Package Current Project
ipx::package_project -root_dir Build_Dir/CUNoC_blob/CUNoC_blob_bd_IP/ -import_files 
ipx::associate_bus_interfaces -busif S_AXI -clock AXIS_ACLK [ipx::current_core]
set_property vendor CU_Boulder [ipx::current_core]
set_property library CUNoC [ipx::current_core]
set_property display_name CUNoC_blob [ipx::current_core]
set_property description CUNoC_blob [ipx::current_core]
set_property taxonomy /CU_NoC_IP [ipx::current_core]
set_property core_revision 1 [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property ip_repo_paths  Build_Dir/CUNoC_blob/CUNoC_blob_bd_IP/ [current_project]
update_ip_catalog
