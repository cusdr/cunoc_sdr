# Zedboard+AD9361 CUNoC Project Gen TCL Script
# By Austin Anderson 

# Create Project for Zynq 7020 
create_project Zed_Test Zed_Test -part xc7z020clg484-1

# Set to Zedboard
set_property board_part em.avnet.com:zed:part0:1.2 [current_project]

# Create Block Diagram Design 
create_bd_design "system_bd"

# Add and configure Zynq Processor
# AA COMMENT: IP Version here
create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
# Set Zedboard Config Default
set_property -dict [list CONFIG.preset {ZedBoard}] [get_bd_cells processing_system7_0]
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
# Turn on AXIS HP0
set_property -dict [list CONFIG.PCW_USE_S_AXI_HP0 {1} CONFIG.PCW_USE_FABRIC_INTERRUPT {1} CONFIG.PCW_IRQ_F2P_INTR {1}] [get_bd_cells processing_system7_0]
# Turn on 250 MHz PS->PL Clock 
set_property -dict [list CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {200.000000} CONFIG.PCW_EN_CLK1_PORT {1}] [get_bd_cells processing_system7_0]
# Turn on GPIO, SPI0 
set_property -dict [list CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {1} CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {1} CONFIG.PCW_GPIO_EMIO_GPIO_IO {49}] [get_bd_cells processing_system7_0]
# Expose GPIO
create_bd_port -dir I -from 48 -to 0 GPIO_I
connect_bd_net [get_bd_pins /processing_system7_0/GPIO_I] [get_bd_ports GPIO_I]
create_bd_port -dir O -from 48 -to 0 GPIO_O
connect_bd_net [get_bd_pins /processing_system7_0/GPIO_O] [get_bd_ports GPIO_O]
create_bd_port -dir O -from 48 -to 0 GPIO_T
connect_bd_net [get_bd_pins /processing_system7_0/GPIO_T] [get_bd_ports GPIO_T]
# Expose SPI0
create_bd_port -dir O spi_sclk_o
create_bd_port -dir O spi_mosi_o
create_bd_port -dir O spi_csn_o
create_bd_port -dir I spi_sclk_i
create_bd_port -dir I spi_mosi_i
create_bd_port -dir I spi_miso_i
create_bd_port -dir I spi_csn_i
connect_bd_net [get_bd_ports spi_sclk_o] [get_bd_pins processing_system7_0/SPI0_SCLK_O]
connect_bd_net [get_bd_ports spi_mosi_o] [get_bd_pins processing_system7_0/SPI0_MOSI_O]
connect_bd_net [get_bd_ports spi_csn_o] [get_bd_pins processing_system7_0/SPI0_SS_O]
connect_bd_net [get_bd_ports spi_sclk_i] [get_bd_pins processing_system7_0/SPI0_SCLK_I]
connect_bd_net [get_bd_ports spi_mosi_i] [get_bd_pins processing_system7_0/SPI0_MOSI_I]
connect_bd_net [get_bd_ports spi_miso_i] [get_bd_pins processing_system7_0/SPI0_MISO_I]
connect_bd_net [get_bd_ports spi_csn_i] [get_bd_pins processing_system7_0/SPI0_SS_I]



# Add and configure DMA Core 
# AA COMMENT: IP Version here
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0
# Set DMA Configuration 
set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0} CONFIG.c_m_axi_mm2s_data_width {64} CONFIG.c_m_axis_mm2s_tdata_width {64} CONFIG.c_mm2s_burst_size {16}] [get_bd_cells axi_dma_0]

# Automate Connections and Memory Mapping 
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/axi_dma_0/M_AXI_MM2S" Clk "Auto" }  [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/processing_system7_0/M_AXI_GP0" Clk "Auto" }  [get_bd_intf_pins axi_dma_0/S_AXI_LITE]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Slave "/processing_system7_0/S_AXI_HP0" Clk "Auto" }  [get_bd_intf_pins axi_dma_0/M_AXI_S2MM]

# Create and connect concat block for interupts
# AA COMMENT: IP Version here
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0
connect_bd_net [get_bd_pins axi_dma_0/mm2s_introut] [get_bd_pins xlconcat_0/In0]
connect_bd_net [get_bd_pins axi_dma_0/s2mm_introut] [get_bd_pins xlconcat_0/In1]
connect_bd_net [get_bd_pins xlconcat_0/dout] [get_bd_pins processing_system7_0/IRQ_F2P]

# CUNoC: Add VHDL Blob's IP Core Here 
# Add CUNoC IP Directory
# AA COMMENT: Absolute Path Here, can be local...
#set_property ip_repo_paths  /home/aanderson/VHDL_Debug/MASTER_IP_REPO/Build_Dir/CUNoC_blob [current_project]
# Try
# ADD CUNoC Blob and Version Control Register Repo
set_property ip_repo_paths  {Build_Dir/CUNoC_blob Build_Dir/VER_CTRL_REG} [current_project]
update_ip_catalog


# Add CUNoC Blob
create_bd_cell -type ip -vlnv CU_Boulder:CUNoC:CUNoC_IP:1.0 CUNoC_IP_0

# Attach AXI Lite Line
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/processing_system7_0/M_AXI_GP0" Clk "Auto" }  [get_bd_intf_pins CUNoC_IP_0/S_AXI]

# Connect AXIS Lines to DMA Engine
connect_bd_intf_net [get_bd_intf_pins CUNoC_IP_0/M0_AXIS] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]
connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins CUNoC_IP_0/S0_AXIS]

# Connect Clocks and Resets
connect_bd_net [get_bd_pins CUNoC_IP_0/DSP_CLK] [get_bd_pins processing_system7_0/FCLK_CLK1]

# Make Radio Connections External External
create_bd_port -dir O tx_clk_out_p
connect_bd_net [get_bd_pins /CUNoC_IP_0/tx_clk_out_p] [get_bd_ports tx_clk_out_p]
create_bd_port -dir O tx_clk_out_n
connect_bd_net [get_bd_pins /CUNoC_IP_0/tx_clk_out_n] [get_bd_ports tx_clk_out_n]
create_bd_port -dir O -from 5 -to 0 tx_data_out_p
connect_bd_net [get_bd_pins /CUNoC_IP_0/tx_data_out_p] [get_bd_ports tx_data_out_p]
create_bd_port -dir O -from 5 -to 0 tx_data_out_n
connect_bd_net [get_bd_pins /CUNoC_IP_0/tx_data_out_n] [get_bd_ports tx_data_out_n]
create_bd_port -dir O tx_frame_out_p
connect_bd_net [get_bd_pins /CUNoC_IP_0/tx_frame_out_p] [get_bd_ports tx_frame_out_p]
create_bd_port -dir O tx_frame_out_n
connect_bd_net [get_bd_pins /CUNoC_IP_0/tx_frame_out_n] [get_bd_ports tx_frame_out_n]
create_bd_port -dir I rx_clk_in_p
connect_bd_net [get_bd_pins /CUNoC_IP_0/rx_clk_in_p] [get_bd_ports rx_clk_in_p]
create_bd_port -dir I rx_clk_in_n
connect_bd_net [get_bd_pins /CUNoC_IP_0/rx_clk_in_n] [get_bd_ports rx_clk_in_n]
create_bd_port -dir I rx_frame_in_p
connect_bd_net [get_bd_pins /CUNoC_IP_0/rx_frame_in_p] [get_bd_ports rx_frame_in_p]
create_bd_port -dir I rx_frame_in_n
connect_bd_net [get_bd_pins /CUNoC_IP_0/rx_frame_in_n] [get_bd_ports rx_frame_in_n]
create_bd_port -dir I -from 5 -to 0 rx_data_in_p
connect_bd_net [get_bd_pins /CUNoC_IP_0/rx_data_in_p] [get_bd_ports rx_data_in_p]
create_bd_port -dir I -from 5 -to 0 rx_data_in_n
connect_bd_net [get_bd_pins /CUNoC_IP_0/rx_data_in_n] [get_bd_ports rx_data_in_n]

# Add Version Control Register 
create_bd_cell -type ip -vlnv CU_Boulder:CUNoC:S_AXI_VER_REG:1.0 VER_CTRL_REG_IP_0
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/processing_system7_0/M_AXI_GP0" Clk "Auto" }  [get_bd_intf_pins VER_CTRL_REG_IP_0/S_AXI]

# Start Build Process 
# AA COMMENT: Need to update pathing
regenerate_bd_layout
validate_bd_design
make_wrapper -files [get_files Zed_Test/Zed_Test.srcs/sources_1/bd/system_bd/system_bd.bd] -top
add_files -norecurse Zed_Test/Zed_Test.srcs/sources_1/bd/system_bd/hdl/system_bd_wrapper.v
add_files -norecurse Build_Dir/system_top.v
set_property top system_top [current_fileset]
add_files -fileset constrs_1 -norecurse Build_Dir/zed_con.xdc
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1
generate_target all [get_files Zed_Test/Zed_Test.srcs/sources_1/bd/system_bd/system_bd.bd]
file mkdir Build_Reports
# AA COMMENT: Need to add resource utilization reports 
launch_runs synth_1 -scripts_only
exec Zed_Test/Zed_Test.runs/synth_1/runme.sh
open_run synth_1

# AA COMMENT: Let's add a build log directory here...
report_timing_summary -file Build_Reports/timing_synth.log
# Set ExtraNetDelay high 
set_property STEPS.PLACE_DESIGN.ARGS.DIRECTIVE ExtraNetDelay_high [get_runs impl_1]

launch_runs impl_1 -scripts_only
exec Zed_Test/Zed_Test.runs/impl_1/runme.sh
open_run impl_1
report_utilization -hierarchical  -file Build_Reports/utilization_impl.log
report_timing_summary -file Build_Reports/timing_impl.log
write_bitstream -force system.bit

# Output Hardware for SDK
file mkdir Zed_Test/Zed_Test.sdk
file mkdir Zed_Test/fsbl_dir
write_hwdef -force  -file Zed_Test/Zed_Test.sdk/system_top.hdf
exec hsi -source Build_Dir/fsbl_gen.hsi
file copy -force Zed_Test/fsbl_dir/executable.elf fsbl.elf

# Copy output products 
file copy -force system.bit Zynq_Sys_Build/boot_bin/system.bit
file copy -force fsbl.elf Zynq_Sys_Build/boot_bin/fsbl.elf
cd Zynq_Sys_Build/boot_bin/
exec ./build.sh
file copy -force BOOT.BIN ../../BOOT.BIN
cd ../../


