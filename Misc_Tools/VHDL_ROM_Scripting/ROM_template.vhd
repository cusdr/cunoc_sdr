-- A parameterized, inferable, true dual-port, dual-clock block RAM in VHDL.
 
library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.ALL;
use IEEE.math_real.all;

entity ROM_config is
generic (
    DATA_WIDTH    : integer := *DAT_VAL*;
    ADDR_WIDTH    : integer := *ADD_VAL*
);
port (
    -- Clocks and Resets
    clk     : in  std_logic;
    rst_n   : in  std_logic;
    -- ROM
	addr  : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
    en    : in  std_logic;
	dout  : out std_logic_vector(DATA_WIDTH-1 downto 0)
);
end ROM_config;
 
architecture rtl of ROM_config is
    constant MEM_SIZE : integer := 2**ADDR_WIDTH; 
	-- Shared memory
    type mem_type is array ( MEM_SIZE-1 downto 0 ) of std_logic_vector(DATA_WIDTH-1 downto 0);
    shared variable mem : mem_type := (
        -- Begin ROM Initialization
        -- End ROM Initialization  
    attribute ram_style: string;
    attribute ram_style of mem : variable is "block";
	signal reg_out : std_logic_vector(DATA_WIDTH-1 downto 0);
begin
-- Assign Output 
dout <= reg_out;
-- ROM Output 
process(clk,rst_n)
begin
    if(rst_n='0') then 
        reg_out <= (others => '0');
    elsif(clk'event and clk='1') then
	    if(en = '1') then 
            reg_out <= mem(to_integer(unsigned(addr)));
	    end if; 
    end if;
end process;
 
end rtl;
