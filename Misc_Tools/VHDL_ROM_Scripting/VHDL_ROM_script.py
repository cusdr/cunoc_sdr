import numpy as np

tab_val ="    "

def contents_swap(file_in,file_out):
    ftemp = open(file_in,'r')
    contents_in = ftemp.readlines()
    ftemp.close()
    contents_out = "".join(contents_in)
    ftemp = open(file_out,'w')
    ftemp.write(contents_out)
    ftemp.close()

def ROM_gen_set(data_width, address_width, tmp_file):
    # Load file
    ftemp = open(tmp_file,'r')
    tmp_contents = ftemp.readlines()
    ftemp.close()
    for kk in range(len(tmp_contents)):
        tmp_contents[kk] = tmp_contents[kk].replace("*DAT_VAL*",str(data_width))
        tmp_contents[kk] = tmp_contents[kk].replace("*ADD_VAL*",str(address_width))    
    # Write file
    contents_out = "".join(tmp_contents)
    ftemp = open(tmp_file,'w')
    ftemp.write(contents_out)
    ftemp.close()

def vector_entry(vector_in, others_flag, tmp_file):
    check_str = "-- End ROM Initialization"
    val_entry = "std_logic_vector(to_signed(*val*,DATA_WIDTH))"
    others_entry = "others => (others => '0'));"
    ROM_temp = "ROM_template.vhd"
   
    # Load file
    ftemp = open(tmp_file,'r')
    tmp_contents = ftemp.readlines()
    ftemp.close()
    
    vec_count = 0
    for jj in tmp_contents:
        val_check = jj.find(check_str)
        if(val_check >= 0):
            vec_tab_val = val_check*" "
            vec_insert_ind = vec_count
        
        vec_count = vec_count+1


    for kk in vector_in[:-1]:
        tmp_vec_entry = val_entry
        tmp_vec_entry = tmp_vec_entry.replace("*val*",str(int(kk)))
        tmp_contents.insert(vec_insert_ind,vec_tab_val+tmp_vec_entry+",\n")
        vec_insert_ind = vec_insert_ind+1

    if(others_flag):
        kk = vector_in[-1]
        tmp_vec_entry = tmp_vec_entry.replace("*val*",str(int(kk)))
        tmp_contents.insert(vec_insert_ind,vec_tab_val+tmp_vec_entry+",\n")
        vec_insert_ind = vec_insert_ind+1
        tmp_contents.insert(vec_insert_ind,vec_tab_val+others_entry+"\n")
    else:           
        kk = vector_in[-1]
        tmp_vec_entry = tmp_vec_entry.replace("*val*",str(int(kk)))
        tmp_contents.insert(vec_insert_ind,vec_tab_val+tmp_vec_entry+");\n")

    # Write file
    contents_out = "".join(tmp_contents)
    ftemp = open(tmp_file,'w')
    ftemp.write(contents_out)
    ftemp.close()


# Debug Script
vec_len = 4096
add_len = int(np.ceil(np.log2(vec_len)))
others_flag = (vec_len!=pow(2,add_len))

data_bits = 16
data_range = pow(2,data_bits)
data_pos_max = pow(2,data_bits-1)-1
data_neg_min = -1*pow(2,data_bits-1)

# Quarter-Wave Sine
vec_ind = np.array(range(vec_len))
vec_ind = vec_ind/float(vec_len)*2*np.pi/4
sin_float = np.sin(vec_ind)

# Scale to full dynamic range
sin_int = sin_float*data_range/2.0

# Cast as an int
sin_int = np.round(sin_int)
sin_int = sin_int.astype(int)

# Clip any overflows 
sin_int[np.where(sin_int>data_pos_max)] = data_pos_max
sin_int[np.where(sin_int<data_neg_min)] = data_neg_min

contents_swap("ROM_template.vhd","tmp.vhd")
ROM_gen_set(data_bits, add_len, "tmp.vhd")
vector_entry(sin_int, others_flag, "tmp.vhd")
contents_swap("tmp.vhd","ROM_config.vhd")




