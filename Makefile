# build the project
# 1. compile proto

SUBDIRS = Config

all: $(SUBDIRS)
	$(MAKE) -C $(SUBDIRS)

clean: 
	$(MAKE) -C $(SUBDIRS) clean